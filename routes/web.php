<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['prefix' => 'admin' , 'middleware' => 'admin'], function(){
	Route::any('/' , 'Admin@Index');
	Route::any('winners' , 'Admin@Winners');
	Route::post('winners/price' , 'Admin@WinnersPrice');
	Route::post('winners/pricewc' , 'Admin@WinnersPriceWC');
	Route::any('users' , 'Admin@Users');
	Route::any('process/{id?}' , 'Admin@Process');
	Route::any('stat' , 'Admin@Stat');
	Route::post('enable' , 'Admin@Enable');
	Route::any('feedback' , 'Admin@Feedback');
	Route::any('statistic' , 'Admin@Statistic');
	Route::any('depositions' , 'Admin@Deposition');
	Route::post('updateCheque' , 'Admin@UpdateCheque');
	Route::post('updateFeedback' , 'Admin@UpdateFeedback');
	Route::post('SetWinner' , 'Admin@SetWinner');
});

Route::any('apm', 'APM@Callback');
Route::any('vk', 'VK@Index');
