$(function ($) {


    $(".nicescroll-block").niceScrollExt({
        cursorwidth : 10,
        cursorcolor : "#21759B",
        cursoropacitymin : 1,
        cursorborder : "none",
        railpadding : {
            top:0,
            right: -12,
            left:0,
            bottom:0
        }
    });
    $(".nicescroll-block-page").niceScrollExt({
        cursorwidth : 10,
        cursorcolor : "#21759B",
        cursoropacitymin : 1,
        cursorborder : "none",
        railpadding : {
            top:0,
            right: 0,
            left:0,
            bottom:0
        }
    });




    $(".rules-page .btn-collapse-block").click(function () {
        var i = $(this).parents(".faq-block").find("div:first-child i");
        i.attr("class", "").addClass($(this).attr("aria-expanded") == "true" ? "icon-double-arrow-right" : "icon-double-arrow-down");
    });



    $("input[type=radio]").RadioTheme();
    $("input[type=checkbox]").CheckboxTheme();
    SelectTheme.Init();








    $(".nicescroll-block-lk-upload-check").niceScrollCor({
        xl: 440,
        lg: 440,
        md: 440,
        sm: "auto",
        xs: "auto",
    });

    $(".nicescroll-winners").niceScrollCor({
        xl: 410,
        lg: 410,
        md: 410,
        sm: "auto",
        xs: "auto",
    });











    $("input[type=file]").each(function () {
        var input = $(this);
        var text = input.parent().find("span");
        input.change(function () {
            var ar = $(this).val().split('\\');
            text.text(ar[ar.length - 1]);
        });
    });









});

























$.fn.niceScrollCor = function (e) {
    var main = $(this);
    if (main.length) {
        setInterval(Check, 100);
        $(window).resize(Check);
        function Check() {
            var w = $(window);
            if (w.width() >= 1200) {
                main.height(e.xl == "auto" ? "auto" : w.height() - e.xl);
            }
            else if (w.width() >= 992) {
                main.height(e.lg == "auto" ? "auto" : w.height() - e.lg);
            }
            else if (w.width() >= 768) {
                main.height(e.md == "auto" ? "auto" : w.height() - e.md);
            }
            else if (w.width() >= 576) {
                main.height(e.sm == "auto" ? "auto" : w.height() - e.sm);
            }
            else {
                main.height(e.xs == "auto" ? "auto" : w.height() - e.xs);
            }
        }
    }
};




function c(t) {
    console.log(t);
}

$.fn.niceScrollCor2 = function (e) {
    var main = $(this);


    if (main.length) {

        var idI = setInterval(function () {
            main.height(1);
        }, 10);


        setInterval(Check, 100);

        $(window).resize(function () {
            main.height(300);
            Check();
        });



        function Check() {
            clearInterval(idI);

            var w = $("main");
            if (w.width() >= 1200) {
                main.height(e.xl == "auto" ? "auto" : w.height() - e.xl);
            }
            else if (w.width() >= 992) {
                main.height(e.lg == "auto" ? "auto" : w.height() - e.lg);
            }
            else if (w.width() >= 768) {
                main.height(e.md == "auto" ? "auto" : w.height() - e.md);
            }
            else if (w.width() >= 576) {
                main.height(e.sm == "auto" ? "auto" : w.height() - e.sm);
            }
            else {
                main.height(e.xs == "auto" ? "auto" : w.height() - e.xs);
            }
        }
    }
};






$.fn.niceScrollExt = function (e) {
    var main = $(this);
    var wraper = $(this).find("> div");


    if (main.length) {
        main.niceScroll(e);
        setInterval(function () {
            main.getNiceScroll().resize();
            if (main.height() >= wraper.height()) {
                $(".nicescroll-rails-vr").hide();
                main.css({
                    "touch-action" : "manipulation"
                });
            } else {
                $(".nicescroll-rails-vr").show();
                main.css({
                    "touch-action" : "none"
                });
            }
        }, 100);
    }
};









$.fn.hasAttr = function (name) {
    return this.attr(name) !== undefined;
};


$.fn.CheckboxTheme = function () {
    $(this).each(function () {
        var main = $(this);
        var  mainParent = main.parent();
        var label = mainParent.find("label");

        if (mainParent.hasClass("checkbox-block")) {
            return false;
        }

        mainParent.addClass("checkbox-block").prepend("<i></i>");
        if (main.hasAttr("id") && label.length) label.attr("for", main.attr("id"));

        if (main.is(":checked") && main.is(":disabled")) {
            mainParent.addClass("checked-disabled");
        }
        else if (!main.is(":checked") && main.is(":disabled")) {
            mainParent.addClass("disabled");
        }
        else if (main.is(":checked") && !main.is(":disabled")) {
            mainParent.addClass("checked");
        }

        mainParent.find("label").removeAttr("for");

        mainParent.find("i").click(function () {


            if (main.is(":disabled")) return false;

            main.click();

            if (!main.is(":checked")) {
                //main.prop("checked", false);
                mainParent.removeClass("checked");
            }
            else {
                //main.prop("checked", true);
                mainParent.addClass("checked");
            }
            return false;
        });
    });
};


$.fn.RadioTheme = function () {
    $(this).each(function () {
        var main = $(this);
        var mainParent = main.parent();

        if (mainParent.hasClass("theme-radio")) return false;

        mainParent.addClass("theme-radio").prepend("<i></i>");

        if (main.hasAttr("checked") && main.hasAttr("disabled")) mainParent.addClass("checked-disabled");
        else if (!main.hasAttr("checked") && main.hasAttr("disabled")) mainParent.addClass("disabled");
        else if (main.hasAttr("checked") && !main.hasAttr("disabled")) mainParent.addClass("checked");

        mainParent.find("label").removeAttr("for");

        mainParent.find("i").click(function () {

            var input = $(this).parent().find("input");
            var name = input.attr("name");
            var allInputs = $("input[name=" + name + "]");
            allInputs.each(function () {
                $(this).prop("checked", false).parent().removeClass("checked");
            });
            input.prop("checked", true).parent().addClass("checked");
        });
    });
};


var SelectTheme = {
    Flag: false,
    Init: function () {
        this.Build();
        $(document).click(function (e) {
            SelectTheme.main.each(function () {
                var b = $(this).parent();
                if (!b.has(e.target).length) b.find("ul").hide();
            });
        });
    },
    Reload: function (id) {
        $(id).parent().parent().html($(id).remove());
        SelectTheme.Init();
    },
    Build: function () {
        this.main = $("select");
        this.main.each(function () {
            var main = $(this);
            if ($(this).parent().hasClass("theme_select")) return true;
            $(this).wrap("<div class='theme_select'></div>");
            var optionAr = $(this).find("option");
            var defaultOptions = $(this).find(":selected").text();
            defaultOptions = defaultOptions.length ? defaultOptions : $(this).hasAttr("placeholder") ? $(this).attr("placeholder") : "";
            var newSelect = "<div onclick='SelectTheme.OpenList(this)'><div class='ts_selected_item'>" + defaultOptions + "</div><div class='ts_arrow'><span class='icon-arrow-down'></span></div></div><ul class='ts_list'>";
            for (var i = 0; i < optionAr.length; i++) {
                if ($(optionAr[i]).is(":selected")) {
                    newSelect += "<li class='selected' value='" + $(optionAr[i]).val() + "'>" + $(optionAr[i]).text() + "</li>";
                }
                else {
                    newSelect += "<li value='" + $(optionAr[i]).val() + "'>" + $(optionAr[i]).text() + "</li>";
                }
            }
            newSelect += "</ul>";

            $(this).parent().append(newSelect);

            main.parent().find("ul li").click(function () {
                main.find("[selected]").prop("selected", false);
                main.parent().find("[class=selected]").removeAttr("class");
                $(this).addClass("selected");
                main.parent().find(".ts_selected_item").html(main.find($(this).attr("value").length ? '[value="' + $(this).attr("value") + '"]' : "option:first-child").prop("selected", true).html());
                $(this).parent().hide();
                main.change();
            });

            SelectTheme.Cor(main);
            $(window).resize(function() {
                SelectTheme.Cor(main);
            });
        });
    },
    Cor: function (main) {
        var w = main.parent().width();
        main.parent().find(".ts_selected_item").width(w - 32 - 40);
    },
    OpenList: function (btn) {
        SelectTheme.Flag = true;
        var list = $(btn).next();
        if (list.is(":visible")) list.hide();
        else list.show();
        SelectTheme.Flag = false;
    }
};
