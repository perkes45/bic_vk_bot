$(function () {
    var $owl;

    if ($.fn.owlCarousel) {
        $owl = $('.js-slider-main').owlCarousel({
            items: 1,
            nav: false,
            dots: true,
            autoplay: true,
            loop: true,
            responsive: {
                0: {
                    items: 1
                }
            }
        });


    }

    $('.main-menu ul li a').click(function(){
        $('body').removeClass('opened-menu');
    })

    $('.hover-block').hover(function(){
        $('.hover-block').not($(this)).addClass('inactive').removeClass('active');
        $(this).removeClass('inactive');
        $(this).addClass('active');
    }, function(){
        $(this).removeClass('active');
    })

    $('.hover-render').hover(function(){}, function(){
        $('.hover-block').removeClass('inactive').removeClass('active');
    })

    if ($.fn.paginathing) {
        $('.js-pagination').paginathing({
            perPage: 4,
            firstText: '',
            lastText: '',
            insertAfter : '.js-winners-pag'
        });
        $('.js-pagination1').paginathing({
            perPage: 6,
            firstText: '',
            lastText: '',
            insertAfter : '.js-winners-pag1'
        });
        $('.js-pagination2').paginathing({
            perPage: 6,
            firstText: '',
            lastText: '',
            insertAfter : '.js-winners-pag2'
        });
    }

    $('input, select, textarea').on('change keypress paste keyup', function(){
        if ($(this).val()) {
            $(this).addClass('has-val');
        } else {
            $(this).removeClass('has-val');
        }
    })


    $('ul.header-menu li a').click(function(){
        $('body').removeClass('opened-menu');
    })



    if ($('#infoMain').length) {
        $('#infoMain').modal('show');
    }
    if ($('#infoWeekly').length) {
        $('#infoWeekly').modal('show');
    }

    $('.js-toggle-block-header').click(function () {
        let parent = $(this).closest('.js-toggle-block-row');
        parent.toggleClass('opened-week')
        $('.js-toggle-block-row').not(parent).removeClass('opened-week');
    })

    $('.js-header-menu').click(function () {
        $('body').removeClass('opened-menu');
    });
    $('.js-tab-link').click(function () {
        if ($(this).attr('data-toggle')) {
            $(this).toggleClass('active');
            $($(this).attr('href')).toggle();
        } else {
            $('.js-tab-link').not($(this)).removeClass('active');
            $(this).addClass('active');
            $('.js-tab').not($(this).attr('href')).hide();
            $($(this).attr('href')).show();
        }

        return false;
    });
    $('.js-cabinet-btn').click(function () {
        $('.js-cabinet-btn').not($(this)).removeClass('active');
        $(this).addClass('active');

        if ($(this).attr('href') == '#tab_1') {
            $('.tab_2').addClass('hidden');
            $('.tab_1').removeClass('hidden');
        } else {
            $('.tab_1').addClass('hidden');
            $('.tab_2').removeClass('hidden');
        }

        return false;
    });
    $('.js-menu-toggler').click(function () {
        $('body').toggleClass('opened-menu');
        return false;
    });
    $('.winners-week').click(function () {
        var parent = $(this).closest('.winners-row');
        parent.toggleClass('inactive');
        $('.winners-row').not(parent).addClass('inactive');
    });

    $('.js-open').click(function () {
        $('.js-open').not($(this)).each(function () {
            $(this).closest('.js-open-wrap').removeClass('opened-item');
        });
        $(this).closest('.js-open-wrap').toggleClass('opened-item');
    });
    $('.js-file-input').on('change', function () {
        var name = $(this).val();

        if (name) {
            $(this).closest('.js-file-wrap').find('.js-file-name').text(name);
        }
    });
    $('.js-data-wrap').click(function () {
        $('.js-data-wrap').not($(this)).removeClass('opened-info');
        $(this).toggleClass('opened-info');
    });
    if ($.fn.scrollbar) {
        $('.js-scroll').scrollbar({
            scrollx: false
        });
    }

    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        if (scroll >= 1) {
            $('body').addClass("scrolled");
        } else {
            $('body').removeClass("scrolled");
        }
    });

    // function updateShops(city, setCenter) {
    //     var wrap = $('.js-shops-render');
    //     $.ajax({
    //         method: 'POST',
    //         headers: {'X-CSRF-TOKEN': $('meta[name="token"]').attr('content')},
    //         data: {city: city},
    //         dataType: 'json',
    //         url: '/lookup',
    //         success: function (result) {
    //             if (result.length) {
    //                 wrap.empty();
    //                 if (setCenter) {
    //                     myMap.setCenter([result[0].Lat, result[0].Lng], 8);
    //                 }
    //                 console.log(result);
    //                 $.each(result, function (index, value) {
    //                     var num = index + 1;
    //                     wrap.append('<div class="row mb-2"><div class="col-2">' + num + '</div><div class="col-10">' + value.ShopAddress + '</div></div>');
    //                 });
    //             }
    //         }
    //     });
    // }
    // updateShops('Москва');


    $('.js-get-value').click(function(){
        var val = $(this).attr('data-value');
        $('#js-set-value').text(val);
    })

    $('.js-pass-toggle').click(function(){
        var input = $(this).closest('form').find('[name="Pass"]');
        if (input.attr('type') == 'password') {
            input.attr('type', 'text'); 
        } else {
            input.attr('type', 'password'); 
        }
    })

    $('.js-city-selector').change(function () {
        var city = $(this).val();
        updateShops(city, true);
    })

    $('.js-apm-show').click(function () {
        $('#apm-scan-qr').find('.apm-block-btn.apm-action-button').click();
        return false;
    })


    $('body').on('submit', '.js-form', function () {

        var self = $(this),
        id = self.attr('id');

        $('body').addClass('loading');

        self.find('.error').hide();

        var data = new FormData(),
        action = self.attr('data-action');

        self.find('input, textarea , select').each(function () {
            if (!$(this).attr('disabled')) {
                if ($(this).attr('type') == 'file') {
                    if (this.files.length) {
                        data.append($(this).attr('name'), this.files[0]);
                    }
                } else {
                    if ($(this).attr('type') == 'checkbox' || $(this).attr('type') == 'radio') {
                        if ($(this).is(':checked')) {
                            data.append($(this).attr('name'), $(this).val());
                        }
                    } else {
                        data.append($(this).attr('name'), $(this).val());
                    }
                }
            }
        });

        if (self.attr('no-reload')) {
            reload = false;
        } else
        reload = true;

        $.ajax({
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            timeout: 60000,
            dataType: 'json',
            url: self.attr('action'),
            method: self.attr('method'),
            success: function (e) {
                $('body').removeClass('loading');
                if (e.error) {

                    self.find('.error').show();



                    if (action == 'alert') {
                        alert(e.message);
                    } else if (action == 'alert-refresh') {
                        alert(e.message);
                    } else {
                        $('body').removeClass('loading');
                        self.find('.error').empty();
                        $('.js-remove-error').remove();

                        if (typeof e.message === 'object') {
                            for (var i in e.message) {
                                var parent = self.find('[name="'+i+'"]').closest('.custom-row');
                              
                                if (parent.length) {
                                    parent.append('<div class="error js-remove-error">'+e.message[i][0]+'</div>')
                                } else {
                                    self.find('.error').not('.js-remove-error').append('<div>' + e.message[i] + '</div>')    
                                }
                                
                            }
                        } else {
                            self.find('.error').not('.js-remove-error').text(e.message)
                        }
                    }
                } else {
                    if (id == 'registration') {
                    }
                    if (id == 'uploadform') {

                    }
                    if (id == 'feedbackform') {

                    }
                    if (id == 'loginform') {
                    }

                    reload = true;
                    if (self.attr('id') == 'reset-form') {
                        $('.js-email').text(e.message);
                    }

                    if (action == 'alert-refresh') {
                        alert(e.message);
                    } else if (action == 'alert') {
                        alert(e.message);
                    } else if (action == 'location') {
                        location.href = self.attr('data-location');
                    } else if (action == 'modal') {
                        self.closest('.modal').modal('hide');
                        $(self.attr('data-target')).modal('show');
                        $('.js-modal-body').html(e.message);
                        console.log(234);
                    } else {
                        window.location.reload();
                    }

                }
            },
            error: function (e) {
                $('body').removeClass('loading');
                if (self.attr('data-target')) {
                    $(self).closest('.modal').modal('hide');
                    $(self.attr('data-target')).modal('show');
                    $('.js-modal-body').html('<div class="error" style="display: block;">Что-то пошло не так, попробуйте позднее.</div>');
                } else {
                    self.find('.error').show().text('Что-то пошло не так, попробуйте позднее.');
                }
            }
        });
        try {
            grecaptcha.reset(recaptcha1);
            grecaptcha.reset(recaptcha2);
            grecaptcha.reset(recaptcha3);
        } catch (err) {
            console.error(err);
        }
        return false;
    });

$('#static').on('hidden.bs.modal', function () {
    console.log(reload);
    location.reload();
});

$('.modal').on('shown.bs.modal', function () {
    $('body').removeClass('menu-opened').addClass('modal-open');
})
if ($.fn.paginate) {
var pag = $('.js-pag').paginate({
    'elemsPerPage':10,
    'maxButtons' : 3
});

$('.js-p-type').change(function(){
    $('.js-s').removeClass('d-table-row').removeClass('d-none');
    var val = $(this).val();
    if (val) {
        $('.js-s').addClass('d-none');
        $('.js-s[data-ptype="'+val+'"]').addClass('d-table-row');
    }

    $('.js-pag').paginate('destroy');

})

$('.js-p-email').click(function(){
    $('.js-s').removeClass('d-table-row').removeClass('d-none');
    var val = $(this).parent().find('input').val();
    if (val){
        $('.js-s').each(function(){
            if ($(this).attr('data-search').indexOf(val) >= 0){
                $(this).addClass('d-table-row');
            } else {
                $(this).addClass('d-none');
            }
        })
    }

    $('.js-pag').paginate('destroy');
})

}

})
