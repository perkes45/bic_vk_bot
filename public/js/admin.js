$(function(){
    if($('.js-status-select').val() == 'Rejected'){
        $('.js-hide-reject').hide();
    }
    $('body').on('change' , '.js-status-select' , function(){
        var val = $(this).val();
        var input = $(this).closest('.js-status-wrap').find('.js-reason');
        if (val == 'Rejected') {
            input.show();
            $('.js-hide-reject').hide();
        } else {
            input.hide();
            $('.js-hide-reject').show();
        }
    })
    
    var dadataToken = '0cd26ad806163ab4b224da78ce1b8d211f9f495d',
                dadataServiceUrl = 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/';
    
    
    $('#City').suggestions({
                    token: dadataToken,
                    type: 'ADDRESS',
                    hint: false,
                    bounds: 'city-settlement',                    
                    onSelect: function (suggestion) {
                        $(this).attr('data-selected', 'true');
                    }
                });
})