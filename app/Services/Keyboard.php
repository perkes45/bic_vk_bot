<?php

namespace App\Services;

use App\Models\FaqCategory;
use App\Models\FaqItem;
use App\Models\User;
use App\Models\VKUser;
use \App\Models\Prize;

class Keyboard {

    protected $client;

    public function __construct(VKUser $client) {
        $this->client = $client;
    }

    public function __call(string $name, array $arguments) {

        if (method_exists($this, $name)) {
            $result = call_user_func_array(array($this, $name), $arguments);
//            if ($name == 'RenderMainMenu') {
//                $this->client->update(['LastForm' => serialize($result)]);
//            }
            return $result;
        }
    }

    public function PrizesMenu(){
       
        $keyboard = [
            'one_time' => false,
            'buttons' => [

            ]
        ];
     

        $keyboard['buttons'][] = [
                [
                    "action" => [
                        "type" => "text",
                        "payload" => '{"button": "prizesHistory"}',
                        "label" => 'Получить приз'
                    ],
                    "color" => "primary"
                ]
            ];

        $keyboard['buttons'][] = [
                [
                    "action" => [
                        "type" => "text",
                        "payload" => '{"button": "feedback"}',
                        "label" => 'Задать вопрос'
                    ],
                    "color" => "primary"
                ]
            ];

        $keyboard['buttons'][] = [
            [
                "action" => [
                    "type" => "text",
                    "payload" => '{"button": "MainMenu"}',
                    "label" => 'В главное меню'
                ],
                "color" => "positive"
            ]
        ];
        return json_encode($keyboard, JSON_UNESCAPED_UNICODE);
    }


    public function PrizesMenuAll() {
        $keyboard = [
            'one_time' => false,
            'buttons' => [

            ]
        ];

        $keyboard['buttons'][] = [
                [
                    "action" => [
                        "type" => "text",
                        "payload" => '{"button": "winners"}',
                        "label" => 'Победители'
                    ],
                    "color" => "positive"
                ]
            ];
        $keyboard['buttons'][] = [
                [
                    "action" => [
                        "type" => "text",
                        "payload" => '{"button": "art"}',
                        "label" => 'Творческий конкурс'
                    ],
                    "color" => "positive"
                ]
            ];
        $keyboard['buttons'][] = [
            [
                "action" => [
                    "type" => "text",
                    "payload" => '{"button": "MainMenu"}',
                    "label" => 'В главное меню'
                ],
                "color" => "primary"
            ]
        ];
        return json_encode($keyboard, JSON_UNESCAPED_UNICODE);
    }

    public function GarantMenu(){
        $keyboard = [
            'one_time' => false,
            'buttons' => [

            ]
        ];
        $keyboard['buttons'][] = [
                [
                    "action" => [
                        "type" => "text",
                        "payload" => '{"button": "feedbackPrizes"}',
                        "label" => 'Задать вопрос'
                    ],
                    "color" => "primary"
                ]
            ];
        $keyboard['buttons'][] = [
            [
                "action" => [
                    "type" => "text",
                    "payload" => '{"button": "MainMenu"}',
                    "label" => 'В главное меню'
                ],
                "color" => "positive"
            ]
        ];
        return json_encode($keyboard, JSON_UNESCAPED_UNICODE);
    }

    public function DailyMenu(){
        $keyboard = [
            'one_time' => false,
            'buttons' => [

            ]
        ];

        $daily = $this->client->user->winners()->where('WinType', 'daily')->first();
        if ($daily && $daily->infoNeeded()) {
            $keyboard['buttons'][] = [
                [
                    "action" => [
                        "type" => "text",
                        "payload" => '{"button": "setYMoney"}',
                        "label" => 'Получить приз'
                    ],
                    "color" => "primary"
                ]
            ];
        }

        $keyboard['buttons'][] = [
                [
                    "action" => [
                        "type" => "text",
                        "payload" => '{"button": "feedbackPrizes"}',
                        "label" => 'Задать вопрос'
                    ],
                    "color" => "primary"
                ]
            ];
        $keyboard['buttons'][] = [
            [
                "action" => [
                    "type" => "text",
                    "payload" => '{"button": "MainMenu"}',
                    "label" => 'В главное меню'
                ],
                "color" => "positive"
            ]
        ];
        return json_encode($keyboard, JSON_UNESCAPED_UNICODE);
    }

    public function RenderPrizesForCheque($id){
        $prizes = Prize::where('PrizeType', 'garant')->get();
        $keyboard = [
            'one_time' => false,
            'buttons' => [

            ]
        ];
        foreach ($prizes as $prize){
            $keyboard['buttons'][] = [
                [
                    "action" => [
                        "type" => "text",
                        "payload" => '{"button": "setPrize", "id" : "'.$prize->Id.'_'.$id.'"}',
                        "label" => $prize->PrizeName
                    ],
                    "color" => "primary"
                ]
            ];
        }
        $keyboard['buttons'][] = [
            [
                "action" => [
                    "type" => "text",
                    "payload" => '{"button": "MainMenu"}',
                    "label" => 'В главное меню'
                ],
                "color" => "positive"
            ]
        ];
        return json_encode($keyboard, JSON_UNESCAPED_UNICODE);
    }

    public function RenderPrizesForChequeAuchan($id){
        $prizes = Prize::where('PrizeType', 'garant_aushan')->get();
        $keyboard = [
            'one_time' => false,
            'buttons' => [

            ]
        ];
        foreach ($prizes as $prize){
            $keyboard['buttons'][] = [
                [
                    "action" => [
                        "type" => "text",
                        "payload" => '{"button": "setPrize", "id" : "'.$prize->Id.'_'.$id.'"}',
                        "label" => $prize->PrizeName
                    ],
                    "color" => "primary"
                ]
            ];
        }
        $keyboard['buttons'][] = [
            [
                "action" => [
                    "type" => "text",
                    "payload" => '{"button": "MainMenu"}',
                    "label" => 'В главное меню'
                ],
                "color" => "positive"
            ]
        ];
        return json_encode($keyboard, JSON_UNESCAPED_UNICODE);
    }
    
    public function FaqRootMenu(){
        $categories = FaqCategory::all();
        $keyboard = [
            'one_time' => false,
            'buttons' => [

            ]
        ];
        foreach ($categories as $cat){
            $keyboard['buttons'][] = [
                [
                    "action" => [
                        "type" => "text",
                        "payload" => '{"button": "faqCategory", "id" : "'.$cat->Id.'"}',
                        "label" => $cat->Name
                    ],
                    "color" => "primary"
                ]
            ];
        }
        $keyboard['buttons'][] = [
            [
                "action" => [
                    "type" => "text",
                    "payload" => '{"button": "MainMenu"}',
                    "label" => 'В главное меню'
                ],
                "color" => "positive"
            ]
        ];
        return json_encode($keyboard, JSON_UNESCAPED_UNICODE);
    }
    
    public function FaqCategory($id, $page = 0){
        $faqs = FaqItem::where('CategoryId', $id)->orderBy('Id', 'ASC');
        $keyboard = [
            'one_time' => false,
            'buttons' => [

            ]
        ];
        if ($page){
            $faqs->where('Id', '>', $page);
        }
        $faqs->take(5);
        $faqs = $faqs->get();
        foreach ($faqs as $faq){
            $keyboard['buttons'][] = [
                [
                    "action" => [
                        "type" => "text",
                        "payload" => '{"button": "faqItem", "id" : "'.$faq->Id.'"}',
                        "label" => $faq->Title
                    ],
                    "color" => "primary"
                ]
            ];
        }
        // if (!$page || ($page) + 5 <= $faqs->max('Id')){
        //     $keyboard['buttons'][] = [
        //         [
        //             "action" => [
        //                 "type" => "text",
        //                 "payload" => '{"button": "faqCategory", "id" : "'.$id.'", "paginate" : "' . $faqs->max('Id') . '"}',
        //                 "label" => 'Далее'
        //             ],
        //             "color" => "default"
        //         ]
        //     ];
        // }
        if ($page && $page > FaqItem::where('CategoryId', $id)->min('Id')){
         $keyboard['buttons'][] = [
            [
                "action" => [
                    "type" => "text",
                    "payload" => '{"button": "faqCategory", "id" : "'.$id.'", "paginate" : "' . ($faqs->min('Id') - 6) . '"}',
                    "label" => 'Назад'
                ],
                "color" => "default"
            ]
        ]; 
    }
    $keyboard['buttons'][] = [
        [
            "action" => [
                "type" => "text",
                "payload" => '{"button": "faq"}',
                "label" => 'Выбрать категорию FAQ'
            ],
            "color" => "positive"
        ]
    ];
    return json_encode($keyboard, JSON_UNESCAPED_UNICODE);

}

private function RenderStartKeyboard() {
    $keyboard = [
        'one_time' => false,
        'buttons' => [
            [
                [
                    "action" => [
                        "type" => "text",
                        "payload" => "{\"button\": \"start\"}",
                        "label" => "Старт"
                    ],
                    "color" => "positive"
                ]
            ]
        ]
    ];
    return json_encode($keyboard, JSON_UNESCAPED_UNICODE);
}

private function PhoneExistsKeyboard() {
        //return '{"buttons":[],"one_time":true}';
    $keyboard = [
        'one_time' => false,
        'buttons' => [
            [
                [
                    "action" => [
                        "type" => "text",
                        "payload" => '{"button": "feedback"}',
                        "label" => "Написать в поддержку"
                    ],
                    "color" => "positive"
                ],
                [
                    "action" => [
                        "type" => "text",
                        "payload" => '{"button": "MainMenu"}',
                        "label" => "В главное меню"
                    ],
                    "color" => "negative"
                ]
            ]
        ]
    ];
    return json_encode($keyboard, JSON_UNESCAPED_UNICODE);
}

private function HideKeyboard() {
        //return '{"buttons":[],"one_time":true}';
    $keyboard = [
        'one_time' => false,
        'buttons' => [
            [
                [
                    "action" => [
                        "type" => "text",
                        "payload" => '{"button": "MainMenu"}',
                        "label" => "В главное меню"
                    ],
                    "color" => "negative"
                ]
            ]
        ]
    ];
    return json_encode($keyboard, JSON_UNESCAPED_UNICODE);
}

//        $text .= "Подцели : " . $this->GetSPList()."\r\n\r\n";
//        $text .= "Ваши ощущения при достижении цели : " . $this->Impressions."\r\n\r\n";
//        $text .= "Запасная цель : " . $this->AddPurpose."\r\n\r\n";       
//        $text .= "Визуализация : ";


private function MainMenu() {
    $keyboard = [
        'one_time' => false,
        'buttons' => [
            [
                [
                    "action" => [
                        "type" => "text",
                        "payload" => '{"button": "about"}',
                        "label" => "Об акции"
                    ],
                    "color" => "positive"
                ]
            ]

            ]
        
    ];

    if ($this->client->user && $this->client->user->cheques->count()){
        $keyboard['buttons'][] = [
                [
                    "action" => [
                        "type" => "text",
                        "payload" => '{"button": "cheques"}',
                        "label" => "Посмотреть мои чеки"
                    ],
                    "color" => "positive"
                ]
            ];
    }

    $daily = false; 
    if ($this->client->user) {
        $winner = $this->client->user->winners()->where(['WinType' => 'daily'])->first();
        if ($winner && $winner->infoNeeded()) {
            $daily = true; 
        }
    }

    if ($this->client->user && $this->client->user->winners->count()) {

                $keyboard['buttons'][] = [
                        [
                        "action" => [
                            "type" => "text",
                            "payload" => '{"button": "myprizes"}',
                            "label" => "Мои призы"
                        ],
                        "color" => "positive"
                    ]
                ];
            }

    if (($this->client->user && $this->client->user->cheques()->count() < 5000) || !$this->client->user) {

            $keyboard['buttons'][] = [
                [
                    "action" => [
                        "type" => "text",
                        "payload" => '{"button": "uploadCheque"}',
                        "label" => "Зарегистрировать чек"
                    ],
                    "color" => "primary"
                ]
            ];
        }
    return json_encode($keyboard, JSON_UNESCAPED_UNICODE);
}


private function FaqItem(){
        $keyboard = [
        'one_time' => false,
        'buttons' => [
            [
                [
                    "action" => [
                        "type" => "text",
                        "payload" => '{"button": "MainMenu"}',
                        "label" => "😊 Все понятно! Вернуться в начало."
                    ],
                    "color" => "positive"
                ]
            ], [
                [
                    "action" => [
                        "type" => "text",
                        "payload" => '{"button": "faq"}',
                        "label" => "❓Вернуться к выбору категории вопроса"
                    ],
                    "color" => "positive"
                ]
            ], [
                [
                    "action" => [
                        "type" => "text",
                        "payload" => '{"button": "feedback"}',
                        "label" => "😒 Не получилось найти информацию"
                    ],
                    "color" => "positive"
                ]
            ]
        ]
    ];
    return json_encode($keyboard, JSON_UNESCAPED_UNICODE);
}

private function RouterMenu() {
    $keyboard = [
        'one_time' => false,
        'buttons' => [
            [
                [
                    "action" => [
                        "type" => "text",
                        "payload" => '{"button": "ncp"}',
                        "label" => "Механика акции"
                    ],
                    "color" => "positive"
                ]
            ,
                [
                    "action" => [
                        "type" => "text",
                        "payload" => '{"button": "prizes"}',
                        "label" => "Призы"
                    ],
                    "color" => "positive"
                ]
            ], [
                [
                    "action" => [
                        "type" => "text",
                        "payload" => '{"button": "faq"}',
                        "label" => "Вопросы/FAQ"
                    ],
                    "color" => "positive"
                ]
                
            ,
                [
                    "action" => [
                        "type" => "text",
                        "payload" => '{"button": "art"}',
                        "label" => "Творческий конкурс"
                    ],
                    "color" => "positive"
                ]
                
            ],[
                [
                    "action" => [
                        "type" => "text",
                        "payload" => '{"button": "rules"}',
                        "label" => "Полные правила"
                    ],
                    "color" => "positive"
                ]
                
            ], [
                [
                    "action" => [
                        "type" => "text",
                        "payload" => '{"button": "MainMenu"}',
                        "label" => "Главное меню"
                    ],
                    "color" => "primary"
                ]
                
            ]
        ]
    ];
    return json_encode($keyboard, JSON_UNESCAPED_UNICODE);
}

private function MagnitMenu() {
    $keyboard = [
        'one_time' => false,
        'buttons' => [
            [
                [
                    "action" => [
                        "type" => "text",
                        "payload" => '{"button": "mechMagnit"}',
                        "label" => "Механика акции"
                    ],
                    "color" => "positive"
                ]
            ],[
                [
                    "action" => [
                        "type" => "text",
                        "payload" => '{"button": "rules"}',
                        "label" => "Полные правила"
                    ],
                    "color" => "positive"
                ]
            ],[
                [
                    "action" => [
                        "type" => "text",
                        "payload" => '{"button": "faq"}',
                        "label" => "Вопросы / FAQ"
                    ],
                    "color" => "positive"
                ]
            ]
        ]
    ];
    return json_encode($keyboard, JSON_UNESCAPED_UNICODE);
}
private function NCPMenu() {
    $keyboard = [
        'one_time' => false,
        'buttons' => [
            [
                [
                    "action" => [
                        "type" => "text",
                        "payload" => '{"button": "mechNCP"}',
                        "label" => "Механика акции"
                    ],
                    "color" => "positive"
                ]
            ],[
                [
                    "action" => [
                        "type" => "text",
                        "payload" => '{"button": "rules"}',
                        "label" => "Полные правила"
                    ],
                    "color" => "positive"
                ]
            ],[
                [
                    "action" => [
                        "type" => "text",
                        "payload" => '{"button": "faq"}',
                        "label" => "Вопросы / FAQ"
                    ],
                    "color" => "positive"
                ]
            ]
        ]
    ];
    return json_encode($keyboard, JSON_UNESCAPED_UNICODE);
}


private function MainMenuAdd() {
    $keyboard = [
        'one_time' => false,
        'buttons' => [
            [
                [
                    "action" => [
                        "type" => "text",
                        "payload" => '{"button": "about"}',
                        "label" => "Узнать об акции"
                    ],
                    "color" => "positive"
                ],
                [
                    "action" => [
                        "type" => "text",
                        "payload" => '{"button": "uploadCheque"}',
                        "label" => "Зарегистрировать чек"
                    ],
                    "color" => "positive"
                ]
            ]
        ]
    ];
    return json_encode($keyboard, JSON_UNESCAPED_UNICODE);
    if (!$this->client->UserId) {
        $keyboard['buttons'][] = [
            [
                "action" => [
                    "type" => "text",
                    "payload" => '{"button": "login"}',
                    "label" => "Вход"
                ],
                "color" => "primary"
            ]
        ];
        $keyboard['buttons'][] = [
            [
                "action" => [
                    "type" => "text",
                    "payload" => '{"button": "registration"}',
                    "label" => "Регистрация"
                ],
                "color" => "negative"
            ]
        ];
    } else {
        $keyboard['buttons'][] = [
            [
                "action" => [
                    "type" => "text",
                    "payload" => '{"button": "cheques"}',
                    "label" => "Мои чеки"
                ],
                "color" => "primary"
            ]
        ];
        if ($this->client->user->cheques()->count() < 10) {

            $keyboard['buttons'][] = [
                [
                    "action" => [
                        "type" => "text",
                        "payload" => '{"button": "uploadCheque"}',
                        "label" => "Загрузить чек"
                    ],
                    "color" => "primary"
                ]
            ];
        }

        if ($this->client->user->winners()->where('WinType', 'garant')->whereNull('PrizeId')->count() ) {

            $keyboard['buttons'][] = [
                [
                    "action" => [
                        "type" => "text",
                        "payload" => '{"button": "selectPrizes"}',
                        "label" => "Выбрать гарантированные призы"
                    ],
                    "color" => "primary"
                ]
            ];
        }

        if ($this->client->user->winners()->where('WinType', 'garant_aushan')->whereNull('PrizeId')->count() ) {

            $keyboard['buttons'][] = [
                [
                    "action" => [
                        "type" => "text",
                        "payload" => '{"button": "selectPrizesAuchan"}',
                        "label" => "Выбрать гарантированные призы АШАН"
                    ],
                    "color" => "primary"
                ]
            ];
        }

        if ($this->client->user->winners()->where('WinType', 'okey')->whereNull('PrizeId')->count() ) {

            $keyboard['buttons'][] = [
                [
                    "action" => [
                        "type" => "text",
                        "payload" => '{"button": "selectPrizesAuchan"}',
                        "label" => "Выбрать гарантированные призы ОКЕЙ"
                    ],
                    "color" => "primary"
                ]
            ];
        }

        if ($this->client->user->infoNeeded() ) {

            $keyboard['buttons'][] = [
                [
                    "action" => [
                        "type" => "text",
                        "payload" => '{"button": "infoUpdate"}',
                        "label" => "Заполнить данные для получения приза"
                    ],
                    "color" => "negative"
                ]
            ];
        }
    }
    return json_encode($keyboard, JSON_UNESCAPED_UNICODE);
}



}
