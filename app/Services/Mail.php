<?php

namespace App\Services;

//use Illuminate\Http\Request;
//use Carbon\Carbon;
//use Validator;
//use App\Models\User;
//use App\Models\Cheque;
//use App\Models\Prize;
//use App\Models\Winner;
//use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Mail as MailFacade;

class Mail {

    public static function SendRegistrationMail($email, $link, $name) {
        MailFacade::send('emails.registration', ['Link' => $link, 'Name' => $name, 'email' => $email], function($message) use ($email) {
            $message->to($email)->subject('Поздравляем c регистрацией!');
        });
    }

    public static function SendResetMail($email, $pass, $name) {
        MailFacade::send('emails.reset', ['email' => $email, 'Password' => $pass, 'Name' => $name], function($message) use ($email) {
            $message->to($email)->subject('Пароль восстановлен!');
        });
    }

    public static function SendFeedbackMail($email, $name) {
        MailFacade::send('emails.feedbackuser', ['email' => $email, 'name' => $name], function($message) use ($email) {
            $message->to($email)->subject('Вопрос получен, ответ уже в работе.');
        });
    }

    public static function SendWinMail($email , $subject, $code) {
       $body = view('emails.win', ['code' => $code])->render(); 
       MailFacade::send([], [],  function($message) use ($email, $subject, $body) {
            $message->to($email)->subject($subject)->setBody($body, 'text/html');;
        }); 
    }

    public static function SendMainWinMail() {
        
    }

    public static function SendDailyWinMail($email, $name) {
        MailFacade::send('emails.windaily', ['email' => $email, 'name' => $name], function($message) use ($email) {
            $message->to($email)->subject('Приз 3000 рублей');
        });
    }

    public static function SendWeeklyWinMail($email, $name) {
        MailFacade::send('emails.winweekly', ['email' => $email, 'name' => $name], function($message) use ($email) {
            $message->to($email)->subject('Еженедельный приз');
        });
    }

    public static function SendVKCode($email, $code) {
        $body = view('emails.code', ['code' => $code])->render();
        $subject = 'Регистрация в акции Epica: «Больше хочется - больше сбудется!»';
        MailFacade::send([], [], function($message) use ($email, $subject, $body) {
            $message->to($email)->subject($subject)->setBody($body, 'text/html');
        });
    }

}
