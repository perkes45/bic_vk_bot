<?php

namespace App\Services;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Validator;
use App\Models\User;
use App\Models\Cheque;
use App\Models\Prize;
use App\Models\Winner;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use App\Models\Code;
use \App\Services\Mail;

class Admin {

    public static function SetChequeStatus($data) {
        $errors = [];

        $cheque = Cheque::find($data['ChequeId']);
        if (!$cheque)
            $errors[] = 'Чек не найден';
        if (!in_array($data['Status'], [
                    Cheque::STATUS_ACCEPTED,
                    Cheque::STATUS_NEW,
                    Cheque::STATUS_REJECTED
                ])) {
            $errors[] = 'Невалидный статус';
        }

        if ($cheque->winners->count() && $data['Status'] != Cheque::STATUS_ACCEPTED)
            $errors[] = 'Невозможно изменить статус. Чек уже назначен победителем';

        if ($errors)
            return ['errors' => 1, 'message' => $errors];

        $cheque->update([
            'Status' => $data['Status'],
            'ModeratorProcessed' => Carbon::now(),
            'RejectReasonId' => $data['RejectReasonId'] ?? null
        ]);


        if ($data['Status'] == Cheque::STATUS_ACCEPTED && !$cheque->winners->count()) { //Механика назначения кода  
            try {
                if ($cheque->user->winners()->where('WinType', 'daily')->count()) {
                    return ['error' => 1, 'message' => 'Победитель не назначен: пользователь уже выигрывал купон. Статус чека обновлен'];
                }
                $code = Code::whereNull('WinnerId')->first();
                if (!$code) {
                    return ['error' => 1, 'message' => 'Нет доступных кодов'];
                } else {
                    $prize = Prize::findOrFail(1);
                    $winner = Winner::create([
                                'UserId' => $cheque->user->Id,
                                'WinType' => $prize->PrizeType,
                                'PrizeId' => $prize->Id,
                                'ChequeId' => $cheque->Id,
                                'Status' => Winner::TYPE_CONFIRMED
                    ]);
                    $code->update(['WinnerId' => $winner->Id]);
                    Mail::SendWinMail($winner->cheque->user->Email, 'Вы выиграли подписку на сервис Яндекс.Музыка', $code->Value);
                    return ['error' => 0, 'message' => 'Статус обновлен. Назначен код - ' . $code->Value . '. Сообщение отправлено'];
                }
            } catch (\Exception $ex) {
                return ['error' => 1, 'message' => $ex->getMessage()];
            }
        }


        return ['error' => 0, 'message' => 'Обновлено успешно'];
    }

    public static function SetWinner($data) {
        $errors = [];
        foreach ($data['PrizeId'] as $id) {
            $cheque = Cheque::find($data['ChequeId']);
            if (!$cheque)
                $errors[] = 'Чек не найден';
            $prize = Prize::find($id);
            if (!$prize)
                $errors[] = 'Приз не найден';
            if ($cheque->Status != 'Accepted')
                $errors[] = 'Чек еще не подтвержден';

            if ($cheque->user->winners()->where(['WinType' => $prize->PrizeType])->count())
                $errors[] = 'Пользователь уже выигрывал приз с типом ' . $prize->PrizeCategory;

            if (!$errors) {
                // $chequeTypes = [
                //     'weekly' => 'WeeklyChequeId',
                //     'main' => 'MainChequeId',
                //     'daily' => 'DailyChequeId'
                // ];
                // $type = $chequeTypes[$prize->PrizeType];
                // $cheque->user->$type = $cheque->Id;
                // $cheque->user->save();

                Winner::create([
                    'UserId' => $cheque->user->Id,
                    'WinType' => $prize->PrizeType,
                    'PrizeId' => $prize->Id,
                    'ChequeId' => $cheque->Id,
                    'Status' => Winner::TYPE_PROCESSING
                ]);
            }
        }
        if ($errors)
            return ['errors' => 1, 'message' => $errors];
        else
            return ['error' => 0, 'message' => 'Победитель назначен'];
    }

}
