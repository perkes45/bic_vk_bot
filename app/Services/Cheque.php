<?php

namespace App\Services;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Validator;
use App\Models\User;
use App\Models\Cheque as Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;

class Cheque {

    public static function check(Request $request) {

        if (Auth::user()->Blocked) {
            return ['error' => 1, 'message' => 'Вы заблокированы за подозрительную активность. Обратитесь к организатору акции через <a href="/feedback">Обратную связь</a>'];
        }

        $validator = Validator::make($request->all(), [
                    'ChequeImage' => 'required|image|mimes:jpeg,png,jpg,bmp|max:5120',
                    'ChequeNumber' => 'required|digits_between:1,8',
                    'ShopId' => 'required',
                    'BuyDate' => 'required|date',
                    'ShopName' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return [
                'error' => 1,
                'message' => $errors->getMessages(),
            ];
        }
        $image = $request->ChequeImage ?? null;
        if (!$image instanceof UploadedFile) {
            return ['error' => 1, 'message' => 'Нет валидного фото чека'];
        }

        return ['error' => 0, 'message' => 0];
    }

    public static function upload(Request $request, $userId = null) {

        $image = $request->ChequeImage;
        $filename = md5(time()) . '.' . $image->extension();
        $image->move(public_path('cheques'), $filename);

        Model::create([
            'ChequeImage' => $filename,
            'ChequeNumber' => $request->ChequeNumber,
            'ShopId' => $request->ShopId,
            'BuyDate' => Carbon::parse($request->BuyDate)->format('Y-m-d'),
            'ShopName' => $request->ShopName,
            'UserId' => $userId ?? Auth::user()->Id,
            'Status' => Model::STATUS_NEW
        ]);

        return ['error' => 0, 'message' => 'Ваш чек отправлен на модерацию. Загружайте больше чеков и получите шанс выиграть один из призов. Больше чеков – больше шансов! Удачи!'];
    }

    public static function GetWeeks() {
        $startDate = new Carbon(env('ACTION_START'));
        $now = Carbon::now();

        $weeksCount = $now->diffInWeeks($startDate);
        return $weeksCount;
    }

}
