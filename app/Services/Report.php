<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Models\Cheque;
use App\Models\Transaction;
use Maatwebsite\Excel\Classes\LaravelExcelWorksheet;

class Report {

    public static function Download($columns, $dataset, $name) {
	$excel = app('excel');
	$exportedReport = $excel->create($name);
	$exportedReport->sheet('Данные', function (LaravelExcelWorksheet $exportedSheet) use ($columns, $dataset) {	    
	    $exportedSheet->row(1, array_values($columns))->row(1, function ($row) {
		$row->setBackground('#ccccaa');
	    });
	    foreach ($dataset as $i => $row) {
		$data = array_replace($columns, (array)$row);
		$exportedSheet->row($i + 2, $data);
	    }
	});
	$exportedReport->export();
	return redirect()->back();
    }

}
