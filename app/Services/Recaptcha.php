<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services;

use GuzzleHttp\Client as GuzzleClient;

class Recaptcha {

	public static function checkCaptcha($captcha) {
		$client = new GuzzleClient;
		$response = $client->request('POST', 'https://www.google.com/recaptcha/api/siteverify', [
			'form_params' => [
				'secret'   => env('RECAPTCHA_SECRET'),
				'response' => $captcha,
			],
		]);
		$body = $response->getBody();
		$result = json_decode($body->getContents());

		if (!isset($result->success) || !$result->success) {
			return ['error'   => 1,
					'message' => 'Вы не заполнили дополнительное поле, оно необходимо для проверки, что Вы не являетесь роботом.',
			];
		} else {
			return ['error'   => 0,
					'message' => '',
			];
		}
	}

}
