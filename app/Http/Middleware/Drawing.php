<?php

namespace App\Http\Middleware;

use Illuminate\Cookie\Middleware\EncryptCookies as Middleware;
use Illuminate\Http\Request;
use Closure;
use \App\Models\Drawing as Model;

class Drawing extends Middleware
{
    public function handle($request, Closure $next)
    {
        
        $activeDrawing = Model::getActive();
        \View::share(compact('activeDrawing'));
        
        
        
        
        return $next($request);
        
         
        
        
    }
}
