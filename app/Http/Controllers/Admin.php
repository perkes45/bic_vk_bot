<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Validator;
use App\Models\User;
use App\Models\Group;
use App\Models\Cheque;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\UploadedFile;
use App\Models\Prize;
use App\Models\Winner;
use App\Models\Feedback;
use Maatwebsite\Excel\Classes\LaravelExcelWorksheet;
use App\Services\Admin as Service;
use App\Models\RejectReason;
use Illuminate\Support\Facades\DB;
use App\Services\Report;

/**
 * Description of Auth
 *
 * @author r_zhalialov
 */
class Admin extends BaseController {

    public function Index(Request $request) {

        $attrs = [
            'ReceiptId' => 'Id чека', 
            'Id' => 'Id участника', 
            'Email' => 'Email',
            'CreatedAt' => 'Дата регистрации чека',
		      'ChequeImage' => 'Фото чека',
            'Status' => 'Статус чека',
            'SKUCount' => 'Кол-во SKU в чеке',
            'IsWinner' => 'Чек выиграл',

            
        ];

        $query = Cheque::join('Users', 'UserId', '=', 'Users.Id')->leftJoin('Winners', 'Winners.ChequeId', '=', 'Cheques.Id')->select([
            'Users.Email',
            'Cheques.CreatedAt',
            'Cheques.SKUCount',
            'Cheques.Id',
            'Cheques.ReceiptId',
            'Cheques.Status',
        ]);

        if ($request->method() == $request::METHOD_POST) {

            $total = $query->count();

            if ($request->has('draw')) {
                $data = [];
                $orderColumnIndex = $request->order[0]['column'];
                $orderColumn = $request->columns[$orderColumnIndex]['data'];
                $orderDir = $request->order[0]['dir'];

                foreach ($request->columns as $col) {
                    $prefix = $col['data'] == 'CreatedAt' ? 'Cheques' : null;
                    if ($col['search']['value'])
                        $query->where($prefix . '.' . $col['data'], 'LIKE', '%' . $col['search']['value'] . '%');
                }

                $filtered = $query->count();

                if ($orderColumn == 'CreatedAt') {
                    $orderColumn = 'Cheques.CreatedAt';
                }
                $dataset = $query->orderBy($orderColumn, $orderDir)->skip($request->start)->take($request->length)->get();

                $reasons = RejectReason::all();
                $prizes = Prize::where('PrizeType', '!=', 'daily')->get();
                foreach ($dataset as $row) {
                    $cheque = Cheque::find($row->Id);
			$img = '';
                foreach ($cheque->GetImages() ?? [] ?? [] as $imgi) {
                        $img .=    '<a href="'.$imgi.'" target="_blank" class="cheque-link">Фото</a>';
                }

                    $count = Cheque::where(['ChequeNumber' => $row->ChequeNumber])->count();
                    $prepared = array_replace($attrs, $row->toArray());
                    $prepared['CreatedAt'] = \Carbon\Carbon::parse($row->CreatedAt)->format('d.m.Y H:i:s');
                    $prepared['Status'] = view('admin.helpers.status', ['cheque' => $cheque, 'reasons' => $reasons])->render();
                    $prepared['IsWinner'] = $cheque->winners->count() ? 'Да' : 'Нет';
			$prepared['ChequeImage'] = $img;
                    $data[] = $prepared;
                }

                return [
                    "recordsTotal" => $total,
                    "recordsFiltered" => $filtered,
                    'data' => $data
                ];
            }

            if ($request->has('export')) {
                $result = $query->get()->toArray();
                foreach ($result as $i => $row){
		
                    $cheque = Cheque::find($row['Id']);
                    $result[$i]['CreatedAt'] = \Carbon\Carbon::parse($row['CreatedAt'])->format('d.m.Y H:i:s');
                    $result[$i]['Status'] = view('admin.helpers.status', ['cheque' => $cheque])->render();
                    $result[$i]['IsWinner'] = $cheque->winners->count() ? 'Да' : 'Нет';
                }
                $attrs['Id'] = 'Id';
                $attrs['Status'] = 'Статус';
                Report::Download($attrs, $result, 'Статистика по чекам');
                return redirect()->back();
            }

            if ($request->has('Status')) {
                return Service::SetChequeStatus($request->all());
            } elseif ($request->has('PrizeId')) {
                return Service::SetWinner($request->all());
            }
        }

        return view('admin.index', [
            'active' => 'admin',
            'attrs' => $attrs
        ]);
    }

    public function UserEdit(Request $request, $id) {
        $user = User::findOrFail($id);
        if ($request->method() == 'POST') {
            $data = $request->all();
            $validator = \Validator::make($data, [
                'LastName' => 'required|cyrillic|min:1|max:200',
                'FirstName' => 'required|cyrillic|min:1|max:200',
                'BirthDate' => 'date',
                'Phone' => 'required|phone',
                'City' => 'required',
            ]);


            if ($validator->fails()) {
                $errors = $validator->errors();
                return [
                    'error' => 1,
                    'message' => $errors->getMessages()
                ];
            } else {
                $data['Phone'] = clearPhone($data['Phone']);
                if ($request->Phone != $user->Phone && User::where('Id', '!=', $user->Id)->where(['Phone' => $data['Phone']])->first()) {
                    return ['error' => 1, 'message' => 'Пользователь с таким номером телефона уже существует'];
                }
                $age = Carbon::now()->diffInYears(Carbon::parse($data['BirthDate']));
                if ($age < 18) {
                    $message = 'Вы не можете зарегистрироваться, т.к. Вам менее 18 лет.';
                    return ['error' => 1, 'message' => $message];
                }
                $user->update($data);
                return ['error' => 0, 'message' => 'Пользователь обновлен'];
            }
        }
        return view('admin.user', ['user' => $user]);
    }

    public function Users(Request $request) {

        $attrs = [
            'Id' => 'Id',
	    'Uid' => 'Uid',
            'Email' => 'Email',
            'FirstName' => 'Имя',
            'Phone' => 'Телефон',
		'ActivationHash' => 'Ссылка для подтверждения',
            'CreatedAt' => 'Дата регистрации',
            'LastActionAt' => 'Дата последнего входа',

        ];

        $query = User::select(array_keys($attrs));

        $attrs['ChequesCount']  = 'Кол-во загруженных чеков';

        $total = $query->count();
        if ($request->has('draw')) {
            $data = [];
            $orderColumnIndex = $request->order[0]['column'];
            $orderColumn = $request->columns[$orderColumnIndex]['data'];
            $orderDir = $request->order[0]['dir'];

            foreach ($request->columns as $col) {
                if ($col['search']['value'])
                    $query->where($col['data'], 'LIKE', '%' . $col['search']['value'] . '%');
            }

            $filtered = $query->count();

            $dataset = $query->orderBy($orderColumn, $orderDir)->skip($request->start)->take($request->length)->get();

            foreach ($dataset as $row) {
                $user = User::find($row->Id);
                $dataset = array_replace($attrs, $row->toArray());
		$dataset['ActivationHash'] = '<a href="/activate/'.$row['ActivationHash'].'" target="_blank">Подтвердить</a>';
                $dataset['ChequesCount'] =  $user->cheques->count();
                $data[] = $dataset;
            }

            return [
                "recordsTotal" => $total,
                "recordsFiltered" => $filtered,
                'data' => $data
            ];
        }

        if ($request->method() == $request::METHOD_POST) {
            if ($request->has('export')) {
                $dataEx = $query->get()->toArray();
                foreach ($dataEx as $key => $value) {
                    $user = User::find($value['Id']);
                    $dataEx[$key]['ChequesCount'] =  $user->cheques->count();
                }
                Report::Download($attrs, $dataEx, 'Статистика участников');
            }
        }

        return view('admin.users', ['active' => 'users', 'attrs' => $attrs]);
    }

    public function Winners(Request $request) {

        if ($request->has('delete')) {
            $winner = Winner::find($request->delete);
            if ($winner) {
                $winner->delete();
                return redirect()->back();
            }
        }

        $attrs = [
            'Id' => 'Id',
            'CreatedAt' => 'Дата/время присвоения приза',
            'FatherName' => 'ФИО',
		  'ChequeImage' => 'Фото чека',
            'WinnerPhone' => 'Номер телефона участника',
            'Email' => 'E-mail участника',
            'PrizeName' => 'Наименование приза',
            'Status' => 'Статус',
            'Delete' => 'Удалить'

        ];

        $query = Winner::join('Cheques', 'ChequeId', '=', 'Cheques.Id')
        ->join('Users', 'Cheques.UserId', '=', 'Users.Id')
        ->join('Prizes', 'PrizeId', '=', 'Prizes.Id')                
        ->select([
            'Winners.Id',
            'Winners.CreatedAt',
            'Users.Email',
            'Prizes.PrizeName',
            'Winners.Status',
            'Winners.FatherName',
            'Winners.WinnerPhone',
        ]);

        $total = $query->count();
        if ($request->has('draw')) {
            $data = [];
            $orderColumnIndex = $request->order[0]['column'];
            $orderColumn = $request->columns[$orderColumnIndex]['data'];
            $orderDir = $request->order[0]['dir'];

            foreach ($request->columns as $col) {
                if ($col['search']['value'])
                    $query->where($col['data'], 'LIKE', '%' . $col['search']['value'] . '%');
            }

            $filtered = $query->count();

            $dataset = $query->orderBy($orderColumn, $orderDir)->skip($request->start)->take($request->length)->get();

            foreach ($dataset as $row) {
		$img = '';
		$cheque =  Winner::find($row->Id)->cheque;
		foreach ($cheque->GetImages() ?? [] ?? [] as $imgi) {
	                $img .=    '<a href="'.$imgi.'" target="_blank" class="cheque-link">Фото</a>';
                }

                $dataset = array_replace($attrs, $row->toArray());
                $dataset['Status'] = view('admin.helpers.winner', ['id' => $row->Id, 'status' => $row->Status])->render();
                $dataset['Delete'] = '<a href="?delete='.$row->Id.'">Удалить</a>';
		          $dataset['ChequeImage'] = $img; 
                $data[] = $dataset;
            }

            return [
                "recordsTotal" => $total,
                "recordsFiltered" => $filtered,
                'data' => $data
            ];
        }

        if ($request->method() == $request::METHOD_POST) {
            if ($request->has('exportCSV')) {
                $wnrs = Winner::whereNotNull('YWallet')->get();
                $report = [];
                foreach ($wnrs as $value) {
                    $report[] = [
                         $value->WinnerPhone,
                         'Кошелек - '.strval($value->YWallet),
                         $value->WinType == 'Специальный' ? $value->PriceWC : 1000,
                         $value->UserId,
                         1
                    ];
                }
                Report::Download([
                    'Phone',
                    'Wallet',
                    'Sum',
                    'UserID',
                    'Activity'
                ], $report, 'Статистика для начисления');
            }
            if ($request->has('export')) {
		$dataEE = $query->get()->toArray();
		foreach ($dataEE as $k => $rowEE) {
			$dataEE[$k]['YWallet'] = 'Кошелек - '.strval($rowEE['YWallet']);
		}
                Report::Download($attrs, $dataEE, 'Статистика победителей');
            } else {

                try {
                    $winner = Winner::find($request->WinnerId);                    
                    $winner->Status = $request->Status;
                    $winner->save();


                    return ['error' => 0, 'message' => 'Статус обновлен'];
                } catch (\Exception $ex) {
                    return ['error' => 1, 'message' => $ex->getMessage()];
                }
            }
        }

        return view('admin.winners', ['active' => 'winners', 'attrs' => $attrs]);
    }

    public function WinnersPrice(Request $request){
       try {
        $winner = Winner::find($request->WinnerId);                    
        $winner->Price = $request->Price;
        $winner->save();


        return ['error' => 0, 'message' => 'Стоимость обновлена'];
    } catch (\Exception $ex) {
        return ['error' => 1, 'message' => $ex->getMessage()];
    }
}

public function WinnersPriceWC(Request $request){
       try {
        $winner = Winner::find($request->WinnerId);                    
        $winner->PriceWC = $request->PriceWC;
        $winner->save();


        return ['error' => 0, 'message' => 'Стоимость обновлена'];
    } catch (\Exception $ex) {
        return ['error' => 1, 'message' => $ex->getMessage()];
    }
}

public function Feedback(Request $request) {
    $attrs = [
        'UserId' => 'Id пользователя',
        'CreatedAt' => 'Дата/время отправки',
        'Image' => 'Изображение',
        'Email' => 'E-mail',
	'Theme' => 'Тема',
        'Message' => 'Текст сообщения',
        'UserId' => 'Ссылка',
        'Answered' => 'Отвечено'
    ];
    $select = array_merge(array_keys($attrs), ['Id']);
    $query = Feedback::select($select);

    $total = $query->count();
    if ($request->has('draw')) {
        $data = [];
        $orderColumnIndex = $request->order[0]['column'];
        $orderColumn = $request->columns[$orderColumnIndex]['data'];
        $orderDir = $request->order[0]['dir'];

        foreach ($request->columns as $col) {
            if ($col['search']['value'])
                $query->where($col['data'], 'LIKE', '%' . $col['search']['value'] . '%');
        }

        $filtered = $query->count();

        $dataset = $query->orderBy($orderColumn, $orderDir)->skip($request->start)->take($request->length)->get();

        foreach ($dataset as $row) {

        	

            $dataset = array_replace($attrs, $row->toArray());
            $dataset['Answered'] = view('admin.helpers.answered', ['answered' => $row->Answered, 'id' => $row->Id])->render();
            $dataset['Status'] = $row->Answered ? 'success' : '';
            $dataset['UserId'] = '<a href="https://vk.com/id'.$row->UserId.'" target="_blank">Перейти к профилю</a>';
            $dataset['Image'] = view('admin.helpers.image', ['image' => env('APP_URL') . '/images/feedback/' . $row->Image])->render();
            $data[] = $dataset;
        }

        return [
            "recordsTotal" => $total,
            "recordsFiltered" => $filtered,
            'data' => $data
        ];
    }

    if ($request->has('export')) {
        $result = $query->get()->toArray();
        Report::Download($attrs, $result, 'Статистика обратной связи');
        return redirect()->back();
    }
    return view('admin.feedback', ['active' => 'feedback', 'attrs' => $attrs]);
}

public function Stat(Request $request) {

    $dateFrom = $request->DateFrom ? Carbon::parse($request->DateFrom) : Carbon::parse('2020-08-17');
    $dateTo = $request->DateTo ? Carbon::parse($request->DateTo) : Carbon::now();

    $result = [];
    $users = User::where('CreatedAt', '>', $dateFrom->format('Y-m-d 00:00:01'))->where('CreatedAt', '<', $dateTo->format('Y-m-d 23:59:59'))->count();
    $cheques = Cheque::where('CreatedAt', '>', $dateFrom->format('Y-m-d 00:00:01'))->where('CreatedAt', '<', $dateTo->format('Y-m-d 23:59:59'))->where('Approved', 1)->count();
    $result['users'] = $users;
	$result['all_cheques'] = Cheque::where('CreatedAt', '>', $dateFrom->format('Y-m-d 00:00:01'))->where('CreatedAt', '<', $dateTo->format('Y-m-d 23:59:59'))->count();
   $result['rejected_cheques'] = Cheque::where('CreatedAt', '>', $dateFrom->format('Y-m-d 00:00:01'))->where('CreatedAt', '<', $dateTo->format('Y-m-d 23:59:59'))->where('Status', 'REVIEWED')->where('Approved', 0)->count();
   $result['users_with_cheques'] = User::join('Cheques', 'UserId', '=', 'Users.Id')->where('Users.CreatedAt', '>', $dateFrom->format('Y-m-d 00:00:01'))->where('Users.CreatedAt', '<', $dateTo->format('Y-m-d 23:59:59'))->distinct('UserId')->count('UserId');
   $result['users_with_valid_cheques'] = User::join('Cheques', 'UserId', '=', 'Users.Id')->where('Users.CreatedAt', '>', $dateFrom->format('Y-m-d 00:00:01'))->where('Users.CreatedAt', '<', $dateTo->format('Y-m-d 23:59:59'))->where('Approved', 1)->distinct('UserId')->count('UserId');
    $result['cheques'] =  $cheques;
    $result['cheques_per_user'] = $users ? $cheques/$users : 0;
    $result['users_50'] = Winner::where('CreatedAt', '>', $dateFrom->format('Y-m-d 00:00:01'))->where('CreatedAt', '<', $dateTo->format('Y-m-d 23:59:59'))->where('WinType', 'garant')->count();
    $result['users_3000'] = Winner::where('CreatedAt', '>', $dateFrom->format('Y-m-d 00:00:01'))->where('CreatedAt', '<', $dateTo->format('Y-m-d 23:59:59'))->where('WinType', 'daily')->count();
    $result['users_1000000'] = Winner::where('CreatedAt', '>', $dateFrom->format('Y-m-d 00:00:01'))->where('CreatedAt', '<', $dateTo->format('Y-m-d 23:59:59'))->where('WinType', 'weekly')->count();
    return view('admin.stat', ['active' => 'stat', 'result' => $result, 'dateFrom' => $dateFrom, 'dateTo' => $dateTo ]);
}

public function UpdateFeedback(Request $request) {
    $feedback = Feedback::find($request->FeedbackId);
    if (!$feedback)
        return ['error' => 1, 'message' => 'Чек не найден'];

    $answered = ($request->has('Answered') && $request->Answered == 1) ? 1 : null;
    $feedback->Answered = $answered;
    $feedback->save();

    return ['error' => 0, 'message' => 'Обнолвено успешно'];
}

public function UpdateCheque(Request $request) {
    $fields = $request->all();
    $rules = [];
    $cheque = Cheque::find($request->ChequeId);
    foreach ($fields as $k => $v) {
        if ($k == 'ChequeNumber')
            $rules[$k] = 'required|min:1|max:8';
        elseif ($k == 'TradePoint')
            $rules[$k] = 'required';
        elseif ($k == 'BuyDate') {
            $rules[$k] = 'required|date';
            $fields[$k] = Carbon::parse($v);
        } else {
            unset($fields[$k]);
        }
    }
    $validator = Validator::make($fields, $rules);

    if ($validator->fails()) {
        $errors = $validator->errors();
        return [
            'error' => 1,
            'message' => $errors->getMessages(),
        ];
    } else {
        $cheque->update($fields);
        $cheque->save();
        return ['error' => 0, 'message' => 'Чек обновлен'];
    }
}

}
