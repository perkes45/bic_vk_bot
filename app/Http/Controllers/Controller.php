<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Models\Winner;
use Illuminate\Http\Request;
use App\Models\Feedback;
use Validator;
use App\Models\LookupShop;
use App\Models\ShopsLookup;
use Illuminate\Support\Facades\Mail;
use \Carbon\Carbon;
use App\Services\Mail as MailService;
use App\Models\User;
use App\Models\Request as Req;

class Controller extends BaseController {

    public function __construct() {
        if (Carbon::parse('now')->lt(Carbon::parse('28.08.2019')) && request()->cookie('dev') != 'sv43v45v345') {            
            echo view('promo')->render();
            die();
        }
    }

	public function Starter(Request $request){
		$req = json_encode($request->header());
		Req::create(['Payload' => 'welcome_'.$req]);
		return redirect('/');
	}

    public function Eland(Request $request){
        Req::create(['Payload' => json_encode($request->all())]);
        try {
            $winner = Winner::where('UserId', $request->accountId)->firstOrFail();
            $winner->update([
                'WinnerStatus' => $request->success ? 'Успешная выплата' : 'Ошибка - '.$request->message,
                'Bank' => json_encode($request->all())
            ]);
            return 'ok';
        } catch (\Exception $ex){
            \Log::error($ex->getMessage());
            return 'error';
        }
    }

    public function Unsubscribe($email) {
        $user = User::where(['Email' => $email])->first();
        if (!$user) {
            $message = 'Некорректная ссылка';
        } else if ($user->DistribAccept) {
            $user->update(['DistribAccept' => 0]);
            $message = 'Вы отписались от e-mail рассылок.';
        } else {
            $message = 'Почтовый ящик был отписан от  e-mail рассылок ранее.';
        }

        return view('unsubscribe', ['message' => $message, 'retail' => env('DEFAULT_RETAIL')]);
    }

    public function Index(Request $request) {
        $data = \Session::get('RegData') ?? [];

        return view('welcome', [
            'active' => 'main',
            'autoOpen' => $data ? true : false, 
            'data' => $data
        ]);
    }
    
    public static function GetCityList() {
		$res = ShopsLookup::groupBy('ShopCity')->orderBy('ShopCity', 'ASC')->pluck('ShopCity')->toArray();
		$pre = array_flip($res);
		unset($pre['Москва']);
		$res = array_flip($pre);
		array_unshift($res, 'Москва');
		return $res;
	}
    
    public function Lookup(Request $request) {
		if ($request->city) {
			return self::GetCityShops($request->city);
		}
	}
    
    public static function GetCityShops($city = 'Москва') {
		return ShopsLookup::where(['ShopCity' => $city])->get()->toArray();
	}

    public function Feedback(Request $request) {
        if ($request->method() == 'POST') {

           $captcha = $request->get('g-recaptcha-response');
           $captchaCheck = \App\Services\Recaptcha::checkCaptcha($captcha);
           if ($captchaCheck['error'])
               return $captchaCheck;

            $validator = Validator::make($request->all(), [
                        'Email' => 'required|email',
                        //'RetailId' => 'required|exists:Retailers,Id',
                        'Message' => 'required|max:2000',
//                'PersonName' => 'required',
//                'Theme' => 'required',
                        'ProcessAccept' => 'required',
                        'Image' => 'file|mimes:jpeg,png,jpg,bmp|max:10240'
            ]);

            if ($validator->fails()) {
                $errors = $validator->errors();
                return ['error' => 1, 'message' => $errors];
            } else {

                $fields = [
                    'Email' => $request->Email,
                    'PersonName' => $request->PersonName,
                    'RetailId' => $request->RetailId,
                    'Theme' => $request->Theme,
                    'ProcessAccept' => $request->ProcessAccept ? 1 : 0,
                    'Message' => $request->Message,
                    'UserId' => \Auth::check() ? \Auth::user()->Id : null
                ];

                if ($request->has('Image')) {
                    $image = $request->Image;
                    $filename = md5(time()) . '.' . $image->extension();
                    $image->move(public_path('images/feedback'), $filename);
                    $fields['Image'] = $filename;
                }

                $feedback = Feedback::create($fields);

                Mail::send('emails.feedback', [
                    'Email' => $feedback->Email,
                    'Message' => $feedback->Message,
                    'UserId' => \Auth::check() ? \Auth::user()->Id : null,
                    'Theme' => $request->Theme,
                    'Name' => $feedback->PersonName,
                    'Image' => $feedback->Image ? url('/images/feedback/' . $feedback->Image) : null
                        ], function($message) {
                    $message->to(env('FEEDBACK_MAIL'))->subject('Обращение с сайта '.env('APP_URL'));
                });

                MailService::SendFeedbackMail($feedback->Email, $feedback->PersonName);

                return ['error' => 0, 'message' => 'Спасибо, ваше сообщение отправлено!'];
            }
        }

        return view('feedback', ['active' => 'faq']);
    }



    public function Winners() {
        $daily = Winner::where('WinType', 'daily')->orderBy('CreatedAt', 'DESC')->get();
        $weekly = Winner::where('WinType', 'weekly')->orderBy('CreatedAt', 'DESC')->get();
        return view('winners', ['active' => 'winners', 'daily' => $daily, 'weekly' => $weekly]);
    }






}
