<?php

namespace App\Http\Controllers;

use App\Models\Cheque;
use App\Models\Group;
use App\Models\PassReset;
use App\Models\Prize;
use App\Models\Response;
use App\Models\Request as Req;
use App\Models\Sert;
use App\Models\User;
use App\Models\Winner;
use App\Services\Cheque as ChequeService;
use App\Services\Mail as MailService;
use App\Services\Keyboard;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth as AuthFacade;
use Ramsey\Uuid\Uuid;
use VK\Client\VKApiClient;
use Validator;

/**
 * Description of Auth
 *
 * @author r_zhalialov
 */
class APM extends BaseController {

    public function Callback(Request $request) {

        Req::create(['Payload' => json_encode($request->all())]);
        if ($request->header('Token') == '9wsmrtc92469t2873469rt78239') {
            try {
                $user = User::where('Uid', $request->userUuid)->first();
                if ($user) {
                    $cheque = Cheque::where('Uid', $request->uuid)->first();


                    $skuCount = 0;
                    $skuArr = $request->answers['products'] ?? [];
                    foreach ($skuArr as $key => $sku) {                        
                        $skuCount += $sku['count'];                                                        
                    }

                    $data = [
                        'Uid' => $request->uuid,
                        'ReceiptId' => $request->receiptId,
                        'ChequeImage' => json_encode($request->photos),
                        'UserId' => $user->Id,
                        'SKUCount' => $skuCount,
                        'ShopName' => $request->retailer['name'] ?? '',
                        'Status' => $request->state,
                        'QRData' => json_encode($request->all()),
                        'Approved' => $request->approved ? 1 : 0,
                        'RejectReason' => $request->rejectReason ?? null
                    ];
                    if (!$cheque) {
                        $cheque = Cheque::create($data);
                    } else {
                        $cheque->update($data);
                    }

                    $vkId = $user->vKUsers()->first()->VKId;

                    if ($cheque->Approved ) {
                        $cnt = $user->cheques()->where('Approved', 1)->count();
                        

                        if ($cnt  == 1) {

                            $text = "Ура! 🎉 Ты уже зарегистрировал первый валидный чек!
                                Теперь ты можешь участвовать в творческом конкурсе и получить шанс выиграть 150 000 руб! 🎁
                                Твой номер: ".$vkId;
                            
                        } else {
                            $text = "Ура! 🎉 Еще один твой валидный чек зарегистрирован!
                                Продолжай участвовать в творческом конкурсе и увеличивай свои шансы выиграть 150 000 рублей! 🎁
                                Твой номер: ".$vkId;
                        }

                        $response = Response::create([
                            'Body' => $text
                        ]);
                        $lastRequestId = $response->Id;                            

                        $dataset = [
                            'user_id' => $vkId,
                            'message' => $text,
                            'random_id' => $lastRequestId,
                            'attachment' => 'photo-204950899_457239032'
                        ];

                        $vk = new VKApiClient('5.95');

                        $vk->messages()->send('e6b9a5ba80ddbfea1fbc9b8e0cae1ccd59e9e0afce284c75bf56576ff537641022f024c5fdaec4024cb39', $dataset);
                        
                    } else if ($request->state != 'ARRIVED') {
                        $text = "Что-то пошло не так 😕, твой чек не прошел модерацию. Попробуй повторить регистрацию или напиши модератору акции!";
                        $response = Response::create([
                            'Body' => $text
                        ]);
                        $lastRequestId = $response->Id;  



                        $keyboard = [
                            'one_time' => false,
                            'buttons' => [

                            ]
                        ];
                     

                        $keyboard['buttons'][] = [
                                [
                                    "action" => [
                                        "type" => "text",
                                        "payload" => '{"button": "uploadCheque"}',
                                        "label" => 'Зарегистрировать чек'
                                    ],
                                    "color" => "primary"
                                ]
                            ];

                        $keyboard['buttons'][] = [
                                [
                                    "action" => [
                                        "type" => "text",
                                        "payload" => '{"button": "feedback"}',
                                        "label" => 'Написать модератору'
                                    ],
                                    "color" => "primary"
                                ]
                            ];

                        $keyboard['buttons'][] = [
                            [
                                "action" => [
                                    "type" => "text",
                                    "payload" => '{"button": "MainMenu"}',
                                    "label" => 'В главное меню'
                                ],
                                "color" => "positive"
                            ]
                        ];
                        $keyboard = json_encode($keyboard, JSON_UNESCAPED_UNICODE);


                        $dataset = [
                            'user_id' => $vkId,
                            'message' => $text,
                            'random_id' => $lastRequestId,
                            'keyboard' => $keyboard
                        ];

                        $vk = new VKApiClient('5.95');

                        $vk->messages()->send('e6b9a5ba80ddbfea1fbc9b8e0cae1ccd59e9e0afce284c75bf56576ff537641022f024c5fdaec4024cb39', $dataset);
                    }




                    return json_encode(['externalId' => $cheque->Id]);
                } else {
                    return 'no user founded';
                }
            } catch (Exception $ex) {
                \Log::error($ex->getMessage());
            }
        } else {
            return 'Token missmatch';
        }
    }

}
