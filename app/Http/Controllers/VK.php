<?php

namespace App\Http\Controllers;

use App\Models\Group;
use App\Models\Response;
use App\Models\User;
use App\Models\VKUser;
use App\Models\Feedback;
use App\Models\Prize;
use App\Models\Drawing;
use App\Models\Winner;
use App\Services\Cheque;
use App\Services\Keyboard;
use App\Services\Mail;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Validator;
use VK\Client\VKApiClient;
use App\Models\Cheque as ChequeModel;
use Carbon\Carbon;
use Ramsey\Uuid\Uuid;


class VK extends BaseController {

    const KEY = 'e6b9a5ba80ddbfea1fbc9b8e0cae1ccd59e9e0afce284c75bf56576ff537641022f024c5fdaec4024cb39';
    const SECRET = '9wrj7t69c7n40982s034589n';

    private $vk;
    private $log;
    private $response;
    private $lastRequestId;
    private $userId;
    private $client;
    private $kb;
    private $message;
    private $fly_id;
    private $action;
    private $dataset;
    private $custom_msg;
    private $msg_id;
    private $paginate = null;

    const STOP_ACTIONS = [
        'MainMenu', 'feedback'
    ];

    public function Index(Request $request) {

        if (!isset($request->secret)) {
            return 'ok';
        }
        if ($request->secret != self::SECRET) {
            return 'ok' . $request->secret;
        }
        try {
            //\Log::error(json_encode($request->all()));
            if ($request->has('type') && $request->type == 'message_new') {
                $this->vk = new VKApiClient('5.95');

                $this->dataset = $request->all();

                $this->custom_msg = $request->custom_msg ?? null;
//                $this->log = ReqModel::create([
//                            'Body' => json_encode($this->dataset, JSON_UNESCAPED_UNICODE),
//                ]);
                $this->CheckForClient($this->dataset);
//
                $btn = json_decode($this->dataset['object']['payload'] ?? '');
                $this->message = $this->dataset['object']['text'] ?? '';
                $this->msg_id = $this->dataset['object']['id'] ?? '';
                if ($btn && $btn->button) {
                    $this->action = $btn->button;
                    $this->fly_id = $btn->id ?? null;
                    $this->paginate = $btn->paginate ?? null;
                }
                if ($this->client->State && !in_array($this->action, self::STOP_ACTIONS)) {
                    if (method_exists($this, $this->client->State)) {
                        $action = $this->client->State;
                        $this->$action();
                    } else {
                        $this->client->ClearState();
                        $this->message('К сожалению, такого статуса я не знаю :( ', $this->kb->MainMenu());
                        \Log::error('Unknown state - ' . $this->client->State);
                    }
                    return 'ok';
                }
                if ($this->action) {
                    if (method_exists($this, $this->action)) {
                        $action = $this->action;
                        $this->$action();
                    } else {
                        $this->client->ClearState();
                        $this->message('К сожалению, такой команды я не знаю :( ', $this->kb->MainMenu());
                        \Log::error('Unknown action - ' . $this->action);
                    }
                } else {


                    if (!$this->client->UserId) {
                        $this->message('Я не очень тебя понял, но модератор сможет тебе помочь! Укажи email адрес для обратной связи', $this->kb->HideKeyboard());
                        $this->client->update(['State' => 'WaitFeedbackEmail']);
                    } else {
                        $this->message('Я не очень тебя понял. Отправь свой вопрос в ответе на это сообщение, и организатор акции постарается тебе помочь.', $this->kb->HideKeyboard());
                        $this->client->update(['State' => 'WaitQuestion']);
                    }


                }
            }
        } catch (Exception $ex) {
            \Log::error($ex->getMessage());
        }

        return 'ok';
    }

    public function message($text, $keyboard = null, $attach = null) {
        $this->response = Response::create([
            'Body' => $text
        ]);
        $this->lastRequestId = $this->response->Id;
        $dataset = [
            'user_id' => $this->userId,
            'message' => $text,
            'random_id' => $this->lastRequestId,
        ];
        if ($keyboard) {
            $dataset['keyboard'] = $keyboard;
        }
        if ($attach) {
            \Log::error($attach);
            $dataset['attachment'] = $attach;
        }

        $this->vk->messages()->send(self::KEY, $dataset);
    }

    public function CheckForClient($dataset) {
        try {
            $userId = $dataset['object']['from_id'] ?? null;
            if (!$userId) {
                return;
            }
            $this->userId = $userId;

            $user = VKUser::where('VKId', $this->userId)->first();
            if (!$user) {
                $VKuser = $this->vk->users()->get(self::KEY, [
                    'user_id' => $userId
                ]);
                $userInfo = [
                    'FirstName' => $VKuser[0]['first_name'] ?? '',
                    'LastName' => $VKuser[0]['last_name'] ?? '',
                    'Dataset' => json_encode($VKuser[0] ?? []),
                    'VKId' => $userId,
                    'Code' => generateRandomCode()
                ];
                \Log::error(json_encode($userInfo));
                $this->client = VKUser::create($userInfo);
                $this->kb = new Keyboard($this->client);
                $this->message('Приветствуем тебя в акции BIC «ВСЕ включено!» 😊', null, 'photo-204950899_457239024');
                $this->message('Выбери раздел, который тебе интересен:', $this->kb->MainMenuAdd());
                die('ok');
            }
            $this->client = $user;
            $this->kb = new Keyboard($this->client);
        } catch (\Exception $ex) {
            \Log::error($ex->getMessage() . " UserId - $this->userId");
        }
    }

    public function MainMenu() {
        $this->client->ClearState();
        $this->message('Выбери пункт меню', $this->kb->MainMenu());
    }

    //ACTIONS

    public function feedback() {
        if (!$this->client->UserId) {
            $this->message('Укажи email адрес для обратной связи', $this->kb->HideKeyboard());
            $this->client->update(['State' => 'WaitFeedbackEmail']);
        } else {
            $this->message('Отправь свой вопрос в ответе на это сообщение, и организатор акции постарается тебе помочь.', $this->kb->HideKeyboard());
            $this->client->update(['State' => 'WaitQuestion']);
        }
    }

    public function feedbackPrizes() {

        $this->message('Напишите свой вопрос в ответном сообщении, и организатор акции постарается Вам помочь.', $this->kb->HideKeyboard());
        $this->client->update(['State' => 'WaitQuestion']);
        
    }

    public function prizes() {

        $message = "Посмотри, какие крутые призы ты можешь выиграть! 🎁🎁🎁
            Каждый день разыгрывается 5 сертификатов OZON на 1000 рублей.
            Каждую неделю — 3 сертификата OZON на 4000 рублей.
            И это ещё не всё! Среди участников творческого конкурса разыгрывается специальный приз — 150 000 рублей!
            Удачи! 🤗";

        $this->message($message, $this->kb->PrizesMenuAll(), 'photo-204950899_457239023');
    }

    public function winners() {

        $text = "Победители ежедневного розыгрыша: \r\n\r\n";

        $winnersDaily = Winner::where('WinType', 'daily')->get();

        if (!$winnersDaily->count()){
            $text .= "Победителей пока нет";
        } else {
            foreach ($winnersDaily as $winner) {
                $text .= "Дата розыгрыша: ".$winner->drawing->FireAt.". Email победителя: ".hideEmail($winner->user->Email).".\r\n";
            }
        }

        $text .= "\r\nПобедители еженедельного розыгрыша: \r\n\r\n";

        $winnersWeekly = Winner::where('WinType', 'weekly')->get();

        if (!$winnersWeekly->count()){
            $text .= "Победителей пока нет";
        } else {
            foreach ($winnersWeekly as $winner) {
                $text .= "Дата розыгрыша: ".$winner->drawing->FireAt.". Email победителя: ".hideEmail($winner->user->Email).".\r\n";
            }
        }

        $this->message($text);
    }

    public function drawings() {

        $message = '';
        $winners = Winner::where('WinType', 'weekly')->orderBy('CreatedAt', 'ASC')->get();
        if (!$winners->count()) {
            $message = 'Розыгрыши пока не проводились';
        } else {


            foreach ($winners as $winner) {
                $drawing = $winner->drawing;
                $message .= "Неделя ".$drawing->PeriodStart." - ".$drawing->PeriodEnd." приз: ".$winner->Price." ₽\r\n";
            }
        }

        $this->message($message);
    }

    public function registration() {

        $this->client->update(['State' => 'WaitFirstName']);
        $this->message('Напиши свое Имя', $this->kb->HideKeyboard());
    }

    public function login() {
        $this->client->update(['State' => 'WaitLoginEmail']);
        $this->message('Введите email', $this->kb->HideKeyboard());
    }

    public function cheques() {
        $this->message($this->client->RenderCheques(), $this->kb->MainMenu());
    }

    public function myprizes() {
        $this->message('Выбери пункт меню:', $this->kb->PrizesMenu());
    }

    public function garant() {
        $this->message('Поздравляем! Вам начислен приз 50 рублей. Приз будет отправлен в ближайшее время на номер телефона указанный при регистрации. 

Если у вас остались вопросы, смело обращайтесь в Службу обратной связи', $this->kb->GarantMenu());
    }


    public function prizesHistory() {
        $winners = $this->client->user->winners;

        $message = '';

        foreach ($winners as $winner) {
            $message .= 'Поздравляем! 🎉🎉🎉 Ты выиграл приз - '.$winner->prize->PrizeName."🎁\r\n\r\n";
            if ($sert = $winner->serts()->first()) {
                $message .= 'Номер сертификата - '.$sert->Number."\r\n";
                $message .= 'Код сертификата - '.$sert->Pin."\r\n";
                $message .= 'Годен до - '.$sert->ExpireDate."\r\n\r\n";
            }
        }

        $message .= 'Если у тебя остались вопросы, смело обращайся в Службу обратной связи.';

        $this->message($message, null, 'photo-204950899_457239032');
    }

    public function weekly() {
        $winner = $this->client->user->winners()->where('WinType', 'weekly')->first();
        $price = 0;
        if ($winner) {
            $price = $winner->Price;
        } 
        $this->message('Поздравляем! Вы можете получить Еженедельный приз! 

Размер Еженедельного приза — '.$price.' рублей, без учета НДФЛ 35%. В течение одного дня с вами свяжется наш модератор и сообщит дальнейшие шаги для получения приза. 

ВАЖНО! Чтобы объявить вас победителем, нам необходим пакет документов, указанный в п.7.1.2. Правил Акции.', $this->kb->GarantMenu());
    }

    public function daily() {
        $this->message('Поздравляем! Вы можете получить приз 3000 рублей на ваш электронный кошелек ЮMoney! Для этого выберите в меню пункт «Получить приз» и введите данные кошелька в течение 5 рабочих дней после розыгрыша.

Если у вас остались вопросы, смело обращайтесь в Службу обратной связи.', $this->kb->DailyMenu());
    }

    public function uploadCheque() {
        if (!$this->client->UserId) {
            $this->message('Для участия в акции тебе необходимо предоставить данные о себе', $this->kb->HideKeyboard());
            $this->registration();
        } else {
            $this->client->update(['State' => 'WaitImage']);
            $this->message('Загрузи фото чека 📸', $this->kb->HideKeyboard());
        }

    }

    public function infoUpdate(){
        $this->client->update(['State' => 'WaitWinnerLastName']);
        $this->message('Для получения приза вам необходимо заполнить следующие данные:'."\r\n".'Фамилия:' , $this->kb->HideKeyboard());
    }

    public function setYMoney(){
        $this->client->update(['State' => 'WaitYMoney']);
        $this->message('Чтобы перевод состоялся, введите номер своего электронного кошелька ЮMoney целиком, без пробелов и дефисов.
Если у вас нет электронного кошелька ЮMoney, вы можете создать его, перейдя по ссылке. - https://yoomoney.ru/.' , $this->kb->HideKeyboard());
    }

    public function selectPrizes(){
        $this->message('Выберите чек', $this->kb->RenderChequesForPrizeSelect());
    }

    public function selectPrizesAuchan(){
        $this->message('Выберите чек', $this->kb->RenderChequesForPrizeSelectAuchan());
    }

    public function chequePrize(){
        $this->message('Выберите приз', $this->kb->RenderPrizesForCheque($this->fly_id));
    }

    public function chequePrizeAuchan(){
        $this->message('Выберите приз', $this->kb->RenderPrizesForChequeAuchan($this->fly_id));
    }

    public function setPrize(){
        $ids = explode('_', $this->fly_id);
        $prizeId = $ids[0];
        $winnerId = $ids[1];

        try {
            $prize = Prize::findOrFail($prizeId);
            $winner = $this->client->user->winners()->findOrFail($winnerId);
            $winner->update(['PrizeId' => $prize->Id]);

            $this->message($this->client->RenderCheques(), $this->kb->MainMenu());

        } catch(Exception $ex){
            \Log::error($ex->getMessage());
            $this->message('Произошла ошибка. Попробуйте еще раз.', $this->kb->MainMenu());
        }

    }



    public function magnit(){
        $this->message('Выберите интересующий Вас раздел', $this->kb->MagnitMenu());
    }

    public function art(){
        $this->message("Сделай фото с бритвой BIC в кадре. 📸 Придумай к своей фотографии подпись, которая расскажет о том, как комфортно ты проводишь отпуск благодаря BIC.😎 Запости фото в Instagram или Вконтакте с хэштегами #bicвсевключено, #(твой номер участника*). \r\nСтань участником розыгрыша 150 000 руб. 🎁 \r\n
            *Номер присваивается участнику акции после регистрации чека в чат-боте", null, 'photo-204950899_457239022');

        if ($this->client->user && $this->client->user->cheques()->where('Approved' , 1)->count() == 1) {
            $this->message("Ура! 🎉 Ты уже зарегистрировал первый валидный чек!
                                Теперь ты можешь участвовать в творческом конкурсе и получить шанс выиграть 150 000 руб! 🎁
                                Твой номер: ".$this->client->VKId);
        }

        if ($this->client->user && $this->client->user->cheques()->where('Approved', 1)->count() > 1) {
            $this->message("Ура! 🎉 Еще один твой валидный чек зарегистрирован!
                                Продолжай участвовать в творческом конкурсе и увеличивай свои шансы выиграть 150 000 рублей! 🎁
                                Твой номер: ".$this->client->VKId);
        }
    }

    public function ncp(){
    $this->message("1. Купи любые бритвы БИК 🪒\r\n2. Активируй чек в чат-боте (прямо здесь) 👇\r\n3. Участвуй в розыгрыше призов 🎁\r\n\r\nПолучи шанс выиграть специальный приз в творческом конкурсе 📸", null, 'photo-204950899_457239025');
    }

    public function faq(){
        $this->message('На какую тему твой вопрос?', $this->kb->FaqRootMenu());
    }

    public function about(){
        $this->message('Выбери пункт меню:', $this->kb->RouterMenu());
    }

    public function rules(){
        

        $this->message("Скачать полные правила акции: https://vk.com/doc105568368_602589324?hash=5a67dfd8a65220f152&dl=27ed4023d4082e4a6b", $this->kb->RouterMenu());
    }

    public function faqCategory(){
        $this->message('Выбери интересующий тебя вопрос', $this->kb->FaqCategory($this->fly_id, $this->paginate));
    }

    public function faqItem(){
        $item = \App\Models\FaqItem::find($this->fly_id);
        if (!$item){
            $this->faq;
        } else {
            $this->message($item->Body, $this->kb->FaqItem());
        }
    }

    //STATES


    public function WaitFeedbackEmail(){
        $this->client->update(['FlyEmail' => $this->message]);
        $this->message('Отправь свой вопрос в ответ на это сообщение, и организатор акции постарается тебе помочь.', $this->kb->HideKeyboard());
        $this->client->update(['State' => 'WaitQuestion']);
    }

    public function WaitQuestion(){

        $fields = [
            'Email' => $this->client->user->Email ?? $this->client->FlyEmail,
            'PersonName' => $this->client->user->FirstName ?? $this->client->FirstName,
            'RetailId' => null,
            'Theme' => 'Обращение из бота',
            'ProcessAccept' => 1,
            'Message' => $this->message,
            'UserId' => $this->client->VKId
        ];



        $feedback = Feedback::create($fields);

        \Mail::send('emails.feedback', [
            'Email' => $feedback->Email,
            'Message' => $feedback->Message,
            'UserId' => \Auth::check() ? \Auth::user()->Id : null,
            'Theme' => 'Обращение из бота',
            'Name' => $feedback->PersonName,
            'Image' => $feedback->Image ? url('/images/feedback/' . $feedback->Image) : null
        ], function($message) {
            $message->to(env('FEEDBACK_MAIL'))->subject('Обращение из бота '.env('APP_URL'));
        });

        $this->client->ClearState();

        $this->message('Спасибо! 🙂 Мы ответим в течение 3-х рабочих дней на электронную почту, указанную тобой. ⏳', $this->kb->MainMenu());
    }

    public function WaitFirstName() {
        $dataset = $this->client->UserDataset ? json_decode($this->client->UserDataset, true) : [];
        $dataset['FirstName'] = $this->message;
        $validator = Validator::make($dataset, [
            'FirstName' => 'cyrillic',
        ]);
        if ($validator->fails()) {
            $this->message('Имя может состоять из кириллицы и пробелов');
        } else {
            $this->client->update(['UserDataset' => json_encode($dataset), 'State' => 'WaitEmail']);
            $this->message('Напиши свой e-mail', $this->kb->HideKeyboard());
        }
    }

    public function WaitLastName() {
        $dataset = $this->client->UserDataset ? json_decode($this->client->UserDataset, true) : [];
        $dataset['LastName'] = $this->message;
        $validator = Validator::make($dataset, [
            'LastName' => 'cyrillic',
        ]);
        if ($validator->fails()) {
            $this->message('Фамилия может состоять из кириллицы и пробелов');
        } else {
            $this->client->update(['UserDataset' => json_encode($dataset), 'State' => 'WaitEmail']);
            $this->message('Введите ваш email', $this->kb->HideKeyboard());
        }
    }

    public function WaitEmail() {
        $dataset = $this->client->UserDataset ? json_decode($this->client->UserDataset, true) : [];
        $dataset['Email'] = $this->message;
        $validator = Validator::make($dataset, [
            'Email' => 'email',
        ]);
        if ($validator->fails()) {
            $this->message('Необходимо ввести корректный email');
        } else {
            if (User::where('Email', $this->message)->count()) {
                $this->message('Данный email уже занят');
            } else {
                $this->client->update(['UserDataset' => json_encode($dataset), 'State' => 'WaitPhone']);
                $this->message('Укажи свой номер мобильного телефона', $this->kb->HideKeyboard());
                //Mail::SendVKCode($this->message, $this->client->Code);
            }
        }
    }

    public function WaitPhoneConfirm() {
        $dataset = $this->client->UserDataset ? json_decode($this->client->UserDataset, true) : [];
        if ($dataset['Phone'] != $this->message) {
            $this->message('Номера не совпадают. Вы можете начать регистрацию заново нажав кнопку "«В главное меню» или ввести корректный номер мобильного телефона.', $this->kb->HideKeyboard());
        } else {
            $this->client->update(['UserDataset' => json_encode($dataset), 'State' => 'WaitPass']);
            $this->message('Придумайте пароль', $this->kb->HideKeyboard());
        }
        
    }

    public function WaitPhone() {
        $dataset = $this->client->UserDataset ? json_decode($this->client->UserDataset, true) : [];
        $dataset['Phone'] = $this->message;
        $validator = Validator::make($dataset, [
            'Phone' => 'phone',
        ]);
        if ($validator->fails()) {
            $this->message('Необходимо ввести корректный номер мобильного телефона без пробелов и дефисов пример Пример: 89771111111 ');
        } else {


            if (User::where('Phone', clearPhone($this->message))->count()){
                $this->message('Этот номер уже зарегистрирован 😒 Укажи другой или обратись в службу поддержки', $this->kb->PhoneExistsKeyboard());
            } else {
                    $this->client->ClearState();
                try {
                    $dataset = json_decode($this->client->UserDataset);
                    $user = User::create([
                        'FirstName' => $dataset->FirstName,
                        'Email' => $dataset->Email,
                        'RulesAccept' => 1,
                        'GroupId' => Group::DEFAULT_GROUP_ID,
                        'Enabled' => 1,
                        'Phone' => clearPhone($this->message),
                        'Password' => \Hash::make('sdfsdfsdfs'.$dataset->Email),
                        'Uid' => Uuid::uuid4(),
                        'ActivationHash' => md5('s34lk2j' . $dataset->Email)
                    ]);
                    $this->client->update(['UserId' => $user->Id]);
                    $this->message('Спасибо! 🤗 Ты успешно зарегистрировался в акции! Теперь ты можешь загружать чеки и отслеживать свои результаты. ', $this->kb->MainMenu());
                } catch (Exception $ex) {
                    \Log::error($ex->getMessage());
                    $this->message('Произошла ошибка при регистрации. Попробуйте еще раз.', $this->kb->MainMenu());
                }

            
            }

           
        }
    }

    public function WaitPass() {
        $dataset = $this->client->UserDataset ? json_decode($this->client->UserDataset, true) : [];
        $dataset['Pass'] = $this->message;
        $validator = Validator::make($dataset, [
            'Pass' => 'min:6',
        ]);
        if ($validator->fails()) {
            $this->message('Минимальная длинна пароля - 6 символов');
        } else {
            $this->client->update(['UserDataset' => json_encode($dataset), 'State' => 'WaitCode']);
            $this->message('Введите проверочный код, который был отправлен на вашу почту.', $this->kb->HideKeyboard());
        }
    }

    public function WaitCode() {

        if ($this->client->Code != $this->message) {
            $this->message('Введен некорректный проверочный код', $this->kb->HideKeyboard());
        } else {
            $this->client->ClearState();
            try {
                $dataset = json_decode($this->client->UserDataset);
                $user = User::create([
                    'FirstName' => $dataset->FirstName,
                    'Email' => $dataset->Email,
                    'RulesAccept' => 1,
                    'GroupId' => Group::DEFAULT_GROUP_ID,
                    'Enabled' => 1,
                    'Phone' => clearPhone($dataset->Phone),
                    'Password' => \Hash::make($dataset->Pass),
                    'Uid' => Uuid::uuid4(),
                    'ActivationHash' => md5('s34lk2j' . $dataset->Email)
                ]);
                $this->client->update(['UserId' => $user->Id]);
            } catch (Exception $ex) {
                \Log::error($ex->getMessage());
                $this->message('Произошла ошибка при регистрации. Попробуйте еще раз.', $this->kb->MainMenu());
            }

            $this->message('Спасибо! Вы успешно зарегистрировались в акции! Теперь вы сможете загружать чеки и отслеживать свои результаты. Выберите интересующий вас пункт меню', $this->kb->MainMenu());
        }
    }

    public function WaitLoginEmail() {
        $user = User::where('Email', $this->message)->first();
        if (!$user) {
            $this->client->ClearState();
            $this->message('Пользователь с таким email не найден', $this->kb->MainMenu());
        } else {
            $this->client->update(['FlyEmail' => $this->message, 'State' => 'WaitLoginPass']);
            $this->message('Введите пароль');
        }
    }

    public function WaitLoginPass() {
        $user = User::where('Email', $this->client->FlyEmail)->first();
        if (!$user) {
            $this->client->ClearState();
            $this->message('Пользователь с таким email не найден', $this->kb->MainMenu());
        } else {
            if (!$user->Enabled) {
                $this->client->ClearState();
                $this->message('Данный аккаунт еще не подтвержден', $this->kb->MainMenu());
            } else {
                $attempt = \Auth::attempt(['Email' => $this->client->FlyEmail, 'password' => $this->message, 'Enabled' => 1], 0);
                if ($attempt) {
                    $this->client->ClearState();
                    $this->client->update(['UserId' => $user->Id]);
                    $this->message('Вы успешно вошли в личный кабинет', $this->kb->MainMenu());
                } else {
                    $this->client->ClearState();
                    $this->message('Неверный логин и\или пароль');
                }
            }
        }
    }

    public function WaitImage() {
        try {
            $images = [];
            \Log::error(json_encode($this->dataset));
            $attachArr = $this->dataset['object']['attachments'][0] ?? null;
            if (!$attachArr || count($this->dataset['object']['attachments']) > 5) {
                $this->message('Необходимо добавить от 1 до 5 изображений', $this->kb->HideKeyboard());
            } else {
                foreach ($this->dataset['object']['attachments'] as $attach) {
                    if ($attach['type'] != 'photo') {
                        $this->message('Неверный тип вложения', $this->kb->HideKeyboard());
                        $this->uploadCheque();
                        return;
                    } else {

                        $g = $this->vk->messages()->getById(self::KEY, ['message_ids' => $this->msg_id]);
                        $sizes = collect($attach['photo']['sizes']);
                        $image = $sizes->sortByDesc('width')->first();
                        $url = $image['url'];


                        $contents = file_get_contents($url);
                        $name = substr($url, strrpos($url, '/') + 1);
                        $nameArr = explode('?', $name);
                        $name = $nameArr ? $nameArr[0] : $name;
                        \Log::error($name);
                        file_put_contents(public_path('images/cheques/'.$name), $contents);

                        $images[] = $name;

                    }
                }
                $dataset = $this->client->ChequeDataset ? json_decode($this->client->ChequeDataset, true) : [];
                $dataset['Image'] = $images;
                $this->client->update(['ChequeDataset' => json_encode($dataset), 'State' => null]);

                $client = new \GuzzleHttp\Client();

                $multipart = [                    
                    [
                        'name' => 'source',
                        'contents' => 'chatbot'
                    ],
                    [
                        'name' => 'channel',
                        'contents' => 'cbot_vk'
                    ],
                    [
                        'name' => 'userUuid',
                        'contents' => $this->client->user->Uid
                    ]
                ];

                foreach ($images as $img){
                    $multipart[] = [
                        'name' => 'photos',
                        'contents' => fopen(public_path('images/cheques/'.$img), 'r'),
                    ];
                }

                $response = $client->request('POST', env("APM_API"), [
                    'multipart' => $multipart,
                    'headers' => [
                        'Accept' => 'application/json',
                        'api-key' => env('APM_KEY_API')
                    ]
                ]);

                \Log::error((string)$response->getBody());

                $this->message('Отлично! 👍 Твой чек отправлен на модерацию. Загружай чеки и участвуй в розыгрыше призов! 🎁 И помни, больше чеков — больше шансов на победу. Удачи! 🤗');
                $this->message('Для продолжения выбери раздел, который тебе интересен:', $this->kb->MainMenu());

            }
        } catch (\Exception $ex) {
            \Log::error($ex);
            $this->message('Что-то пошло не так. Попробуй еще раз', $this->kb->HideKeyboard());

        }
    }

    public function WaitChequeNumber() {
        $dataset = $this->client->ChequeDataset ? json_decode($this->client->ChequeDataset, true) : [];
        $dataset['ChequeNumber'] = $this->message;
        $validator = Validator::make($dataset, [
            'ChequeNumber' => 'digits_between:1,8',
        ]);
        if ($validator->fails()) {
            $this->message('Номер чека должен быть числом, от 1 до 8 знаков');
        } else {
            $this->client->update(['ChequeDataset' => json_encode($dataset), 'State' => 'WaitShop']);
            $this->message('Введите название магазина', $this->kb->HideKeyboard());
        }
    }

    public function WaitShop() {
        $dataset = $this->client->ChequeDataset ? json_decode($this->client->ChequeDataset, true) : [];
        $dataset['Shop'] = $this->message;
        $this->client->update(['ChequeDataset' => json_encode($dataset), 'State' => 'WaitTradePoint']);
        $this->message('Введите название торговой точки', $this->kb->HideKeyboard());
    }

    public function WaitTradePoint() {
        $dataset = $this->client->ChequeDataset ? json_decode($this->client->ChequeDataset, true) : [];
        $dataset['TradePoint'] = $this->message;
        $this->client->update(['ChequeDataset' => json_encode($dataset), 'State' => 'WaitBuyDate']);
        $this->message('Введите дату покупки в формате 01.01.2020', $this->kb->HideKeyboard());
    }

    public function WaitBuyDate() {
        $dataset = $this->client->ChequeDataset ? json_decode($this->client->ChequeDataset, true) : [];
        $dataset['BuyDate'] = $this->message;
        $validator = Validator::make($dataset, [
            'BuyDate' => 'date',
        ]);
        if ($validator->fails()) {
            $this->message('Введите корректную дату');
        } else {
            $this->client->ClearState();
            ChequeModel::create([
                'ChequeImage' => $dataset['Image'],
                'ChequeNumber' => $dataset['ChequeNumber'],
                'ShopId' => $dataset['TradePoint'],
                'ShopName' => $dataset['Shop'],
                'BuyDate' => Carbon::parse($dataset['BuyDate'])->format('Y-m-d'),
                'UserId' => $this->client->UserId,
                'Status' => ChequeModel::STATUS_NEW,
                'IsVK' => 1
            ]);
            $this->message('Ваш чек отправлен на модерацию. Загружайте больше чеков и получите шанс выиграть один из призов. Больше чеков – больше шансов! Удачи!', $this->kb->MainMenu());
        }
    }

    public function WaitYMoney(){

        $daily = $this->client->user->winners()->where('WinType', 'daily')->first();
        if ($daily) {
            $daily->update(['YWallet' => $this->message]);
        }

        $this->client->update(['State' => null]);
        $this->message('Поздравляем! В ближайшее время денежный приз будет перечислен на указанный вами Электронный Кошелек Юмани', $this->kb->MainMenu());
    }


    public function WaitWinnerLastName(){
        $this->client->user->update(['WinnerLastName' => $this->message]);
        $this->client->update(['State' => 'WaitWinnerFirstName']);
        $this->message('Имя:', $this->kb->HideKeyboard());
    }
    public function WaitWinnerFirstName(){
        $this->client->user->update(['WinnerFirstName' => $this->message]);
        $this->client->update(['State' => 'WaitWinnerPatronymic']);
        $this->message('Отчество:', $this->kb->HideKeyboard());
    }
    public function WaitWinnerPatronymic(){
        $this->client->user->update(['WinnerPatronymic' => $this->message]);
        $this->client->update(['State' => 'WaitPassNumber']);
        $this->message('Номер паспорта:', $this->kb->HideKeyboard());
    }
    public function WaitPassNumber(){
        $this->client->user->update(['PassportNumber' => $this->message]);
        $this->client->update(['State' => 'WaitPassSerial']);
        $this->message('Серия паспорта:', $this->kb->HideKeyboard());
    }
    public function WaitPassSerial(){
        $this->client->user->update(['PassportSerialNumber' => $this->message]);
        $this->client->update(['State' => 'WaitPassportWhenIssued']);
        $this->message('Дата выдачи паспорта:', $this->kb->HideKeyboard());
    }
    public function WaitPassportWhenIssued(){
        $validator = Validator::make(['PassportWhenIssued' => $this->message], [
            'PassportWhenIssued' => 'date',
        ]);
        if ($validator->fails()) {
            $this->message('Введите корректную дату');
        } else {
            $this->client->user->update(['PassportWhenIssued' => $this->message]);
            $this->client->update(['State' => 'WaitPassportIssued']);
            $this->message('Кем выдан паспорт:', $this->kb->HideKeyboard());
        }
    }
    public function WaitPassportIssued(){
        $this->client->user->update(['PassportIssued' => $this->message]);
        $this->client->update(['State' => 'WaitBirthDate']);
        $this->message('Дата рождения:', $this->kb->HideKeyboard());
    }

    public function WaitBirthDate(){
        $validator = Validator::make(['BirthDate' => $this->message], [
            'BirthDate' => 'date',
        ]);
        if ($validator->fails()) {
            $this->message('Введите корректную дату');
        } else {
            $this->client->user->update(['BirthDate' => $this->message]);
            $this->client->update(['State' => 'WaitInn']);
            $this->message('Номер ИНН:', $this->kb->HideKeyboard());
        }


    }

    public function WaitInn(){
        $this->client->user->update(['Inn' => $this->message]);
        $this->client->update(['State' => 'WaitRegistrationAddress']);
        $this->message('Адрес регистрации:', $this->kb->HideKeyboard());
    }

    public function WaitRegistrationAddress(){
        $this->client->user->update(['RegistrationAddress' => $this->message]);
        $this->client->update(['State' => 'WaitDeliveryAddress']);
        $this->message('Адрес доставки приза:', $this->kb->HideKeyboard());
    }

    public function WaitDeliveryAddress(){
        $this->client->user->update(['DeliveryAddress' => $this->message]);
        $this->client->update(['State' => 'WaitPassportImage']);
        $this->message('Загрузите фото главной страницы паспорта', $this->kb->HideKeyboard());
    }

    public function WaitPassportImage(){
        try {
            $attach = $this->dataset['object']['attachments'][0] ?? null;
            if (!$attach) {
                $this->message('Необходимо добавить изображение', $this->kb->HideKeyboard());
            } else {
                if ($attach['type'] != 'photo') {
                    $this->message('Необходимо добавить картинку', $this->kb->HideKeyboard());
                } else {                    

                    $sizes = collect($attach['photo']['sizes']);
                    $image = $sizes->sortByDesc('width')->first();
                    $url = $image['url'];


                    $contents = file_get_contents($url);
                    $name = substr($url, strrpos($url, '/') + 1);
                    $nameArr = explode('?', $name);
                    $name = $nameArr ? $nameArr[0] : $name;
                    $name = '/winners_data/PassportImage/'.$name;

                    file_put_contents(public_path($name), $contents);


                    $this->client->user->update(['PassportImage' => $name]);
                    $this->client->update(['State' => 'WaitRegistratonImage']);
                    $this->message('Загрузите фото страницы паспорта с пропиской', $this->kb->HideKeyboard());
                }
            }
        } catch (\Exception $ex) {
            $this->message('Что-то пошло не так. Попробуй еще раз' . $ex->getMessage(), $this->kb->HideKeyboard());
        }
    }

    public function WaitRegistratonImage(){
        try {
            $attach = $this->dataset['object']['attachments'][0] ?? null;
            if (!$attach) {
                $this->message('Необходимо добавить изображение', $this->kb->HideKeyboard());
            } else {
                if ($attach['type'] != 'photo') {
                    $this->message('Необходимо добавить картинку', $this->kb->HideKeyboard());
                } else {                    

                    $sizes = collect($attach['photo']['sizes']);
                    $image = $sizes->sortByDesc('width')->first();
                    $url = $image['url'];


                    $contents = file_get_contents($url);
                    $name = substr($url, strrpos($url, '/') + 1);
                    $nameArr = explode('?', $name);
                    $name = $nameArr ? $nameArr[0] : $name;
                    $name = '/winners_data/RegistratonImage/'.$name;

                    file_put_contents(public_path($name), $contents);


                    $this->client->user->update(['RegistratonImage' => $name]);
                    $this->client->update(['State' => 'WaitInnImage']);
                    $this->message('Загрузите скан ИНН', $this->kb->HideKeyboard());
                }
            }
        } catch (\Exception $ex) {
            $this->message('Что-то пошло не так. Попробуй еще раз' . $ex->getMessage(), $this->kb->HideKeyboard());
        }
    }

    public function WaitInnImage(){
        try {
            $attach = $this->dataset['object']['attachments'][0] ?? null;
            if (!$attach) {
                $this->message('Необходимо добавить изображение', $this->kb->HideKeyboard());
            } else {
                if ($attach['type'] != 'photo') {
                    $this->message('Необходимо добавить картинку', $this->kb->HideKeyboard());
                } else {                    

                    $sizes = collect($attach['photo']['sizes']);
                    $image = $sizes->sortByDesc('width')->first();
                    $url = $image['url'];


                    $contents = file_get_contents($url);
                    $name = substr($url, strrpos($url, '/') + 1);
                    $nameArr = explode('?', $name);
                    $name = $nameArr ? $nameArr[0] : $name;
                    $name = '/winners_data/InnImage/'.$name;

                    file_put_contents(public_path($name), $contents);


                    $this->client->user->update(['InnImage' => $name]);
                    $this->client->update(['State' => null]);
                    $this->message('Спасибо, ваши данные приняты!', $this->kb->MainMenu());
                }
            }
        } catch (\Exception $ex) {
            $this->message('Что-то пошло не так. Попробуй еще раз' . $ex->getMessage(), $this->kb->HideKeyboard());
        }
    }

}
