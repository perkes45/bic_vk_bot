<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Request as Req;

class Yandex extends Controller
{
    public function Error(Request $request) {
    	Req::create(['Payload' => json_encode($request->all())]);
    }
}
