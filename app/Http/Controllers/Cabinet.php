<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Validator;
use App\Models\User;
use App\Models\Group;
use App\Models\Winner;
use App\Models\Cheque;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\UploadedFile;
use App\Services\Cheque as ChequeService;

/**
 * Description of Auth
 *
 * @author r_zhalialov
 */
class Cabinet extends BaseController {

    public function updateInfo(Request $request) {

        $user = Auth::user();
        $fields = $request->all();
        $available = User::FIELDS;
        $rules = [
            'City' => 'required',
            'Region' => 'required',
            'BirthDate' => 'required|date'
        ];

        foreach ($fields as $field) {
            if (in_array($field, $available)) {
                unset($fields[$field]);
            }
        }

        $validator = Validator::make($fields, $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return [
                'error' => 1,
                'message' => $errors,
            ];
        } else {
            $fields['BirthDate'] = Carbon::parse($fields['BirthDate']);
            $age = Carbon::now()->diffInYears($fields['BirthDate']);
            if ($age < $user::MIN_AGE) {                
                return ['error' => 1, 'message' => 'Вы не можете зарегистрироваться, т.к. Вам менее 18 лет.'];
            }

            if ($age > $user::MAX_AGE){                
                return ['error' => 1, 'message' => 'Вы не можете зарегистрироваться, т.к. Вам более 90 лет.'];
            }

            $user->update($fields);
            $user->save();

            return ['error' => 0, 'message' => 'Данные успешно обновлены'];
        }
    }

    public function Index(Request $request) {

        if ($request->method() == $request::METHOD_POST) {  


            if(!Auth::user()->Enabled){
                return ['error' => 1, 'message' => 'Вам временно ограничена возможность загрузки новых чеков'];
            }

            $result = ChequeService::check($request);
            if ($result['error']) {
                return $result;
            } else {
                $result = ChequeService::upload($request);
                return $result;
            }
        }

        //$show = Auth::user()->infoNeeded();

        $user = \Auth::user();

        $daily = $user->winners()->where(['WinType' => 'daily'])->first();
        $special = $user->winners()->where(['WinType' => 'Специальный'])->first();
        $showDaily = $showSpecial = false;
        if ($daily && $daily->infoNeeded()){
            $showDaily = true;
        }

        if ($special && $special->infoNeeded()){
            $showSpecial = true;
        }

        return view('cabinet', compact('user', 'showDaily', 'showSpecial', 'daily', 'special'));
    }


    public function updateWinnerInfo(Request $request, $id) {
        try {
            $winner = Auth::user()->winners()->find($id);
            if (!$winner) {
                return ['error' => 1, 'message' => 'Произошла ошибка, попробуйте еще раз'];
            }
            $available = $winner->WinType == 'daily' ? $winner::DAILY_PRIZE_FIELDS : $winner::SPECIAL_PRIZE_FIELDS;
            $fields = $request->all();
            $rules = [
                'YWallet' => 'required',
            ];

            if (Winner::where('YWallet', $request->YWallet)->count()){
                return [
                    'error' => 1,
                    'message' => 'Данный номер кошелька уже был использован',
                ];
            }

            if ($winner->WinType == 'Специальный') {
                $rules['InnImage'] = !$winner->InnImage ? 'required|file|mimes:jpeg,png,jpg,pdf,gif|max:10240' : '';
            }

            foreach ($fields as $field) {
                if (in_array($field, $available)) {
                    unset($fields[$field]);
                }
            }
            $validator = Validator::make($fields, $rules);
            if ($validator->fails()) {
                $errors = $validator->errors();
                return [
                    'error' => 1,
                    'message' => $errors,
                ];
            } else {

                // $PassportMain = $request->PassportMain;
                // if ($PassportMain){                    
                //     $filename = md5($winner->Id) . '.' . $PassportMain->extension();
                //     $PassportMain->move(public_path('winners_data/PassportMain'), $filename);
                //     $fields['PassportMain'] = '/winners_data/PassportMain/'.$filename;
                // }
                
                // $PassportRegistration = $request->PassportRegistration;
                // if ($PassportRegistration){
                //     $filename = md5($winner->Id) . '.' . $PassportRegistration->extension();
                //     $PassportRegistration->move(public_path('winners_data/PassportRegistration'), $filename);
                //     $fields['PassportRegistration'] = '/winners_data/PassportRegistration/'.$filename;
                // }

                // $WinnerCheques = $request->WinnerCheques;
                // if ($WinnerCheques){                    
                //     $filename = md5($winner->Id) . '.' . $WinnerCheques->extension();
                //     $WinnerCheques->move(public_path('winners_data/WinnerCheques'), $filename);
                //     $fields['WinnerCheques'] = '/winners_data/WinnerCheques/'.$filename;
                // }


                // $PrizeDocument = $request->PrizeDocument;
                // if ($PrizeDocument){
                //     $filename = md5($winner->Id) . '.' . $PrizeDocument->extension();
                //     $PrizeDocument->move(public_path('winners_data/PrizeDocument'), $filename);
                //     $fields['PrizeDocument'] = '/winners_data/PrizeDocument/'.$filename;
                // }

                // if ($winner->WinType == 'Специальный') { 
                //     $InnImage = $request->InnImage;
                //     if ($InnImage){
                //         $filename = md5($winner->Id) . '.' . $InnImage->extension();
                //         $InnImage->move(public_path('winners_data/InnImage'), $filename);
                //         $fields['InnImage'] = '/winners_data/InnImage/'.$filename;
                //     }
                // }
                
                $fields['Status'] = Winner::TYPE_CONFIRMED;
                $winner->update($fields);

                return ['error' => 0, 'message' => 'Данные успешно обновлены'];
            }
        } catch (\Exception $ex) {
		\Log::error($ex->getMessage());
            return ['error' => 1, 'message' => 'Произошла ошибка, попробуйте еще раз'];
        }
    }

}
