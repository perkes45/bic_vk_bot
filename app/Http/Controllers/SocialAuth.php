<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Socialite;

class SocialAuth extends Controller {

    public function redirectToProvider(Request $request, $driver) {
        return Socialite::driver($driver)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback(Request $request, $driver) {
        try {


            $user = Socialite::driver($driver)->user();
            $dataset['Email'] = $user->getEmail();
            if ($driver == 'vkontakte') {
                $dataset = [
                    'FirstName' => $user->user['first_name'] ?? null,
                    'LastName' => $user->user['last_name'] ?? null
                ];
                if (!isset($dataset['Email']) || !$dataset['Email']) {
                    $dataset['Email'] = $user->accessTokenResponseBody['email'] ?? null;
                }
            }
            if ($driver == 'odnoklassniki') {
                if (strpos($user->user['name'], ' ') !== false) {
                    [$dataset['FirstName'], $dataset['LastName']] = explode(' ', $user->user['name']);
                } else {
                    $dataset['LastName'] = $user->user['name'];
                }
            }
            if ($driver == 'facebook') {
                if (strpos($user->user['name'], ' ') !== false) {
                    [$dataset['LastName'], $dataset['FirstName']] = explode(' ', $user->user['name']);
                } else {
                    $dataset['LastName'] = $user->user['name'];
                }
            }
            return redirect('/')->with('RegData', $dataset);
        } catch (Exception $ex) {
            \Log::error($ex->getMessage());
            return redirect('/');
        }
    }

}
