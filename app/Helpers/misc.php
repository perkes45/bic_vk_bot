<?php

function clearPhone($phone) {
		if (strlen($phone) == 10 && is_numeric($phone)) {
		return $phone;
	}
	$phone = preg_replace('#[^0-9a-zа-я]#iu', '', $phone);
	$phone = preg_split('#[^0-9]#', $phone);
	$phone = array_filter($phone);
	if (!$phone) {
		return null;
	}
	$phone = $phone[array_keys($phone)[0]];
	if (strlen($phone) == 10) {
		return $phone;
	}
	if (strlen($phone) == 11 && ($phone[0] == '8' || $phone[0] == '7')) {
		return substr($phone, 1);
	}
	if (strlen($phone) == 12 && substr($phone, 0, 2) == '98') {
		return substr($phone, 2);
	}
	return null;
}
function generateRandomCode() {  
    return rand(100000, 999999);
}
function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $uppercase = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $uppercaseLength = strlen($uppercase);
    $randomString = $uppercase[rand(0, $uppercaseLength - 1)];    
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    $rand = ['!', '#', '&', '$', '?'];
    $spec = $rand[array_rand($rand)];
    $randomString .= $spec;

    return $randomString;
}

function hideEmail($email){
    $str = '';
    $domainAtt = explode('.', $email);
    $domain = end($domainAtt);
    $loginParts = explode('@', $email);
    
    $str = substr($loginParts[0], 0, 3).str_repeat('*', strlen($loginParts[0])-3).'@'.substr($loginParts[1], 0, 3).str_repeat('*', strlen($loginParts[1])-3).'.'.$domain;
    //$str = str_repeat('*', strlen($loginParts[0])).'@'.str_repeat('*', strlen($loginParts[1])).'.'.$domain;
    return $str;
}













