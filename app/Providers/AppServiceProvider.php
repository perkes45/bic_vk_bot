<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\DB;

class AppServiceProvider extends ServiceProvider {

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
	//
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
	if (env('APP_ENV') == 'local') {
	    $this->app->register('Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider');
	}

    }

}
