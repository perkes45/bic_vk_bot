<?php

namespace App\Providers;

use App\Validators\Validator;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class ValidateServiceProvider extends ServiceProvider {

    public function boot() {
	\Validator::resolver(function($translator, $data, $rules, $messages) {
	    return new Validator($translator, $data, $rules, $messages);
	});
    }

    public function register() {
	
    }

}
