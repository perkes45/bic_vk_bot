<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Drawing;
use \Carbon\Carbon;
use App\Models\Cheque;
use App\Models\Prize;
use App\Models\Applicant;
use App\Models\Winner;
use App\Models\Sert;
use App\Models\Response;
use App\Services\Mail as MailService;
use VK\Client\VKApiClient;

class DailyDrawings extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'drawing:daily';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'fire daily drawings';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $drawing = Drawing::where('FireAt', '=', Carbon::now()->format('Y-m-d'))->where('Type', 'daily')->first();
        //$drawing = Drawing::find(17);
        if (!$drawing) {
            $this->error('No active drawing');
        } else {
           
            $prize = Prize::where('PrizeType', 'daily')->first();
            $cheques = Cheque::select('Cheques.*')->where('Approved', 1)
                    ->where('Cheques.CreatedAt', '>=', $drawing->PeriodStart)
                    ->where('Cheques.CreatedAt', '<=', $drawing->PeriodEnd)
                    ->where('ChequeCanWinDaily', 1)
                    ->where('UserCanWinDaily', '<=', 4)
                    ->join('Users', 'Cheques.UserId', '=', 'Users.Id')->get();
            $this->info($cheques->count());
            $winnersCount = 5 - $drawing->winners->count();
            $this->info($winnersCount);
            if (Applicant::count()) {
                Applicant::query()->truncate();
            }
            foreach ($cheques as $cheque) {  
                Applicant::create([
                    'UserId' => $cheque->UserId,
                    'ChequeId' => $cheque->Id
                ]);
            }

            for ($i = 1; $i <= $winnersCount; $i++) {
		$message = '';
                try {
                    $n = floor(Applicant::count() / ($winnersCount + 1));
                    
                    $index = $i * $n;
                    
                    $applicant = Applicant::orderBy('ChequeId', 'ASC')->skip($index - 1)->take(1)->first();
                    if (!$applicant){
                        $this->error('Нет претендента');
                        continue;
                    }
                    $cheque = Cheque::find($applicant->ChequeId);

                    if ($applicant && $cheque) {
                        $winner = Winner::create([
                                    'UserId' => $cheque->user->Id,
                                    'DrawingId' => $drawing->Id,
                                    'WinType' => $prize->PrizeType,
                                    'PrizeId' => $prize->Id,
                                    'ChequeId' => $cheque->Id,
                                    'Price' => 1000,
                                    'Status' => Winner::TYPE_CONFIRMED
                        ]);
                        Applicant::where('UserId', $cheque->UserId)->forceDelete();
                        $cheque->user->update(['UserCanWinDaily' => ($cheque->user->UserCanWinDaily + 1)]);
                        $cheque->update(['ChequeCanWinDaily' => 0]);
                        $this->info('Победитель назначен '.$cheque->Id);

                        $sert = Sert::whereNull('WinnerId')->where('Type', 'daily')->first();
                        if ($sert) {
                            $sert->update(['WinnerId' => $winner->Id]);
                        } else {
                             $this->info('Нет сертификата для победителя '.$winner->Id);
                        }

                        $message .= 'Поздравляем! 🎉🎉🎉 Ты выиграл приз - '.$prize->PrizeName."🎁\r\n\r\n";   
                        if ($sert) {
                            $message .= 'Номер сертификата - '.$sert->Number."\r\n";
                            $message .= 'Код сертификата - '.$sert->Pin."\r\n";
                            $message .= 'Годен до - '.$sert->ExpireDate."\r\n\r\n";
                        }     
                        $message .= 'Если у тебя остались вопросы, смело обращайся в Службу обратной связи.';

                        $response = Response::create([
                            'Body' => $message
                        ]);
                        $lastRequestId = $response->Id;  
                        $vkId = $cheque->user->vKUsers()->first()->VKId;                          

                        $dataset = [
                            'user_id' => $vkId,
                            'message' => $message,
                            'random_id' => $lastRequestId,
                            'attachment' => 'photo-204950899_457239032'
                        ];

                        $vk = new VKApiClient('5.95');

                        $vk->messages()->send('e6b9a5ba80ddbfea1fbc9b8e0cae1ccd59e9e0afce284c75bf56576ff537641022f024c5fdaec4024cb39', $dataset);

                        //MailService::SendDailyWinMail($cheque->user->Email, $cheque->user->FirstName);

                        // $email = $cheque->user->Email;
                        // \Mail::send('emails.win', [], function($message) use ($email) {
                        //     $message->to($email)->subject('Вы выиграли приз');
                        // });
                    }
                } catch (\Exception $ex) {
                    $this->error("Ошибка при назначении победителя".$ex->getMessage());
                }
            }
        }
    }

}
