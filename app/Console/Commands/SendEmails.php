<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Email;
use App\Services\Mail;
use App\Models\ShopsLookup;

class SendEmails extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'emails:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $array = \Excel::toArray(new \App\Imports\ShopImport, public_path('ap.xlsx'));
        $data = $array[0];
        $this->info(count($data));
        unset($data[0]);
        foreach ($data as $row) {
            $address = $row[0];
            $options = array(
                'http' => array(
                    'method' => "GET",
                    'header' => "Accept-language: en\r\n" .
                    "User-Agent: Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B334b Safari/531.21.102011-10-16 20:23:10\r\n" // i.e. An iPad 
                )
            );

            $context = stream_context_create($options);            
            $url = "https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/findAddressCandidates?outSr=4326&forStorage=false&outFields=*&maxLocations=5&singleLine=".urlencode($address)."&f=json";
            $dataset = file_get_contents($url, false, $context);
            $dataset = json_decode($dataset);
            $lng = $dataset->candidates[0]->location->x;
            $lat = $dataset->candidates[0]->location->y;
            $city = $dataset->candidates[0]->attributes->City ? $dataset->candidates[0]->attributes->City : $dataset->candidates[0]->attributes->District;
            $d = [
                'Lng' => $lng,
                'Lat' => $lat,
                'ShopCity' => $city,
                'ShopAddress' => $address,
                'ShopName' => 'Магнит'
            ];
            var_dump($d);
            ShopsLookup::create($d);
        }
    }

}
