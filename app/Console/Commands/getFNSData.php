<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Cheque;
use GuzzleHttp\Client;

class getFNSData extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fns:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get info about cheque from QR data string';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
	parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
	$cheques = Cheque::whereNotNull('QRData')->whereNull('FNSData')->whereNull('FNSDataCheckedAt')->get();	
	$client = new Client();
	if (!$cheques->count()){
	    $this->info('Nothing to check');
	    return;
	}
	foreach ($cheques as $cheque) {
	    $result = $cheque->GetFNSData();
	    if ($result['error']) $this->error($result['message']);
	    else $this->info($result['message']);
	}
    }

}
