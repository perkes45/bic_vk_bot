<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Cheque;


class checkQR extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'qr:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Getting QR string from cheque image';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
	parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {	
	$cheques = Cheque::whereNull('QRCheckedAt')->get();
	foreach ($cheques as $cheque) {
	    $result = $cheque->GetQRData();
	    if ($result['error']) $this->error($result['message']);
	    else $this->info($result['message']);
	}
    }

}
