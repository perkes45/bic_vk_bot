<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Cheque;
use App\Models\Winner;
use App\Models\Prize;
use Carbon\Carbon;
class RecreateGarant extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'garant:recreate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cheques = Cheque::where('Approved', 1)->where('CreatedAt', '>=', '2021-03-12')->get();

        $f = 0;

        foreach ($cheques as $cheque ) {
            $user = $cheque->user;


            if (!$user->winners()->where('WinType', 'garant')->count()) {
                if (!Winner::where('WinnerPhone', $user->Phone)->count()) {
                    if (Carbon::now()->lt(Carbon::parse('2021-03-22'))) {
                        $prize = Prize::where('PrizeType', 'garant')->first();
                        $f += 1;
                        $this->info('++++');
                        $winner = Winner::create([
                            'UserId' => $cheque->user->Id,
                            'WinType' => $prize->PrizeType,
                            'PrizeId' => $prize->Id,
                            'ChequeId' => $cheque->Id,
                            'Status' => Winner::TYPE_CONFIRMED,
                            'WinnerPhone' => $user->Phone,
                            'Price' => 50
                        ]);
                    }
                } else {
                    $this->info('Телефон уже выигрывал');
                }

            } else {
                $this->info('Пользователь уже выигрывал');
            }


        }

        $this->info($f);

    }
}
