<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Drawing;
use \Carbon\Carbon;
use App\Models\Cheque;
use App\Models\Prize;
use App\Models\Sert;
use App\Models\Applicant;
use App\Models\Winner;
use App\Services\Mail as MailService;

class SpecialDrawings extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'drawing:special';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'fire weekly drawings';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $drawing = Drawing::where('FireAt', '=', Carbon::now()->format('Y-m-d'))->where('Type', 'weekly')->first();
        //$drawing = Drawing::find(17);
        if (!$drawing) {
            $this->error('No active drawing');
        } else {
           
            $prize = Prize::where('PrizeType', 'weekly')->first();
            $cheques = Cheque::select('Cheques.*')->where('Approved', 1)
                    ->where('Cheques.CreatedAt', '>=', $drawing->PeriodStart)
                    ->where('Cheques.CreatedAt', '<=', $drawing->PeriodEnd)
                    ->where('ChequeCanWinWeekly', 1)
                    ->where('UserCanWinWeekly', 1)
                    ->join('Users', 'Cheques.UserId', '=', 'Users.Id')->get();
            $this->info($cheques->count());
            $winnersCount = 1 - $drawing->winners->count();
            $this->info($winnersCount);
            if (Applicant::count()) {
                Applicant::query()->truncate();
            }
            foreach ($cheques as $cheque) {  
                Applicant::create([
                    'UserId' => $cheque->UserId,
                    'ChequeId' => $cheque->Id
                ]);
            }

            for ($i = 1; $i <= $winnersCount; $i++) {
                try {
                    $n = floor(Applicant::count() / ($winnersCount + 1));
                    
                    $index = $i * $n;
                    
                    $applicant = Applicant::orderBy('ChequeId', 'ASC')->skip($index - 1)->take(1)->first();
                    if (!$applicant){
                        $this->error('Нет претендента');
                        continue;
                    }
                    $cheque = Cheque::find($applicant->ChequeId);

                    if ($applicant && $cheque) {
                        $winner = Winner::create([
                                    'UserId' => $cheque->user->Id,
                                    'DrawingId' => $drawing->Id,
                                    'WinType' => $prize->PrizeType,
                                    'PrizeId' => $prize->Id,
                                    'ChequeId' => $cheque->Id,
                                    'Price' => $drawing->getScore(),
                                    'Status' => Winner::TYPE_CONFIRMED
                        ]);
                        Applicant::where('UserId', $cheque->UserId)->forceDelete();
                        $cheque->user->update(['UserCanWinWeekly' => 0]);
                        $cheque->update(['ChequeCanWinWeekly' => 0]);
                        $this->info('Победитель назначен '.$cheque->Id);

                        $sert = Sert::whereNull('WinnerId')->where('Type', 'weekly')->first();
                        if ($sert) {
                            $sert->update(['WinnerId' => $winner->Id]);
                        } else {
                             $this->info('Нет сертификата для победителя '.$winner->Id);
                        }

                        //MailService::SendWeeklyWinMail($cheque->user->Email, $cheque->user->FirstName);
                        // $email = $cheque->user->Email;
                        // \Mail::send('emails.win', [], function($message) use ($email) {
                        //     $message->to($email)->subject('Вы выиграли приз');
                        // });
                    }
                } catch (\Exception $ex) {
                    $this->error("Ошибка при назначении победителя".$ex->getMessage());
                }
            }
        }
    }

}
