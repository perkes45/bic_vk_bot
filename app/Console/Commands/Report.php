<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Drawing;
use \Carbon\Carbon;
use App\Models\Cheque;
use App\Models\Prize;
use App\Models\Applicant;
use App\Models\Winner;

class Report extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:daily';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'daily reprort';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $body = view('emails.report', [])->render(); 
        $subject = 'Сводка статистики «Больше хочется - больше сбудется»';
        $emails = explode(';', env('REPORT_EMAIL'));
        \Mail::send([], [],  function($message) use ($emails, $subject, $body) {
            $message->to($emails)->subject($subject)->setBody($body, 'text/html');;
        });
    }

}
