<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use YooKassaPayout\Client;
use YooKassaPayout\Request\Keychain;
use YooKassaPayout\Model\Recipient\BaseRecipient;
use YooKassaPayout\Model\Recipient\BankAccountRecipient;
use YooKassaPayout\Model\Recipient\BankCardRecipient;
use YooKassaPayout\Model\CurrencyCode;
use YooKassaPayout\Model\FormatType;
use YooKassaPayout\Request\TestDepositionRequest;
use YooKassaPayout\Request\MakeDepositionRequest;
use YooKassaPayout\Request\BalanceRequest as BRequest;
use YooKassaPayout\Common\Exceptions\ApiException;
use App\Models\Winner;
use Carbon\Carbon;
use App\Models\Request as Req;


class BalanceRequest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'balance:get';


    const AMMOUNT = 50;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $keychain = new Keychain('204774.cer', 'private.key', 'perkes46');
        $client = new Client(env("YA_SHOP_ID"), $keychain);

	 try {
             $response = $client->createDeposition(new BRequest());
             var_dump($response->getXmlResponse());
         } catch (\Exeption $ex) {
             $this->error($ex->getMessage());
         }


        die();

        // $recipient = new BaseRecipient();
        // $recipient->setPofOfferAccepted(true);

        // $depositionRequest = new MakeDepositionRequest();
        // $depositionRequest->setDstAccount('79104202807')
        //     ->setAmount(1)
        //     ->setCurrency(CurrencyCode::RUB)
        //     ->setContract('')->setPaymentParams($recipient);
        // try {
        //     $response = $client->createDeposition($depositionRequest);
        //     var_dump($response->getXmlResponse());
        // } catch (\Exeption $ex) {
        //     $this->error($ex->getMessage());
        // }

        // die();


        // $recipient = new BaseRecipient();
        // $recipient->setPofOfferAccepted(true);

        //     $depositionRequest = new MakeDepositionRequest();
        //     $depositionRequest->setDstAccount('410012160316857')
        //     ->setAmount(1)            
        //     ->setCurrency(CurrencyCode::RUB)
        //     ->setContract('Гарантированный приз в акции Эпика')->setPaymentParams($recipient);

        //         try {
        //             $response = $client->createDeposition($depositionRequest);
        //             var_dump($response->getXmlResponse());
        //             var_dump($response->getXmlResponse()->getClientOrderId());
        //         } catch (ApiException $ex) {
        //             $this->error($ex->getMessage());
        //         }


        //         die();


       //  $recipient = new BankCardRecipient();
       // $recipient->setPdrLastName('Иванов')
       //            ->setPdrFirstName('Иван')
       //            ->setPdrMiddleName('Иванович')
       //            ->setDocNumber('1234567890')
       //            ->setPofOfferAccepted(true)
       //            ->setSkrDestinationCardSynonym('i8DZjk1u4a8TAm-577VSEzFh.SC.000.201905');

       //  $depositionRequest = new MakeDepositionRequest();
       //      $depositionRequest->setDstAccount('25700120202056919')
       //      ->setAmount(101)
       //      //->setClientOrderId(161354668986)
       //      ->setPaymentParams($recipient)
       //      ->setCurrency(CurrencyCode::RUB)
       //      ->setContract('test');

        

        die();
        
    }
}
