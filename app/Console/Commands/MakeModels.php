<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use User11001\EloquentModelGenerator\Console\GenerateModelsCommand;

class MakeModels extends GenerateModelsCommand {
	public function handle() {
		$this->fire();
	}
}
