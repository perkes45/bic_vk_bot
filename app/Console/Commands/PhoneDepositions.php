<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use YooKassaPayout\Client;
use YooKassaPayout\Request\Keychain;
use YooKassaPayout\Model\Recipient\BaseRecipient;
use YooKassaPayout\Model\Recipient\BankAccountRecipient;
use YooKassaPayout\Model\Recipient\BankCardRecipient;
use YooKassaPayout\Model\CurrencyCode;
use YooKassaPayout\Model\FormatType;
use YooKassaPayout\Request\TestDepositionRequest;
use YooKassaPayout\Request\MakeDepositionRequest;
use YooKassaPayout\Request\BalanceRequest;
use YooKassaPayout\Common\Exceptions\ApiException;
use App\Models\Winner;
use Carbon\Carbon;
use App\Models\Request as Req;


class PhoneDepositions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deposition:phone';


    const AMMOUNT = 50;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $keychain = new Keychain('204774.cer', 'private.key', 'perkes46');
        $client = new Client(env("YA_SHOP_ID"), $keychain);



        $winners = Winner::where('WinType', 'garant')->whereNotNull('WinnerPhone')->where('Status', 'Confirmed')->whereNull('ProcessedAt')->get();

        if (!$winners->count()){
            $this->info('No winners for phone depositions');
        }

        foreach ($winners as $winner) {
            $num = $winner->Id.$winner->ChequeId;
            $this->info($num);

                try {

                    $phone = '7'.$winner->WinnerPhone;

                    $recipient = new BaseRecipient();
                    $recipient->setPofOfferAccepted(true);

                    $depositionRequest = new TestDepositionRequest();
                    $depositionRequest->setDstAccount($phone)
                                ->setAmount(self::AMMOUNT)
                                ->setClientOrderId('102'.$num)
                                ->setCurrency(CurrencyCode::RUB)
                                ->setContract('')->setPaymentParams($recipient);
                    Req::create(['Payload' => json_encode((array)$depositionRequest)]);
                    $response = $client->createDeposition($depositionRequest);

                    $winner->update(['DepositionInfo' => $response->getXmlResponse()->getFullXmlResponse()]);

                    if ($response->getXmlResponse()->getStatus() == 0) {


                        $depositRecipient = new BaseRecipient();
                        $depositRecipient->setPofOfferAccepted(true);

                        $makeDepositionRequest = new MakeDepositionRequest();
                        $makeDepositionRequest->setDstAccount($phone)
                            ->setAmount(self::AMMOUNT)
                            ->setClientOrderId('202'.$num)
                            ->setCurrency(CurrencyCode::RUB)
                            ->setContract('')->setPaymentParams($depositRecipient);

                        Req::create(['Payload' => json_encode((array)$makeDepositionRequest)]);    
                        $makeDepositionResponse = $client->createDeposition($makeDepositionRequest);


                        //Проверка начисления денег

                        if ($makeDepositionResponse->getXmlResponse()->getStatus() == 0) {

                            $this->info($winner->Id.' - успешно');
                            $winner->update(['ProcessedAt' => Carbon::now(), 'WinnerStatus' => 'Успешно']);
                        } else {
                            $this->info($winner->Id.' - ошибка выплаты. Код - '.$makeDepositionResponse->getXmlResponse()->getStatus());
                            $winner->update(['ProcessedAt' => Carbon::now(), 'WinnerStatus' => 'Ошибка выплаты. Код - '.$makeDepositionResponse->getXmlResponse()->getStatus()]);
                        }

                        //
                    
                    } else {
                        $this->info($winner->Id.' - начисление невозможно. Код - '.$response->getXmlResponse()->getStatus());
                        $winner->update(['ProcessedAt' => Carbon::now(), 'WinnerStatus' => 'Начисление невозможно. Код - '.$response->getXmlResponse()->getStatus()]);
                    }
                } catch (ApiException $ex) {
                    $winner->update(['DepositionInfo' => $ex->getMessage(), 'ProcessedAt' => Carbon::now(), 'WinnerStatus' => 'Ошибка выплаты']);
                    $this->error($ex->getMessage());
                }

        }

        die();

        // $recipient = new BaseRecipient();
        // $recipient->setPofOfferAccepted(true);

        // $depositionRequest = new MakeDepositionRequest();
        // $depositionRequest->setDstAccount('79104202807')
        //     ->setAmount(1)
        //     ->setCurrency(CurrencyCode::RUB)
        //     ->setContract('')->setPaymentParams($recipient);
        // try {
        //     $response = $client->createDeposition($depositionRequest);
        //     var_dump($response->getXmlResponse());
        // } catch (\Exeption $ex) {
        //     $this->error($ex->getMessage());
        // }

        // die();


        // $recipient = new BaseRecipient();
        // $recipient->setPofOfferAccepted(true);

        //     $depositionRequest = new MakeDepositionRequest();
        //     $depositionRequest->setDstAccount('410012160316857')
        //     ->setAmount(1)            
        //     ->setCurrency(CurrencyCode::RUB)
        //     ->setContract('Гарантированный приз в акции Эпика')->setPaymentParams($recipient);

        //         try {
        //             $response = $client->createDeposition($depositionRequest);
        //             var_dump($response->getXmlResponse());
        //             var_dump($response->getXmlResponse()->getClientOrderId());
        //         } catch (ApiException $ex) {
        //             $this->error($ex->getMessage());
        //         }


        //         die();


       //  $recipient = new BankCardRecipient();
       // $recipient->setPdrLastName('Иванов')
       //            ->setPdrFirstName('Иван')
       //            ->setPdrMiddleName('Иванович')
       //            ->setDocNumber('1234567890')
       //            ->setPofOfferAccepted(true)
       //            ->setSkrDestinationCardSynonym('i8DZjk1u4a8TAm-577VSEzFh.SC.000.201905');

       //  $depositionRequest = new MakeDepositionRequest();
       //      $depositionRequest->setDstAccount('25700120202056919')
       //      ->setAmount(101)
       //      //->setClientOrderId(161354668986)
       //      ->setPaymentParams($recipient)
       //      ->setCurrency(CurrencyCode::RUB)
       //      ->setContract('test');

        

        die();
        
    }
}
