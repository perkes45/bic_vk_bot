<?php

namespace App\Validators;

use App\Models\User;

class Validator extends \Illuminate\Validation\Validator {

	public function validateUniqueEmail($attribute, $value, $parameters) {
		if (User::where(['Email' => $value])->count())
			return false;
		else
			return true;
	}

	public function validateUniquePhone($attribute, $value, $parameters) {
		if (User::where(['Phone' => clearPhone($value)])->count())
			return false;
		else
			return true;
	}

	public function validateCyrillic($attribute, $value, $parameters) {
		return $value ? preg_match('#^[ёа-я\\- ]+$#iu', $value) : true;
	}

	public function validatePassword($attribute, $value, $parameters) {
		return preg_match('/^[A-Za-z0-9]+$/', $value);
	}

	public function validatePhone($attribute, $value, $parameters) {
		return (bool) clearPhone($value);
	}

	public function validatePasses($attribute, $value, $parameters) {
		return preg_match("/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@()$%^&*=_{}[\]:;\"'|\\<>,.\/~`±§+-])/", $value);
	}

}
