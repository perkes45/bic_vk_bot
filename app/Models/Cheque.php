<?php

namespace App\Models;

/**
 * App\Models\Cheque
 *
 * @property string $Id
 * @property string $Number
 * @property string $Image
 * @property \Carbon\Carbon $CreatedAt
 * @property \Carbon\Carbon|null $UpdatedAt
 * @property string|null $DeletedAt
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\AbstractTable noLock()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cheque whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cheque whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cheque whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cheque whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cheque whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cheque whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $ChequeNumber
 * @property string $ChequeImage
 * @property string $UserId
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cheque whereChequeImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cheque whereChequeNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cheque whereUserId($value)
 * @property string $Status
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cheque whereStatus($value)
 * @property string|null $TradePoint
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cheque whereTradePoint($value)
 * @property string|null $RejectReason
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cheque whereRejectReason($value)
 * @property string|null $RejectReasonId
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cheque whereRejectReasonId($value)
 * @property string|null $QRData
 * @property string|null $QRCheckedAt
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cheque whereQRCheckedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cheque whereQRData($value)
 * @property string|null $FNSData
 * @property string|null $FNSDataCheckedAt
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cheque whereFNSData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cheque whereFNSDataCheckedAt($value)
 * @property string|null $FNSError
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cheque whereFNSError($value)
 * @property string|null $BuyDate
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cheque whereBuyDate($value)
 * @property int $RetailerId
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cheque whereRetailerId($value)
 * @property string|null $City
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cheque whereCity($value)
 * @property string|null $ShopName
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cheque whereShopName($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ChequeProduct[] $chequeProducts
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Drawing[] $drawings
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Product[] $products
 * @property-read \App\Models\RejectReason|null $rejectReason
 * @property-read \App\Models\Retailer|null $retailer
 * @property-read \App\Models\User $user
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Winner[] $winners
 * @property string|null $ShopId
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cheque whereShopId($value)
 * @property string|null $ModeratorProcessed
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cheque newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cheque newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cheque query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cheque whereModeratorProcessed($value)
 * @property int $IsVK
 * @property int|null $RemoteChequeId
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cheque whereIsVK($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cheque whereRemoteChequeId($value)
 * @property string|null $Uid
 * @property int $Approved
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cheque whereApproved($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cheque whereUid($value)
 * @property int|null $SKUCount
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cheque whereSKUCount($value)
 * @property string|null $ReceiptId
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Cheque whereReceiptId($value)
 * @property int|null $ChequeCanWinDaily
 * @property int|null $ChequeCanWinWeekly
 * @method static \Illuminate\Database\Eloquent\Builder|Cheque whereChequeCanWinDaily($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cheque whereChequeCanWinWeekly($value)
 */
use GuzzleHttp\Client;

class Cheque extends Base\Cheque {

    const STATUS_NEW = 'Pending';
    const STATUS_ACCEPTED = 'Accepted';
    const STATUS_REJECTED = 'Rejected';

    public function GetImage() {
        return env('APP_URL') . 'cheques/' . $this->ChequeImage;
    }

    public function GetImages() {
        return json_decode($this->ChequeImage);
    }

    public function GetQRData() {
        $client = new Client();
        $image = $this->GetImage();

        $opts = array('http' =>
            array(
                'header' => 'User-Agent: Artisan',
            )
        );

        $context = stream_context_create($opts);


        try {
            $response = $client->request('POST', 'http://qr-find-decode.kubernetes.local/qr', [
                'multipart' => [
                    [
                        'name' => 'image',
                        'contents' => fopen($image, 'r', false, $context)
                    ],
                    [
                        'name' => 'type',
                        'contents' => 'QR_CODE'
                    ]
                ]
            ]);
            $body = $response->getBody();
            $content = $body->getContents();
            $data = json_decode($content);
            $this->QRCheckedAt = \Carbon\Carbon::now();
            if ($data->codes) {
                $codes = json_encode($data->codes);
                $this->QRData = $codes;
                $this->save();
                return ['error' => 0, 'message' => $codes];
            } else {
                $this->save();
                return ['error' => 1, 'message' => 'No QR found'];
            }
        } catch (\Exception $ex) {
            return ['error' => 1, 'message' => $ex->getMessage()];
        }
    }

    public function GetFNSData() {
        $client = new Client();
        if (!$this->QRData)
            return ['error' => 1, 'message' => 'No QR data to check'];
        $codes = json_decode($this->QRData);
        $errors = [];
        $success = [];
        try {
            foreach ($codes as $code) {
                $response = $client->request('GET', 'http://receipts.dev.kubernetes.local/api/receipt?' . $code);
                $body = $response->getBody();
                $content = $body->getContents();
                $data = json_decode($content, true);
                if (isset($data['error'])) {
                    $errors[] = $data['error'];
                } else {
                    $success[] = $data;
                }
            }
            if ($errors) {
                $this->FNSError = json_encode($errors);
            } elseif ($success) {
                $this->FNSData = json_encode($success);
            }
            $this->FNSDataCheckedAt = \Carbon\Carbon::now();
            $this->save();
            return ['error' => 0, 'message' => "Errors - " . count($errors) . " Success " . count($success)];
        } catch (\Exception $ex) {
            return ['error' => 1, 'message' => $ex->getMessage()];
        }
    }
    
    public function code($flag = true){    
        $winners = $this->winners();
        if ($flag) {
            $winners->where('Status', 'Confirmed');
        }
        foreach ($winners->get() as $winner){
            if ($winner->codes->count()){
                return $winner->codes->first()->Value;
            }
        }
    }

}
