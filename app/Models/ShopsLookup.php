<?php
namespace App\Models;

/**
 * App\Models\ShopsLookup
 *
 * @property int $Id
 * @property string $Name
 * @property \Carbon\Carbon $CreatedAt
 * @property \Carbon\Carbon|null $UpdatedAt
 * @property string|null $DeletedAt
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\AbstractTable noLock()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopsLookup whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopsLookup whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopsLookup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopsLookup whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopsLookup whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $ExternaId
 * @property string $Region
 * @property string $City
 * @property string $Street
 * @property string $House
 * @property string $DopNum
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopsLookup whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopsLookup whereDopNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopsLookup whereExternaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopsLookup whereHouse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopsLookup whereRegion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopsLookup whereStreet($value)
 * @property string|null $FullAddress
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopsLookup whereFullAddress($value)
 * @property string $ShopName
 * @property string|null $ShopCity
 * @property string|null $ShopAddress
 * @property string|null $Lat
 * @property string|null $Lng
 * @property string|null $MetaData
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopsLookup newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopsLookup newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopsLookup query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopsLookup whereLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopsLookup whereLng($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopsLookup whereMetaData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopsLookup whereShopAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopsLookup whereShopCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ShopsLookup whereShopName($value)
 */
class ShopsLookup extends Base\ShopsLookup {
	//
}

