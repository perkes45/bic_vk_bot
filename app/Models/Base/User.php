<?php namespace App\Models\Base;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Base\User
 *
 * @property int $Id
 * @property int $GroupId
 * @property string|null $Uid
 * @property string|null $Email
 * @property string|null $Phone
 * @property string|null $Password
 * @property string|null $RememberToken
 * @property int $Enabled
 * @property int $Blocked
 * @property string $ActivationHash
 * @property string|null $ResetToken
 * @property string|null $ApiToken
 * @property string|null $ResetTokenCreatedAt
 * @property string|null $LastActionAt
 * @property string $FirstName
 * @property string|null $LastName
 * @property string|null $Region
 * @property string|null $City
 * @property string|null $BirthDate
 * @property int|null $Sex
 * @property int $RulesAccept
 * @property int $AgreementAccept
 * @property int $DistribAccept
 * @property string|null $RegistratonImage
 * @property string|null $InnImage
 * @property string|null $PassportImage
 * @property string|null $DeliveryAddress
 * @property string|null $RegistrationAddress
 * @property string|null $Inn
 * @property string|null $PassportWhenIssued
 * @property string|null $PassportIssued
 * @property string|null $PassportNumber
 * @property string|null $PassportSerialNumber
 * @property int $AlreadyWins
 * @property int|null $UserCanWinDaily
 * @property int|null $UserCanWinWeekly
 * @property \Illuminate\Support\Carbon $CreatedAt
 * @property \Illuminate\Support\Carbon|null $UpdatedAt
 * @property string|null $DeletedAt
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Cheque[] $cheques
 * @property-read int|null $cheques_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Feedback[] $feedback
 * @property-read int|null $feedback_count
 * @property-read \App\Models\Group $group
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Retailer[] $retailers
 * @property-read int|null $retailers_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\VKUser[] $vKUsers
 * @property-read int|null $v_k_users_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Winner[] $winners
 * @property-read int|null $winners_count
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AbstractTable noLock()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereActivationHash($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereAgreementAccept($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereAlreadyWins($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereApiToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereBirthDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereBlocked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereDeliveryAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereDistribAccept($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEnabled($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereInn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereInnImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLastActionAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassportImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassportIssued($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassportNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassportSerialNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassportWhenIssued($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRegion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRegistrationAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRegistratonImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereResetToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereResetTokenCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRulesAccept($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereSex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUserCanWinDaily($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUserCanWinWeekly($value)
 * @mixin \Eloquent
 */
class User extends AbstractTable {

    /**
     * Generated
     */

    protected $table = 'Users';
    protected $fillable = ['Id', 'GroupId', 'Uid', 'Email', 'Phone', 'Password', 'RememberToken', 'Enabled', 'Blocked', 'ActivationHash', 'ResetToken', 'ApiToken', 'ResetTokenCreatedAt', 'LastActionAt', 'FirstName', 'LastName', 'Region', 'City', 'BirthDate', 'Sex', 'RulesAccept', 'AgreementAccept', 'DistribAccept', 'RegistratonImage', 'InnImage', 'PassportImage', 'DeliveryAddress', 'RegistrationAddress', 'Inn', 'PassportWhenIssued', 'PassportIssued', 'PassportNumber', 'PassportSerialNumber', 'AlreadyWins', 'UserCanWinDaily', 'UserCanWinWeekly', 'CreatedAt', 'UpdatedAt', 'DeletedAt'];


    public function group() {
        return $this->belongsTo(\App\Models\Group::class, 'GroupId', 'Id');
    }

    public function retailers() {
        return $this->belongsToMany(\App\Models\Retailer::class, 'Feedbacks', 'UserId', 'RetailId');
    }

    public function cheques() {
        return $this->hasMany(\App\Models\Cheque::class, 'UserId', 'Id');
    }

    public function feedback() {
        return $this->hasMany(\App\Models\Feedback::class, 'UserId', 'Id');
    }

    public function vKUsers() {
        return $this->hasMany(\App\Models\VKUser::class, 'UserId', 'Id');
    }

    public function winners() {
        return $this->hasMany(\App\Models\Winner::class, 'UserId', 'Id');
    }


}
