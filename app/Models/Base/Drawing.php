<?php namespace App\Models\Base;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Base\Drawing
 *
 * @property int $Id
 * @property string $PeriodStart
 * @property string $PeriodEnd
 * @property string $FireAt
 * @property string $Type
 * @property \Illuminate\Support\Carbon $CreatedAt
 * @property \Illuminate\Support\Carbon|null $UpdatedAt
 * @property string|null $DeletedAt
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Winner[] $winners
 * @property-read int|null $winners_count
 * @method static \Illuminate\Database\Eloquent\Builder|Drawing newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Drawing newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AbstractTable noLock()
 * @method static \Illuminate\Database\Eloquent\Builder|Drawing query()
 * @method static \Illuminate\Database\Eloquent\Builder|Drawing whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Drawing whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Drawing whereFireAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Drawing whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Drawing wherePeriodEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Drawing wherePeriodStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Drawing whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Drawing whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Drawing extends AbstractTable {

    /**
     * Generated
     */

    protected $table = 'Drawings';
    protected $fillable = ['Id', 'PeriodStart', 'PeriodEnd', 'FireAt', 'Type', 'CreatedAt', 'UpdatedAt', 'DeletedAt'];


    public function winners() {
        return $this->hasMany(\App\Models\Winner::class, 'DrawingId', 'Id');
    }


}
