<?php namespace App\Models\Base;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Base\Cheque
 *
 * @property int $Id
 * @property string|null $Uid
 * @property string|null $ReceiptId
 * @property string $ChequeImage
 * @property string|null $ChequeNumber
 * @property string|null $BuyDate
 * @property string|null $Number
 * @property int|null $RetailerId
 * @property int $UserId
 * @property string $Status
 * @property int $Approved
 * @property string|null $RejectReason
 * @property string|null $City
 * @property string|null $ShopName
 * @property string|null $ShopId
 * @property string|null $QRData
 * @property string|null $QRCheckedAt
 * @property string|null $FNSData
 * @property string|null $FNSError
 * @property string|null $FNSDataCheckedAt
 * @property string|null $ModeratorProcessed
 * @property int|null $SKUCount
 * @property int|null $ChequeCanWinDaily
 * @property int|null $ChequeCanWinWeekly
 * @property \Illuminate\Support\Carbon $CreatedAt
 * @property \Illuminate\Support\Carbon|null $UpdatedAt
 * @property string|null $DeletedAt
 * @property-read \App\Models\Retailer|null $retailer
 * @property-read \App\Models\User $user
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Winner[] $winners
 * @property-read int|null $winners_count
 * @method static \Illuminate\Database\Eloquent\Builder|Cheque newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Cheque newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AbstractTable noLock()
 * @method static \Illuminate\Database\Eloquent\Builder|Cheque query()
 * @method static \Illuminate\Database\Eloquent\Builder|Cheque whereApproved($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cheque whereBuyDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cheque whereChequeCanWinDaily($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cheque whereChequeCanWinWeekly($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cheque whereChequeImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cheque whereChequeNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cheque whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cheque whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cheque whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cheque whereFNSData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cheque whereFNSDataCheckedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cheque whereFNSError($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cheque whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cheque whereModeratorProcessed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cheque whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cheque whereQRCheckedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cheque whereQRData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cheque whereReceiptId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cheque whereRejectReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cheque whereRetailerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cheque whereSKUCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cheque whereShopId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cheque whereShopName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cheque whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cheque whereUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cheque whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cheque whereUserId($value)
 * @mixin \Eloquent
 */
class Cheque extends AbstractTable {

    /**
     * Generated
     */

    protected $table = 'Cheques';
    protected $fillable = ['Id', 'Uid', 'ReceiptId', 'ChequeImage', 'ChequeNumber', 'BuyDate', 'Number', 'RetailerId', 'UserId', 'Status', 'Approved', 'RejectReason', 'City', 'ShopName', 'ShopId', 'QRData', 'QRCheckedAt', 'FNSData', 'FNSError', 'FNSDataCheckedAt', 'ModeratorProcessed', 'SKUCount', 'ChequeCanWinDaily', 'ChequeCanWinWeekly', 'CreatedAt', 'UpdatedAt', 'DeletedAt'];


    public function retailer() {
        return $this->belongsTo(\App\Models\Retailer::class, 'RetailerId', 'Id');
    }

    public function user() {
        return $this->belongsTo(\App\Models\User::class, 'UserId', 'Id');
    }

    public function winners() {
        return $this->hasMany(\App\Models\Winner::class, 'ChequeId', 'Id');
    }


}
