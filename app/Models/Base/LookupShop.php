<?php namespace App\Models\Base;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Base\LookupShop
 *
 * @property int $Id
 * @property string|null $ShopCity
 * @property string|null $ShopAddress
 * @property string|null $Lat
 * @property string|null $Lng
 * @property \Illuminate\Support\Carbon $CreatedAt
 * @property \Illuminate\Support\Carbon|null $UpdatedAt
 * @property string|null $DeletedAt
 * @method static \Illuminate\Database\Eloquent\Builder|LookupShop newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LookupShop newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AbstractTable noLock()
 * @method static \Illuminate\Database\Eloquent\Builder|LookupShop query()
 * @method static \Illuminate\Database\Eloquent\Builder|LookupShop whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LookupShop whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LookupShop whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LookupShop whereLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LookupShop whereLng($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LookupShop whereShopAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LookupShop whereShopCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LookupShop whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class LookupShop extends AbstractTable {

    /**
     * Generated
     */

    protected $table = 'LookupShops';
    protected $fillable = ['Id', 'ShopCity', 'ShopAddress', 'Lat', 'Lng', 'CreatedAt', 'UpdatedAt', 'DeletedAt'];



}
