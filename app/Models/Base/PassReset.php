<?php namespace App\Models\Base;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Base\PassReset
 *
 * @property int $Id
 * @property string $Email
 * @property \Illuminate\Support\Carbon $CreatedAt
 * @property \Illuminate\Support\Carbon|null $UpdatedAt
 * @property string|null $DeletedAt
 * @method static \Illuminate\Database\Eloquent\Builder|PassReset newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PassReset newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AbstractTable noLock()
 * @method static \Illuminate\Database\Eloquent\Builder|PassReset query()
 * @method static \Illuminate\Database\Eloquent\Builder|PassReset whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PassReset whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PassReset whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PassReset whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PassReset whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PassReset extends AbstractTable {

    /**
     * Generated
     */

    protected $table = 'PassResets';
    protected $fillable = ['Id', 'Email', 'CreatedAt', 'UpdatedAt', 'DeletedAt'];



}
