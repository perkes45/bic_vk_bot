<?php namespace App\Models\Base;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Base\RejectReason
 *
 * @property int $Id
 * @property string $Code
 * @property string $Name
 * @property \Illuminate\Support\Carbon $CreatedAt
 * @property \Illuminate\Support\Carbon|null $UpdatedAt
 * @property string|null $DeletedAt
 * @method static \Illuminate\Database\Eloquent\Builder|RejectReason newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RejectReason newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AbstractTable noLock()
 * @method static \Illuminate\Database\Eloquent\Builder|RejectReason query()
 * @method static \Illuminate\Database\Eloquent\Builder|RejectReason whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RejectReason whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RejectReason whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RejectReason whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RejectReason whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RejectReason whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class RejectReason extends AbstractTable {

    /**
     * Generated
     */

    protected $table = 'RejectReasons';
    protected $fillable = ['Id', 'Code', 'Name', 'CreatedAt', 'UpdatedAt', 'DeletedAt'];



}
