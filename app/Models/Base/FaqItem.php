<?php namespace App\Models\Base;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Base\FaqItem
 *
 * @property int $Id
 * @property int|null $CategoryId
 * @property string $Title
 * @property string $Body
 * @property \Illuminate\Support\Carbon $CreatedAt
 * @property \Illuminate\Support\Carbon|null $UpdatedAt
 * @property string|null $DeletedAt
 * @property-read \App\Models\FaqCategory|null $faqCategory
 * @method static \Illuminate\Database\Eloquent\Builder|FaqItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FaqItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AbstractTable noLock()
 * @method static \Illuminate\Database\Eloquent\Builder|FaqItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|FaqItem whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqItem whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqItem whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqItem whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqItem whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class FaqItem extends AbstractTable {

    /**
     * Generated
     */

    protected $table = 'FaqItems';
    protected $fillable = ['Id', 'CategoryId', 'Title', 'Body', 'CreatedAt', 'UpdatedAt', 'DeletedAt'];


    public function faqCategory() {
        return $this->belongsTo(\App\Models\FaqCategory::class, 'CategoryId', 'Id');
    }


}
