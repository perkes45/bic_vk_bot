<?php namespace App\Models\Base;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Base\ShopsLookup
 *
 * @property int $Id
 * @property string $ShopName
 * @property string|null $ShopCity
 * @property string|null $ShopAddress
 * @property string|null $Lng
 * @property string|null $Lat
 * @property string|null $MetaData
 * @property \Illuminate\Support\Carbon $CreatedAt
 * @property \Illuminate\Support\Carbon|null $UpdatedAt
 * @property string|null $DeletedAt
 * @method static \Illuminate\Database\Eloquent\Builder|ShopsLookup newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ShopsLookup newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AbstractTable noLock()
 * @method static \Illuminate\Database\Eloquent\Builder|ShopsLookup query()
 * @method static \Illuminate\Database\Eloquent\Builder|ShopsLookup whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShopsLookup whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShopsLookup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShopsLookup whereLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShopsLookup whereLng($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShopsLookup whereMetaData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShopsLookup whereShopAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShopsLookup whereShopCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShopsLookup whereShopName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ShopsLookup whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ShopsLookup extends AbstractTable {

    /**
     * Generated
     */

    protected $table = 'ShopsLookup';
    protected $fillable = ['Id', 'ShopName', 'ShopCity', 'ShopAddress', 'Lng', 'Lat', 'MetaData', 'CreatedAt', 'UpdatedAt', 'DeletedAt'];



}
