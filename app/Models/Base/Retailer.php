<?php namespace App\Models\Base;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Base\Retailer
 *
 * @property int $Id
 * @property string $Name
 * @property \Illuminate\Support\Carbon $CreatedAt
 * @property \Illuminate\Support\Carbon|null $UpdatedAt
 * @property string|null $DeletedAt
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Cheque[] $cheques
 * @property-read int|null $cheques_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Feedback[] $feedback
 * @property-read int|null $feedback_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|Retailer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Retailer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AbstractTable noLock()
 * @method static \Illuminate\Database\Eloquent\Builder|Retailer query()
 * @method static \Illuminate\Database\Eloquent\Builder|Retailer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Retailer whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Retailer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Retailer whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Retailer whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Retailer extends AbstractTable {

    /**
     * Generated
     */

    protected $table = 'Retailers';
    protected $fillable = ['Id', 'Name', 'CreatedAt', 'UpdatedAt', 'DeletedAt'];


    public function users() {
        return $this->belongsToMany(\App\Models\User::class, 'Feedbacks', 'RetailId', 'UserId');
    }

    public function cheques() {
        return $this->hasMany(\App\Models\Cheque::class, 'RetailerId', 'Id');
    }

    public function feedback() {
        return $this->hasMany(\App\Models\Feedback::class, 'RetailId', 'Id');
    }


}
