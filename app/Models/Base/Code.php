<?php namespace App\Models\Base;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Base\Code
 *
 * @property int $Id
 * @property string $Value
 * @property int|null $WinnerId
 * @property \Illuminate\Support\Carbon $CreatedAt
 * @property \Illuminate\Support\Carbon|null $UpdatedAt
 * @property string|null $DeletedAt
 * @property-read \App\Models\Winner|null $winner
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\Code newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\Code newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\AbstractTable noLock()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\Code query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\Code whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\Code whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\Code whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\Code whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\Code whereValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\Code whereWinnerId($value)
 * @mixin \Eloquent
 */
class Code extends AbstractTable {

    /**
     * Generated
     */

    protected $table = 'Codes';
    protected $fillable = ['Id', 'Value', 'WinnerId', 'CreatedAt', 'UpdatedAt', 'DeletedAt'];


    public function winner() {
        return $this->belongsTo(\App\Models\Winner::class, 'WinnerId', 'Id');
    }


}
