<?php namespace App\Models\Base;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Base\Winner
 *
 * @property int $Id
 * @property int|null $ChequeId
 * @property int|null $UserId
 * @property int|null $DrawingId
 * @property string $WinType
 * @property int $PrizeId
 * @property string|null $Price
 * @property string|null $PriceWC
 * @property string|null $Status
 * @property string|null $Sert
 * @property string|null $Address
 * @property string|null $FatherName
 * @property string|null $BirthDate
 * @property string|null $PassportInfo
 * @property string|null $PassportWhenIssued
 * @property string|null $PassportIssued
 * @property string|null $WinnerPhone
 * @property string|null $InnNumber
 * @property string|null $PassportMain
 * @property string|null $PassportRegistration
 * @property string|null $PrizeDocument
 * @property string|null $WinnerCheques
 * @property string|null $ProcessDocument
 * @property string|null $InnImage
 * @property string|null $DeliveryStatus
 * @property string|null $ScoreNumber
 * @property string|null $CardNumber
 * @property string|null $Bank
 * @property string|null $BankInn
 * @property string|null $BankKPP
 * @property string|null $BankBik
 * @property string|null $YWallet
 * @property string|null $WinnerStatus
 * @property string|null $DepositionInfo
 * @property string|null $ProcessedAt
 * @property \Illuminate\Support\Carbon $CreatedAt
 * @property \Illuminate\Support\Carbon|null $UpdatedAt
 * @property string|null $DeletedAt
 * @property-read \App\Models\Cheque|null $cheque
 * @property-read \App\Models\Drawing|null $drawing
 * @property-read \App\Models\Prize $prize
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Sert[] $serts
 * @property-read int|null $serts_count
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|Winner newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Winner newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AbstractTable noLock()
 * @method static \Illuminate\Database\Eloquent\Builder|Winner query()
 * @method static \Illuminate\Database\Eloquent\Builder|Winner whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Winner whereBank($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Winner whereBankBik($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Winner whereBankInn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Winner whereBankKPP($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Winner whereBirthDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Winner whereCardNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Winner whereChequeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Winner whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Winner whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Winner whereDeliveryStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Winner whereDepositionInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Winner whereDrawingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Winner whereFatherName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Winner whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Winner whereInnImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Winner whereInnNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Winner wherePassportInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Winner wherePassportIssued($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Winner wherePassportMain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Winner wherePassportRegistration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Winner wherePassportWhenIssued($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Winner wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Winner wherePriceWC($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Winner wherePrizeDocument($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Winner wherePrizeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Winner whereProcessDocument($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Winner whereProcessedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Winner whereScoreNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Winner whereSert($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Winner whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Winner whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Winner whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Winner whereWinType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Winner whereWinnerCheques($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Winner whereWinnerPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Winner whereWinnerStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Winner whereYWallet($value)
 * @mixin \Eloquent
 */
class Winner extends AbstractTable {

    /**
     * Generated
     */

    protected $table = 'Winners';
    protected $fillable = ['Id', 'ChequeId', 'UserId', 'DrawingId', 'WinType', 'PrizeId', 'Price', 'PriceWC', 'Status', 'Sert', 'Address', 'FatherName', 'BirthDate', 'PassportInfo', 'PassportWhenIssued', 'PassportIssued', 'WinnerPhone', 'InnNumber', 'PassportMain', 'PassportRegistration', 'PrizeDocument', 'WinnerCheques', 'ProcessDocument', 'InnImage', 'DeliveryStatus', 'ScoreNumber', 'CardNumber', 'Bank', 'BankInn', 'BankKPP', 'BankBik', 'YWallet', 'WinnerStatus', 'DepositionInfo', 'ProcessedAt', 'CreatedAt', 'UpdatedAt', 'DeletedAt'];


    public function cheque() {
        return $this->belongsTo(\App\Models\Cheque::class, 'ChequeId', 'Id');
    }

    public function user() {
        return $this->belongsTo(\App\Models\User::class, 'UserId', 'Id');
    }

    public function drawing() {
        return $this->belongsTo(\App\Models\Drawing::class, 'DrawingId', 'Id');
    }

    public function prize() {
        return $this->belongsTo(\App\Models\Prize::class, 'PrizeId', 'Id');
    }

    public function serts() {
        return $this->hasMany(\App\Models\Sert::class, 'WinnerId', 'Id');
    }


}
