<?php namespace App\Models\Base;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Base\Feedback
 *
 * @property int $Id
 * @property int|null $UserId
 * @property int|null $RetailId
 * @property string|null $Theme
 * @property string|null $PersonName
 * @property string $Email
 * @property string $Message
 * @property int|null $Answered
 * @property string|null $Image
 * @property int|null $ProcessAccept
 * @property \Illuminate\Support\Carbon $CreatedAt
 * @property \Illuminate\Support\Carbon|null $UpdatedAt
 * @property string|null $DeletedAt
 * @property-read \App\Models\Retailer|null $retailer
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AbstractTable noLock()
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback query()
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereAnswered($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback wherePersonName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereProcessAccept($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereRetailId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereTheme($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Feedback whereUserId($value)
 * @mixin \Eloquent
 */
class Feedback extends AbstractTable {

    /**
     * Generated
     */

    protected $table = 'Feedbacks';
    protected $fillable = ['Id', 'UserId', 'RetailId', 'Theme', 'PersonName', 'Email', 'Message', 'Answered', 'Image', 'ProcessAccept', 'CreatedAt', 'UpdatedAt', 'DeletedAt'];


    public function user() {
        return $this->belongsTo(\App\Models\User::class, 'UserId', 'Id');
    }

    public function retailer() {
        return $this->belongsTo(\App\Models\Retailer::class, 'RetailId', 'Id');
    }


}
