<?php namespace App\Models\Base;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Base\Prize
 *
 * @property int $Id
 * @property string $PrizeName
 * @property string $PrizeType
 * @property string $PrizeCategory
 * @property \Illuminate\Support\Carbon $CreatedAt
 * @property \Illuminate\Support\Carbon|null $UpdatedAt
 * @property string|null $DeletedAt
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Winner[] $winners
 * @property-read int|null $winners_count
 * @method static \Illuminate\Database\Eloquent\Builder|Prize newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Prize newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AbstractTable noLock()
 * @method static \Illuminate\Database\Eloquent\Builder|Prize query()
 * @method static \Illuminate\Database\Eloquent\Builder|Prize whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Prize whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Prize whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Prize wherePrizeCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Prize wherePrizeName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Prize wherePrizeType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Prize whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Prize extends AbstractTable {

    /**
     * Generated
     */

    protected $table = 'Prizes';
    protected $fillable = ['Id', 'PrizeName', 'PrizeType', 'PrizeCategory', 'CreatedAt', 'UpdatedAt', 'DeletedAt'];


    public function winners() {
        return $this->hasMany(\App\Models\Winner::class, 'PrizeId', 'Id');
    }


}
