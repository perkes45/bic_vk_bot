<?php namespace App\Models\Base;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Base\VKUser
 *
 * @property int $Id
 * @property int|null $UserId
 * @property string|null $FirstName
 * @property string|null $LastName
 * @property string|null $Dataset
 * @property string|null $UserDataset
 * @property string|null $ChequeDataset
 * @property int $VKId
 * @property string|null $State
 * @property int|null $Code
 * @property string|null $FlyEmail
 * @property \Illuminate\Support\Carbon $CreatedAt
 * @property \Illuminate\Support\Carbon|null $UpdatedAt
 * @property string|null $DeletedAt
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|VKUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VKUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AbstractTable noLock()
 * @method static \Illuminate\Database\Eloquent\Builder|VKUser query()
 * @method static \Illuminate\Database\Eloquent\Builder|VKUser whereChequeDataset($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VKUser whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VKUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VKUser whereDataset($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VKUser whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VKUser whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VKUser whereFlyEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VKUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VKUser whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VKUser whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VKUser whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VKUser whereUserDataset($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VKUser whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VKUser whereVKId($value)
 * @mixin \Eloquent
 */
class VKUser extends AbstractTable {

    /**
     * Generated
     */

    protected $table = 'VKUsers';
    protected $fillable = ['Id', 'UserId', 'FirstName', 'LastName', 'Dataset', 'UserDataset', 'ChequeDataset', 'VKId', 'State', 'Code', 'FlyEmail', 'CreatedAt', 'UpdatedAt', 'DeletedAt'];


    public function user() {
        return $this->belongsTo(\App\Models\User::class, 'UserId', 'Id');
    }


}
