<?php namespace App\Models\Base;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Base\Attempt
 *
 * @property int $Id
 * @property string $Email
 * @property \Illuminate\Support\Carbon $CreatedAt
 * @property \Illuminate\Support\Carbon|null $UpdatedAt
 * @property string|null $DeletedAt
 * @method static \Illuminate\Database\Eloquent\Builder|Attempt newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Attempt newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AbstractTable noLock()
 * @method static \Illuminate\Database\Eloquent\Builder|Attempt query()
 * @method static \Illuminate\Database\Eloquent\Builder|Attempt whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Attempt whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Attempt whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Attempt whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Attempt whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Attempt extends AbstractTable {

    /**
     * Generated
     */

    protected $table = 'Attempts';
    protected $fillable = ['Id', 'Email', 'CreatedAt', 'UpdatedAt', 'DeletedAt'];



}
