<?php namespace App\Models\Base;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Base\Sert
 *
 * @property int $Id
 * @property int|null $WinnerId
 * @property string|null $Number
 * @property string|null $ExpireDate
 * @property string|null $Pin
 * @property string|null $Type
 * @property \Illuminate\Support\Carbon $CreatedAt
 * @property \Illuminate\Support\Carbon|null $UpdatedAt
 * @property string|null $DeletedAt
 * @property-read \App\Models\Winner|null $winner
 * @method static \Illuminate\Database\Eloquent\Builder|Sert newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Sert newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AbstractTable noLock()
 * @method static \Illuminate\Database\Eloquent\Builder|Sert query()
 * @method static \Illuminate\Database\Eloquent\Builder|Sert whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Sert whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Sert whereExpireDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Sert whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Sert whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Sert wherePin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Sert whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Sert whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Sert whereWinnerId($value)
 * @mixin \Eloquent
 */
class Sert extends AbstractTable {

    /**
     * Generated
     */

    protected $table = 'Serts';
    protected $fillable = ['Id', 'WinnerId', 'Number', 'ExpireDate', 'Pin', 'Type', 'CreatedAt', 'UpdatedAt', 'DeletedAt'];


    public function winner() {
        return $this->belongsTo(\App\Models\Winner::class, 'WinnerId', 'Id');
    }


}
