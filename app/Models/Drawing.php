<?php
namespace App\Models;

/**
 * App\Models\Drawing
 *
 * @property int $Id
 * @property string $PeriodStart
 * @property string $PeriodEnd
 * @property string $FireAt
 * @property string $Type
 * @property \Illuminate\Support\Carbon $CreatedAt
 * @property \Illuminate\Support\Carbon|null $UpdatedAt
 * @property string|null $DeletedAt
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Drawing newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Drawing newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\AbstractTable noLock()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Drawing query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Drawing whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Drawing whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Drawing whereFireAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Drawing whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Drawing wherePeriodEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Drawing wherePeriodStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Drawing whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Drawing whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Drawing extends Base\Drawing {
	
	public static function getActive(){
		$date = \Carbon\Carbon::now()->format('Y-m-d H:i:s');

		return self::where('PeriodStart', '<=', $date)->where('PeriodEnd', '>=', $date)->where('Type', 'weekly')->first();
	}


	public function getScore(){ 
		$min = 500000;
		$max = 1000000;

		$cheques = Cheque::where('CreatedAt', '>=', $this->PeriodStart)->where('CreatedAt', '<=', $this->PeriodEnd)->where('Approved', 1)->count();

		$sup = $cheques * 500;

		$res = $min + $sup;

		return $res > $max ? $max : $res;
	}

}

