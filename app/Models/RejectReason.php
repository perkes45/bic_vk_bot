<?php
namespace App\Models;

/**
 * App\Models\RejectReason
 *
 * @property string $Id
 * @property string $Name
 * @property \Carbon\Carbon $CreatedAt
 * @property \Carbon\Carbon|null $UpdatedAt
 * @property string|null $DeletedAt
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Cheque[] $cheques
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\AbstractTable noLock()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RejectReason whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RejectReason whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RejectReason whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RejectReason whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RejectReason whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $Code
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RejectReason whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RejectReason newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RejectReason newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RejectReason query()
 */
class RejectReason extends Base\RejectReason {
	//
}

