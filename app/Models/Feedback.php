<?php
namespace App\Models;

/**
 * App\Models\Feedback
 *
 * @property string $Id
 * @property string $PersonName
 * @property string $Email
 * @property string $Message
 * @property \Carbon\Carbon $CreatedAt
 * @property \Carbon\Carbon|null $UpdatedAt
 * @property string|null $DeletedAt
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\AbstractTable noLock()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Feedback whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Feedback whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Feedback whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Feedback whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Feedback whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Feedback wherePersonName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Feedback whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $UserId
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Feedback whereUserId($value)
 * @property bool|null $Answered
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Feedback whereAnswered($value)
 * @property int $RetailId
 * @property string|null $Image
 * @property int|null $ProcessAccept
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Feedback whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Feedback whereProcessAccept($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Feedback whereRetailId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Feedback newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Feedback newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Feedback query()
 * @property string|null $Theme
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Feedback whereTheme($value)
 */
class Feedback extends Base\Feedback {
	
    public function GetImage(){
        return $this->Image ? url('/images/feedback/'.$this->Image) : null;
    }
    
}

