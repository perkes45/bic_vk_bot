<?php

namespace App\Models;

/**
 * App\Models\VKUser
 *
 * @property int $Id
 * @property string|null $FirstName
 * @property string|null $LastName
 * @property string|null $Dataset
 * @property int $VKId
 * @property \Illuminate\Support\Carbon $CreatedAt
 * @property \Illuminate\Support\Carbon|null $UpdatedAt
 * @property string|null $DeletedAt
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VKUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VKUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\AbstractTable noLock()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VKUser query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VKUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VKUser whereDataset($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VKUser whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VKUser whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VKUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VKUser whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VKUser whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VKUser whereVKId($value)
 * @mixin \Eloquent
 * @property int $UserId
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VKUser whereUserId($value)
 * @property string|null $State
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VKUser whereState($value)
 * @property string|null $UserDataset
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VKUser whereUserDataset($value)
 * @property int|null $Code
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VKUser whereCode($value)
 * @property string|null $ChequeDataset
 * @property string|null $FlyEmail
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VKUser whereChequeDataset($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\VKUser whereFlyEmail($value)
 */
class VKUser extends Base\VKUser {

    public function ClearState() {
        $this->update(['State' => null]);
    }

    public function RenderCheques() {
        if (!$this->UserId) {
            return 'Вы не авторизованы';
        }
        if (!$this->user->cheques->count()) {
            return 'Вы еще не загрузили ни одного чека';
        }

        $text = '';

        foreach ($this->user->cheques as $k => $cheque) {
            $link = $cheque->GetImage();

            if ($cheque->Status == 'REVIEWED'){
                    if ($cheque->RejectReason){
                        $status = 'Отклонен - '.$cheque->RejectReason;

                    } else {
                        $status = 'Принят';
                    }
                } else {
                    $status = 'В обработке';
                } 

            
            $num = ++$k;
            $text .= $num.'.' . "\r\n";
            $text .= 'Номер чека - ' . $cheque->Id . "\r\n";
            $text .= 'Статус - ' . $status . "\r\n";
            
            
            if ($cheque->winners()->count()) {
                foreach ($cheque->winners as $winner) {
                    $text .= 'Приз - ' . $winner->prize->PrizeName . "\r\n";
                }
                
            }
            $text .= "\r\n";
                                
            
        };

        return $text;
    }

}
