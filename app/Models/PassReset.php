<?php
namespace App\Models;

/**
 * App\Models\PassReset
 *
 * @property string $Id
 * @property string $Email
 * @property \Carbon\Carbon $CreatedAt
 * @property \Carbon\Carbon|null $UpdatedAt
 * @property string|null $DeletedAt
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\AbstractTable noLock()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PassReset whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PassReset whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PassReset whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PassReset whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PassReset whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PassReset newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PassReset newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PassReset query()
 */
class PassReset extends Base\PassReset {
	//
}

