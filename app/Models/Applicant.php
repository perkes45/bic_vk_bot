<?php
namespace App\Models;

/**
 * App\Models\Applicant
 *
 * @property int $Id
 * @property int|null $UserId
 * @property int|null $ChequeId
 * @property \Illuminate\Support\Carbon $CreatedAt
 * @property \Illuminate\Support\Carbon|null $UpdatedAt
 * @property string|null $DeletedAt
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Applicant newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Applicant newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\AbstractTable noLock()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Applicant query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Applicant whereChequeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Applicant whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Applicant whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Applicant whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Applicant whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Applicant whereUserId($value)
 * @mixin \Eloquent
 */
class Applicant extends Base\Applicant {
	//
}

