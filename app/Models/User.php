<?php

namespace App\Models;

/**
 * App\Models\User
 *
 * @property string $Id
 * @property \Carbon\Carbon $CreatedAt
 * @property \Carbon\Carbon|null $UpdatedAt
 * @property string|null $DeletedAt
 * @property string $GroupId
 * @property string|null $Email
 * @property string|null $Password
 * @property string|null $RememberToken
 * @property bool $Enabled
 * @property string|null $ResetToken
 * @property string|null $ResetTokenCreatedAt
 * @property string|null $LastActionAt
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\AbstractTable noLock()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEnabled($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereLastActionAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereResetToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereResetTokenCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $FirstName
 * @property string $LastName
 * @property string $City
 * @property string $Phone
 * @property string $BirthDate
 * @property bool $RulesAccept
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereBirthDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRulesAccept($value)
 * @property-read \App\Models\Group $group
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Cheque[] $cheques
 * @property string|null $PassportSerialNumber
 * @property string|null $PassportNumber
 * @property string|null $PassportIssued
 * @property string|null $PassportWhenIssued
 * @property string|null $Inn
 * @property string|null $RegistrationAddress
 * @property string|null $PassportImage
 * @property string|null $InnImage
 * @property string|null $DeliveryAddress
 * @property string|null $DailyChequeId
 * @property string|null $WeeklyChequeId
 * @property string|null $MainChequeId
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Winner[] $winners
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereDailyChequeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereDeliveryAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereInn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereInnImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereMainChequeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePassportImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePassportIssued($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePassportNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePassportSerialNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePassportWhenIssued($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRegistrationAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereWeeklyChequeId($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Transaction[] $transactions
 * @property string|null $RegistratonImage
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Feedback[] $feedback
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRegistratonImage($value)
 * @property string|null $OperatorId
 * @property-read \App\Models\MobileOperator|null $mobileOperator
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereOperatorId($value)
 * @property string $Region
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRegion($value)
 * @property int $AgreementAccept
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereAgreementAccept($value)
 * @property string $ActivationHash
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereActivationHash($value)
 * @property int $DistribAccept
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereDistribAccept($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Retailer[] $retailers
 * @property int|null $Sex
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereSex($value)
 * @property int $Blocked
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereBlocked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User query()
 * @property int|null $RemoteId
 * @property string|null $ApiToken
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereApiToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRemoteId($value)
 * @property string|null $Uid
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUid($value)
 * @property int $AlreadyWins
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereAlreadyWins($value)
 * @property-read int|null $winners_count
 * @property int|null $UserCanWinDaily
 * @property int|null $UserCanWinWeekly
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUserCanWinDaily($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUserCanWinWeekly($value)
 */
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Carbon\Carbon;

class User extends Base\User implements AuthorizableContract, AuthenticatableContract, CanResetPasswordContract {

    use CanResetPassword,
        Authorizable;

use Authenticatable {
        getAuthPassword as getAuthPassword_;
    }

    const FIELDS = [
        'BirthDate',
        'Region',
        'City'
    ];
    const MIN_AGE = 18;
    const MAX_AGE = 90;
    const CHEQUES_LIMIT = 10;
    const UPLOAD_DELAY = 180;

    public function getAuthPassword() {
        return $this->Password;
    }

    public function CanLoadCheques() {
        $cheques = $this->cheques()->count();
        if ($cheques >= self::CHEQUES_LIMIT) {
            $lastCreated = $this->cheques()->orderBy('CreatedAt', 'DESC')->first()->CreatedAt;
            $firstCreated = $this->cheques()->orderBy('CreatedAt', 'DESC')->skip(self::CHEQUES_LIMIT - 1)->take(1)->first()->CreatedAt;

            if ($lastCreated->diffInMinutes($firstCreated) <= 1) {
                if ($lastCreated->diffInMinutes(Carbon::now()) <= self::UPLOAD_DELAY) {
                    return false;
                }
            }
        }
        return true;
    }

    public function getRememberTokenName() {
        return 'RememberToken';
    }

    public static function credentials($email, $password) {
        return ['Email' => $email ?? '', 'Password' => $password ?? '', 'Enabled' => true];
    }

    public function winners() {
        return $this->hasManyThrough('App\Models\Winner', 'App\Models\Cheque', 'UserId', 'ChequeId', 'Id');
    }

    public function infoNeeded() {

        if ($this->winners()->count()) {
            foreach (self::FIELDS as $field) {
                if (empty($this->$field))
                    return true;
            }
            return false;
        } else
            return false;
    }

    public function isAdmin() {
        return $this->GroupId == Group::ADMIN_GROUP_ID;
    }

    public function GetHiddenMail(){
        $email = substr($this->Email, 3);
        $email[4] = '*';
        return '***'.$email;
    }

}
