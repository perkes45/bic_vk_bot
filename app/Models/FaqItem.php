<?php
namespace App\Models;

/**
 * App\Models\FaqItem
 *
 * @property int $Id
 * @property int|null $CategoryId
 * @property string $Title
 * @property string $Body
 * @property \Illuminate\Support\Carbon $CreatedAt
 * @property \Illuminate\Support\Carbon|null $UpdatedAt
 * @property string|null $DeletedAt
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FaqItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FaqItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\AbstractTable noLock()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FaqItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FaqItem whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FaqItem whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FaqItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FaqItem whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FaqItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FaqItem whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FaqItem whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class FaqItem extends Base\FaqItem {
	//
}

