<?php
namespace App\Models;

/**
 * App\Models\Response
 *
 * @property int $Id
 * @property string $Body
 * @property string|null $UserId
 * @property \Illuminate\Support\Carbon $CreatedAt
 * @property \Illuminate\Support\Carbon|null $UpdatedAt
 * @property string|null $DeletedAt
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Response newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Response newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\AbstractTable noLock()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Response query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Response whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Response whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Response whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Response whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Response whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Response whereUserId($value)
 * @mixin \Eloquent
 */
class Response extends Base\Response {
	//
}

