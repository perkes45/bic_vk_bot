<?php
namespace App\Models;

/**
 * App\Models\Attempt
 *
 * @property int $Id
 * @property string $Email
 * @property \Illuminate\Support\Carbon $CreatedAt
 * @property \Illuminate\Support\Carbon|null $UpdatedAt
 * @property string|null $DeletedAt
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Attempt newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Attempt newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\AbstractTable noLock()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Attempt query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Attempt whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Attempt whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Attempt whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Attempt whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Attempt whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Attempt extends Base\Attempt {
	//
}

