<?php
namespace App\Models;

/**
 * App\Models\LookupShop
 *
 * @property string $Id
 * @property string|null $ShopCity
 * @property string|null $ShopAddress
 * @property string|null $ShopPhone
 * @property string|null $ShopName
 * @property string|null $WorkingHours
 * @property string|null $Lat
 * @property string|null $Lng
 * @property \Carbon\Carbon $CreatedAt
 * @property \Carbon\Carbon|null $UpdatedAt
 * @property string|null $DeletedAt
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\AbstractTable noLock()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LookupShop whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LookupShop whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LookupShop whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LookupShop whereLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LookupShop whereLng($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LookupShop whereShopAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LookupShop whereShopCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LookupShop whereShopName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LookupShop whereShopPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LookupShop whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LookupShop whereWorkingHours($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LookupShop newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LookupShop newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LookupShop query()
 */
class LookupShop extends Base\LookupShop {
	//
}

