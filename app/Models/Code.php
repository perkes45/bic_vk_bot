<?php
namespace App\Models;

/**
 * App\Models\Code
 *
 * @property int $Id
 * @property string $Value
 * @property int $WinnerId
 * @property \Illuminate\Support\Carbon $CreatedAt
 * @property \Illuminate\Support\Carbon|null $UpdatedAt
 * @property string|null $DeletedAt
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Code newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Code newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\AbstractTable noLock()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Code query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Code whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Code whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Code whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Code whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Code whereValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Code whereWinnerId($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Winner $winner
 */
class Code extends Base\Code {
	//
}

