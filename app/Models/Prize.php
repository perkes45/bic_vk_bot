<?php
namespace App\Models;

/**
 * App\Models\Prize
 *
 * @property string $Id
 * @property string $PrizeName
 * @property string $PrizeType
 * @property string $PrizeCategory
 * @property \Carbon\Carbon $CreatedAt
 * @property \Carbon\Carbon|null $UpdatedAt
 * @property string|null $DeletedAt
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Winner[] $winners
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\AbstractTable noLock()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Prize whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Prize whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Prize whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Prize wherePrizeCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Prize wherePrizeName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Prize wherePrizeType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Prize whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Cheque[] $cheques
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Drawing[] $drawings
 * @property int $RetailerId
 * @property-read \App\Models\Retailer $retailer
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Prize whereRetailerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Prize newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Prize newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Prize query()
 */
class Prize extends Base\Prize {
	//
}

