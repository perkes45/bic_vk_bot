<?php
namespace App\Models;

/**
 * App\Models\Winner
 *
 * @property string $Id
 * @property string $UserId
 * @property string $ChequeId
 * @property string $WinType
 * @property string $PrizeId
 * @property string|null $Status
 * @property \Carbon\Carbon $CreatedAt
 * @property \Carbon\Carbon|null $UpdatedAt
 * @property string|null $DeletedAt
 * @property-read \App\Models\Cheque $cheque
 * @property-read \App\Models\Prize $prize
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\AbstractTable noLock()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Winner whereChequeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Winner whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Winner whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Winner whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Winner wherePrizeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Winner whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Winner whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Winner whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Winner whereWinType($value)
 * @mixin \Eloquent
 * @property int $DrawingId
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Winner whereDrawingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Winner newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Winner newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Winner query()
 * @property string|null $Sert
 * @property string|null $Address
 * @property string|null $FatherName
 * @property string|null $BirthDate
 * @property string|null $PassportInfo
 * @property string|null $PassportWhenIssued
 * @property string|null $PassportIssued
 * @property string|null $WinnerPhone
 * @property string|null $InnNumber
 * @property string|null $PassportMain
 * @property string|null $PassportRegistration
 * @property string|null $PrizeDocument
 * @property string|null $WinnerCheques
 * @property string|null $ProcessDocument
 * @property string|null $InnImage
 * @property string|null $DeliveryStatus
 * @property string|null $ScoreNumber
 * @property string|null $CardNumber
 * @property string|null $Bank
 * @property string|null $BankInn
 * @property string|null $BankKPP
 * @property string|null $BankBik
 * @property string|null $SertImg
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Winner whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Winner whereBank($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Winner whereBankBik($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Winner whereBankInn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Winner whereBankKPP($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Winner whereBirthDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Winner whereCardNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Winner whereDeliveryStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Winner whereFatherName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Winner whereInnImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Winner whereInnNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Winner wherePassportInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Winner wherePassportIssued($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Winner wherePassportMain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Winner wherePassportRegistration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Winner wherePassportWhenIssued($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Winner wherePrizeDocument($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Winner whereProcessDocument($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Winner whereScoreNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Winner whereSert($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Winner whereSertImg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Winner whereWinnerCheques($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Winner whereWinnerPhone($value)
 * @property string|null $Price
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Winner wherePrice($value)
 * @property string|null $YWallet
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Winner whereYWallet($value)
 * @property string|null $WinnerStatus
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Winner whereWinnerStatus($value)
 * @property string|null $PriceWC
 * @method static \Illuminate\Database\Eloquent\Builder|Winner wherePriceWC($value)
 * @property string|null $ProcessedAt
 * @method static \Illuminate\Database\Eloquent\Builder|Winner whereProcessedAt($value)
 * @property string|null $DepositionInfo
 * @method static \Illuminate\Database\Eloquent\Builder|Winner whereDepositionInfo($value)
 */
class Winner extends Base\Winner {
	const TYPE_PROCESSING = 'Processing';
	const TYPE_CONFIRMED = 'Confirmed';


	const DAILY_PRIZE_FIELDS = [
		'YWallet'
	];

	const SPECIAL_PRIZE_FIELDS = [
		"BirthDate",
		'FatherName',
		'WinnerPhone',
		'PassportMain',
		'PassportRegistration',
		'WinnerCheques',
		'YWallet',
		'InnImage',		
		'PrizeDocument'
	];

	public function infoNeeded(){
		if ($this->WinType == 'daily') {
			$fields = self::DAILY_PRIZE_FIELDS;
		} 

		// if ($this->WinType == 'Специальный') {
		// 	$fields = self::SPECIAL_PRIZE_FIELDS;
		// } 

		foreach ($fields as $value) {
			if (!$this->$value){
				return true;
			}
		}

		return false;

	}
}

