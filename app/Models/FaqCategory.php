<?php
namespace App\Models;

/**
 * App\Models\FaqCategory
 *
 * @property int $Id
 * @property string $Name
 * @property \Illuminate\Support\Carbon $CreatedAt
 * @property \Illuminate\Support\Carbon|null $UpdatedAt
 * @property string|null $DeletedAt
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\FaqItem[] $faqItems
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FaqCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FaqCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\AbstractTable noLock()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FaqCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FaqCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FaqCategory whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FaqCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FaqCategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FaqCategory whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class FaqCategory extends Base\FaqCategory {
	//
}

