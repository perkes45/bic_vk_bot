<?php
namespace App\Models;

/**
 * App\Models\Retailer
 *
 * @property int $Id
 * @property string $Name
 * @property \Illuminate\Support\Carbon $CreatedAt
 * @property \Illuminate\Support\Carbon|null $UpdatedAt
 * @property string|null $DeletedAt
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Retailer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Retailer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Base\AbstractTable noLock()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Retailer query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Retailer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Retailer whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Retailer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Retailer whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Retailer whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Retailer extends Base\Retailer {
	//
}

