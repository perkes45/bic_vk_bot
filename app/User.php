<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * App\User
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @mixin \Eloquent
 * @property string $Id
 * @property string $CreatedAt
 * @property string|null $UpdatedAt
 * @property string|null $DeletedAt
 * @property string $GroupId
 * @property string|null $Email
 * @property string|null $Password
 * @property string|null $RememberToken
 * @property bool $Enabled
 * @property string|null $ResetToken
 * @property string|null $ResetTokenCreatedAt
 * @property string|null $LastActionAt
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEnabled($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereLastActionAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereResetToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereResetTokenCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @property string $FirstName
 * @property string $LastName
 * @property string $City
 * @property string $Phone
 * @property string $BirthDate
 * @property bool $RulesAccept
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereBirthDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRulesAccept($value)
 * @property string|null $PassportSerialNumber
 * @property string|null $PassportNumber
 * @property string|null $PassportIssued
 * @property string|null $PassportWhenIssued
 * @property string|null $Inn
 * @property string|null $RegistrationAddress
 * @property string|null $PassportImage
 * @property string|null $InnImage
 * @property string|null $DeliveryAddress
 * @property string|null $DailyChequeId
 * @property string|null $WeeklyChequeId
 * @property string|null $MainChequeId
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereDailyChequeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereDeliveryAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereInn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereInnImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereMainChequeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassportImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassportIssued($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassportNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassportSerialNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassportWhenIssued($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRegistrationAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereWeeklyChequeId($value)
 * @property string|null $RegistratonImage
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRegistratonImage($value)
 * @property string|null $OperatorId
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereOperatorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @property-read int|null $notifications_count
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
