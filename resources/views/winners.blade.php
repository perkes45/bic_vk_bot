@extends('layout')

@section('content')

@push('styles')
<link rel="stylesheet" href="/css/stat1.css">
<style>
header {
    position: relative;
    z-index: 1;
    background: rgba(255, 255, 255, 0.6);
    backdrop-filter: blur(100px);
}

footer {
    position: relative;
    z-index: 1;
}

footer ul li a {
    color: #162342;
}
</style>
@endpush
<div class="static-section">  

    <div class="faq-wrap">
        <div class="row">
            <div class="col-lg-8">
                <h1 class="page-title">Победители</h1> 
                <div class="search-row">
                    <div class="search-wrap">
                        <div  class="search-block">
                            <input type="text" placeholder="Введите e-mail">
                            <button class="js-p-email btn-custom">Найти</button>
                        </div>                
                    </div>
                    
                    <div class="tabs-wrap">
                        <a href="#tab1" class="tab-link js-tab-link active">
                            Ежедневный приз
                        </a>
                        <a href="#tab2" class="tab-link js-tab-link">
                            Еженедельный приз
                        </a>
                    </div>
                </div>
                <div class="w-table-wrap">
                    <div class="js-tab active" id="tab1">
                        <div class="table-responsive">
                            <table class="table-custom table-cabinet ">
                                <thead>
                                    <tr>
                                        <th>имя</th>
                                        <th>e-mail</th>
                                        <th>
                                            <div class="d-none d-sm-block">
                                                дата регистрации чека
                                            </div>
                                            <div class="d-block d-sm-none">
                                                дата*
                                            </div>
                                        </th>
                                        <th>приз</th>
                                    </tr>
                                </thead>
                                <tbody class="js-pagination1">
                                    @if ($daily->count())
                                    @foreach ($daily as $d)
                                    <tr class="js-s" data-search="{{$d->user->Email}}">
                                        <td>{{$d->user->FirstName}}</td>
                                        <td>{{hideEmail($d->user->Email)}}</td>
                                        <td>{{$d->cheque->CreatedAt->format('d.m.Y')}}</td>
                                        <td>
                                            <b>3 000 ₽</b> @if ($d->Status == \App\Models\Winner::TYPE_PROCESSING) (Идет проверка) @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr>
                                        <td></td>
                                        <td colspan="2">Розыгрыши пока не проводились</td>
                                        <td></td>
                                    </tr>
                                    @endif
                                    
                                </tbody>
                            </table>
                        </div>
                        <div class="js-winners-pag1"></div>
                    </div>
                    <div class="js-tab" id="tab2" style="display: none;">
                        <div class="table-responsive">
                            <table class="table-custom table-cabinet ">
                                <thead>
                                    <tr>
                                        <th>имя</th>
                                        <th>e-mail</th>
                                        <th>
                                            <div class="d-none d-sm-block">
                                                дата регистрации чека
                                            </div>
                                            <div class="d-block d-sm-none">
                                                дата*
                                            </div>
                                        </th>
                                        <th>приз</th>
                                    </tr>
                                </thead>
                                <tbody class="js-pagination2">
                                    @if ($weekly->count())
                                    @foreach ($weekly as $w)
                                    <tr class="js-s" data-search="{{$w->user->Email}}">
                                        <td>{{$w->user->FirstName}}</td>
                                        <td>{{hideEmail($w->user->Email)}}</td>
                                        <td>{{$w->cheque->CreatedAt->format('d.m.Y')}}</td>
                                        <td>
                                            <b>{{number_format($w->Price, 0, ' ', ' ')}} ₽ </b> @if ($w->Status == \App\Models\Winner::TYPE_PROCESSING) (Идет проверка) @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr>
                                        <td></td>
                                        <td colspan="2">Розыгрыши пока не проводились</td>
                                        <td></td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <div class="js-winners-pag2"></div>
                    </div>
                    
                </div>
            </div>
            <div class="col-lg-4 d-flex">

            </div>
        </div>
    </div>
</div>   
<div class="fly-form fly-form-empty">
    <div class="p-images-wrap">
        <div class="p-image-item" style="background-image: url(/images/prize11.png);"></div>
        <div class="p-image-item" style="background-image: url(/images/prize21.png);"></div>
    </div>
</div>     


@endsection

