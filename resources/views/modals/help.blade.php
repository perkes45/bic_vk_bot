<div class="modal fade" id="helpModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div>
                <div>
                    <h2 class="modal-title">Где найти номер чека</h2>
                    <div class="close" data-dismiss="modal" aria-label="Close">X</div>
                </div>
                <div>
                    <div class="form-sm">
                        <img src="/images/cheque.jpg" class="w-100" alt="">
                        <div class="static-text mb-5 text-center mt-4">
                            Найти номер вы можете в указанном месте на чеке
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
