<div class="modal fade" id="info" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog  modal-md" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
				<h2 class="modal-title">
					Получение приза
				</h2>
			</div>
			<div class="modal-body">
				<div class="modal-wrapper modal-wrapper-slim">

					
					<div class="modal-form modal-content-block js-preview-wrap">
						<form action="/cabinet/winner/{{$daily->Id}}" method="POST" class="js-form">
							{{csrf_field()}}
							<p>
								Поздравляем! Вы можете получить приз 3000 рублей на ваш электронный кошелек ЮMoney! Для этого введите данные кошелька в течение 5 рабочих дней после розыгрыша.
							</p>
							<div class="form-group custom-row">
								
								<input type="text" class="form-control custom-input" name="YWallet" id="YWallet" value="{{$daily->YWallet}}">
								<label for="YWallet">Номер электронного кошелька ЮМани</label>
							</div>	
							<div class="error"></div>
							<div class="text-center mb-4 mt-4">
								<button class="btn-custom w-100 btn-custom-green">Отправить</button>
							</div>

						</form>
					</div>
				</div>					
			</div>
		</div>
	</div>
</div>
