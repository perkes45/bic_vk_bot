<div class="modal fade" id="info-s" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog  modal-md" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
				<h2 class="modal-title">
					Получение приза
				</h2>
			</div>
			<div class="modal-body">
				<div class="modal-wrapper modal-wrapper-slim">

					
					<div class="modal-form modal-content-block js-preview-wrap">
						<form action="/cabinet/winner/{{$special->Id}}" method="POST" class="js-form">
							{{csrf_field()}}
							<div class="post-title mb-3">Для получения приза вам необходимо заполнить поля</div>	
							<div class="form-group">
								<label for="">ФИО</label>
								<input type="text" class="form-control custom-input" name="FatherName" value="{{$special->FatherName}}">
							</div>	 
							<div class="form-group">
								<label for="">Номер телефона</label>
								<input type="text" class="form-control custom-input js-phone" name="WinnerPhone" value="{{$special->WinnerPhone}}">
							</div>	
							<div class="form-group">
								<label for="">Дата рождения</label>
								<input type="text" class="form-control custom-input" name="BirthDate" value="{{$special->BirthDate}}">
							</div>	
							<div class="form-group">
								<label for="">Номер Яндекс.Кошелька</label>
								<input type="text" class="form-control custom-input" name="YWallet" value="{{$special->YWallet}}">
							</div>	    
							<div class="post-title mb-3">Загрузить фото документов: первой страницы <br> паспорта, страницы с пропиской, инн</div>
							<div class="info-modal-block">
								<div class="add-file-wrap text-right form-group">

									@if (!$special->PassportMain)
									<div class="form-group">
										<div class="add-file-block js-file-wrap">
											<div class="file-placeholder js-file-name">Главная страница паспорта</div>
											<input type="file" placeholder="" class="js-file-input" name="PassportMain">                                     
										</div>
									</div>
									@endif


									@if (!$special->PassportRegistration)
									<div class="form-group">
										<div class="add-file-block js-file-wrap">
											<div class="file-placeholder js-file-name">Страница с регистрацией</div>
											<input type="file" placeholder="" class="js-file-input" name="PassportRegistration">                                     
										</div>
									</div>
									@endif


									@if (!$special->WinnerCheques)
									<div class="form-group">
										<div class="add-file-block js-file-wrap">
											<div class="file-placeholder js-file-name">Скан чека\Архив со сканами</div>
											<input type="file" placeholder="" class="js-file-input" name="WinnerCheques">                                     
										</div>
									</div>
									@endif

									@if (!$special->InnImage)
									<div class="form-group">
										<div class="add-file-block js-file-wrap">
											<div class="file-placeholder js-file-name">Скан ИНН</div>
											<input type="file" placeholder="" class="js-file-input" name="InnImage">                                     
										</div>
									</div>
									@endif


									@if (!$special->PrizeDocument)
									<div class="form-group">
										<div class="add-file-block js-file-wrap">
											<div class="file-placeholder js-file-name">Фото чека с товаром*</div>
											<input type="file" placeholder="" class="js-file-input" name="PrizeDocument">                                     
										</div>
									</div>
									<p style="color: #00a88e;font-size: 12px;font-style: italic;">
										* Фото, на котором запечатлены: выигрышный чек, приобретённый из этого чека смартфон с коробкой от него, другая купленная продукция из чека, 2-3 разворот паспорта покупателя
									</p>
									@endif

								</div>
							</div>
							<div class="post-title text-sm mt-3 mb-3">
								Передача личных данных осуществляется в зашифрованном виде и абсолютно безопасна
							</div>
							<div class="error"></div>
							<div class="modal-footer-block">
								<div class="row justify-content-center">
									<div class="col-sm-4 col-6">					
										<button class="btn-custom btn-custom-green">Отправить</button>
									</div>
								</div>
							</div>

						</form>
					</div>
				</div>					
			</div>
		</div>
	</div>
</div>
