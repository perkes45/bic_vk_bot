<div class="modal fade" id="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div>
                <div class="modal-header pb-0">
                    <!--<h2 class="modal-title">Уведомление</h2>-->
                    <div class="close" data-dismiss="modal" aria-label="Close">X</div>
                </div>
                <div class="modal-body">
                    <div class="modal-form modal-content-block modal-info-block">
                        <div class="row justify-content-center">
                            <div class="col-8">
                                <p class='text-center js-modal-body mb-0'>@if(isset($message)){{$message}} @endif</p>
                            </div>									
                        </div>																
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>