<div class="modal fade" id="feedback" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div>
                <div class="modal-header">
                    <h2 class="modal-title">Обратная связь</h2>
                    <div class="close" data-dismiss="modal" aria-label="Close">X</div>
                </div>
                <div class="modal-body">
                    <form action="/feedback" method="POST" class="js-form" id="feedbackform" data-action='modal' data-target="#static" no-reload="true">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="">Ваш email</label>
                        <input type="email" class="custom-input form-control " name="Email" @if (\Auth::check()) value="{{\Auth::user()->Email}}" @endif>
                    </div>
                    <div class="form-group">
                        <label for="">Ваше имя</label>
                        <input type="text" class="custom-input form-control " name='PersonName' @if (\Auth::check()) value="{{\Auth::user()->FirstName}}" @endif>
                    </div>
                    <div class="form-group">
                        <label for="">Тема сообщения</label>
                        <select class="custom-select" name="Theme">
                            <option value="">Тема</option>
                            <option value="Вопросы о Правилах Акции">Вопросы о Правилах Акции</option>
                            <option value="Не могу зарегистрироваться/ авторизоваться ">Не могу зарегистрироваться/ авторизоваться </option>
                            <option value="Не могу загрузить чек">Не могу загрузить чек</option>
                            <option value="Выдача призов">Выдача призов</option>
                            <option value="Результаты розыгрышей">Результаты розыгрышей</option>
                            <option value="Изменение личных данных ">Изменение личных данных </option>
                        </select>
                    </div>
                    <div class="form-group mb-2">
                        <label for="">Введите своё сообщение</label>
                        <textarea class="custom-input form-control " name="Message"></textarea>
                    </div>
                    <div class="add-file-wrap text-right form-group">
                        <div class="add-file-block js-file-wrap">
                            <div class="file-placeholder js-file-name">Прикрепить файл</div>
                            <input type="file" placeholder="" class="js-file-input" name="Image">                                     
                        </div>
                    </div>
                    <div class="form-footer">
                        <div class="form-group">
                            <div class="captcha-wrap">
                                <div id="recaptcha1"></div>
                            </div>
                        </div>
                        <div class="checkbox-block">
                            <div class="checkbox-wrap">
                                <input type="checkbox" class="form__checkbox" name="ProcessAccept" value="1">
                                <span></span>
                            </div>
                            <div class="checkbox-body">Я согласен на обработку моих данных</div>
                        </div>
                    </div>
                    <div class="error"></div>
                    <div class="text-center">
                        <button class="btn-custom w-100 btn-custom-green">Отправить</button>
                    </div>
                </form>
                    <!-- <form action="/login" method="POST" class="js-form m-auto form-sm" data-location="/cabinet" data-action="location">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="">E-mail</label>
                            <input type="text" class="custom-input form-control text-center" name="Email" required>
                        </div>
                        <div class="form-group">
                            <label for="">Пароль</label>
                            <input type="password" class="custom-input form-control text-center" name="Password" required>
                        </div>

                        <div class="checkbox-block">
                            <div class="checkbox-wrap">
                                <input type="checkbox" class="form__checkbox" name="Remember" value="1">
                                <span></span>
                            </div>
                            <div class="checkbox-body">Запомнить пароль</div>
                        </div>
                        
                        <div class="text-center mb-3">
                            <button class="btn-custom w-100 btn-custom-green">Войти</button>
                        </div>
                        <div class="text-center mb-4">
                            <a href="#" class="btn-custom w-100 btn-custom-green btn-custom-green-invert" data-toggle="modal" data-target="#registrationModal" data-dismiss="modal">Зарегистрироваться</a>
                        </div>
                        <div class="error"></div>

                        <div class="modal-footer">
                            <div class="row">
                                <div class="col-12 text-center">
                                    <a href="#" data-toggle="modal" data-target="#reset" data-dismiss="modal">Забыли пароль?</a>
                                </div>                            
                            </div>
                        </div>
                    </form> -->
                </div>
            </div>
        </div>
    </div>
</div>
