<div class="modal fade" id="code" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div>
                <div>
                    <h2 class="modal-title">Уважаемый участник!</h2>
                    <div class="close" data-dismiss="modal" aria-label="Close">X</div>
                </div>
                <div>
                    <div class="modal-form modal-content-block modal-info-block">
                        <div class="row justify-content-center">
                            <div class="col-12">
                                <div class="static-text mb-3 text-center pl-3 pr-3 mb-5">
                                    Код на подписку Яндекс.Музыка                              
                                </div>
                            </div>
                            <div class="col-8">
                                <p class='text-center text-lg' id="js-set-value"></p>
                            </div>									
                        </div>																
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>