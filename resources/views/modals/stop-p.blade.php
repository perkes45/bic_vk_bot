<div class="modal fade" id="stop-p" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
	<div class="modal-content">
	    <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
	    </div>
	    <div class="modal-body">
		<div class="modal-wrapper pb-0">
		    <div class="row justify-content-center static-title-wrap">
			<h3 class="static-title static-title-nw col-auto">Уведомление</h3>
		    </div>		    
		    <div class="modal-form modal-content-block modal-info-block">
			<div class="row justify-content-center">
			    <div class="col-8">
				<p class='text-center js-modal-body'>
					Период регистрации чеков завершен! <br>
					Сроки вручения призов Победителям до 31 октября 2018 года. <br> Подробности см <a href="/rules.pdf" target="_blank">Правила Акции.</a>
				</p>
			    </div>									
			</div>																
		    </div>
		</div>					
	    </div>
	</div>
    </div>
</div>
