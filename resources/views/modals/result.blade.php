<div class="modal fade" id="results" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div>
                <div class="modal-header">
                    <h2 class="modal-title">Призы предыдущих периодов</h2>
                    <div class="close" data-dismiss="modal" aria-label="Close">X</div>
                </div>
                <div class="modal-body">
                   <table class="table-custom w-100">
                       <thead>
                           <tr>
                               <th style="color: #000;">Период</th>
                               <th style="color: #000;">Стоимость приза</th>
                           </tr>
                       </thead>
                       <tbody>
                           @foreach (\App\Models\Winner::where('WinType', 'weekly')->where('Status', 'Confirmed')->get() as $ww)
                           <tr style="border: none;">
                               <td style="color: #000;border: none;">
                                    {{\Carbon\Carbon::parse($ww->drawing->PeriodStart)->format('d.m.Y')}} - {{\Carbon\Carbon::parse($ww->drawing->PeriodEnd)->format('d.m.Y')}}
                               </td>
                               <td style="color: #000;border: none;">
                                   {{$ww->Price}}
                               </td>
                           </tr>
                           @endforeach
                       </tbody>
                   </table>
                </div>
            </div>
        </div>
    </div>
</div>
