<div class="modal fade" id="registrationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div>
                <div class="modal-header">
                    <h2 class="modal-title">Регистрация</h2>
                    <div class="close" data-dismiss="modal" aria-label="Close">X</div>
                </div>
                <div class="modal-body">
                    <div class="social-row text-center">
                                <div class="">Войти через:</div>
                                <a href="/login/vkontakte" class="">
                                    <i class="fa fa-vk"></i>
                                </a>
                                <a href="/login/odnoklassniki" class="">
                                    <i class="fa fa-odnoklassniki"></i>
                                </a>
                                <a href="/login/facebook" class="">
                                    <i class="fa fa-facebook-f"></i>
                                </a>
                            </div>
                    <form id="registration"  class="js-form form-lg" method="POST" action="/registration" data-action="modal" data-target="#static">
                        {{csrf_field()}}
                        <div class="form-group custom-row">                        
                            <input type="text" class="form-control custom-input {{isset($data['FirstName']) ? 'has-val' : ''}}" name="FirstName" id="FirstName" value="{{$data['LastName'] ?? ''}} {{$data['FirstName'] ?? ''}}">
                            <label for="FirstName">Имя</label>
                        </div>
                        <div class="form-group custom-row">                            
                            <input type="email" class="form-control custom-input {{isset($data['Email']) ? 'has-val' : ''}}" name="Email" id="Email" value="{{$data['Email'] ?? ''}}">
                            <label for="Email">e-mail</label>
                        </div>
                        <p class="disclaimer">
                            Обратите внимание, гарантированный приз будет начислен на номер указанный при регистрации.
                        </p>
                       <div class="form-group custom-row">                            
                            <input type="text" class="form-control custom-input" name="Phone" id="Phone">
                            <label for="Phone">Номер телефона</label>
                        </div>
                        <div class="form-group custom-row">                            
                            <input type="text" class="form-control custom-input" name="PhoneConfirm" id="PhoneConfirm">
                            <label for="PhoneConfirm">Повторите ввод телефона</label>
                        </div>
                        <div class="form-group custom-row">                            
                            <input type="password" class="form-control custom-input" name="Pass" id="Pass">
                            <label for="Pass">Пароль</label>
                            <div class="pass-toggle js-pass-toggle"></div>
                        </div>
                        <div class="form-group custom-row">                            
                            <input type="password" class="form-control custom-input" name="PassConfirm" id="PassConfirm">
                            <label for="PassConfirm">Подтверждение пароля</label>
                        </div>
                        <div class="form-group custom-row">
                            <div class="captcha-wrap">
                                <div id="recaptcha2"></div>
                            </div>
                        </div>
                        <div class="checkbox-block custom-row">
                            <div class="checkbox-wrap">
                                <input type="checkbox" class="form__checkbox" name="RulesAccept" value="1">
                                <span></span>
                            </div>
                            <div class="checkbox-body">Я согласен с <a href="/rules.pdf" target="_blank">Правилами акции</a> и <a href="/PS.pdf" target="_blank">Пользовательским соглашением</a></div>
                        </div>
                        
                        <div class="text-center mb-4 mt-4">
                            <button class="btn-custom w-100 btn-custom-green" onclick="ym(72406741,'reachGoal','button_reg')">Регистрация</button>
                        </div>
                        <div class="text-center mb-4">
                            <a href="#" class="btn-custom w-100 btn-custom-invert" data-toggle="modal" data-target="#loginModal" data-dismiss="modal">У меня уже есть аккаунт</a>
                        </div>
                        <div class="text-center error"></div>                                
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
