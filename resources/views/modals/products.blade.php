<div class="modal fade" id="products" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-md" role="document">
	<div class="modal-content">
	    <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
	    </div>
	    <div class="modal-body">
		<div class="modal-wrapper pb-0">
		    <div class="row justify-content-center static-title-wrap">
			<h3 class="static-title static-title-nw col-md-auto">Продукция</h3>
		    </div>						
		    <div class="modal-form modal-content-block mb-0">
			<div class="modal-slider js-modal-slider owl-carousel">
			    <div class="modal-slide-item">
				<div class="row">
				    <div class="col-sm-5">
					<div class="product-preview h-100" style="background-image: url(images/products/P-UP-A.png);"></div>
				    </div>
				    <div class="col-sm-7">
					<div class="product-title" style="background-image: url('images/logo4.png');"></div>
					<div class="js-scroll scrollbar-inner">
					    <div class="product-desc">
						<p>
						    Профессиональная технология SALONPLEX специально создана для волос,
						    поврежденных при окрашивании, обесцвечивании, выпрямлении и завивке. 
						</p>
						<p>
						    Больше не надо искать компромисс между желанием 
						    экспериментировать и здоровьем волос. Меняйся и наслаждайся восстановленными и 
						    сильными волосами, как после салона, каждый день.
						</p>
					    </div>
					</div>
				    </div>
				</div>
			    </div>
			    <div class="modal-slide-item">
				<div class="row">
				    <div class="col-sm-5">
					<div class="product-preview h-100" style="background-image: url(images/products/P-UP-B.png);"></div>
				    </div>
				    <div class="col-sm-7">
					<div class="product-title" style="background-image: url('images/logo4.png');"></div>
					<div class="js-scroll scrollbar-inner">
					    <div class="product-desc">
						<p>
						    SYOSS c технологией FULL HAIR 5, разработан специально для тонких и 
						    лишенных объема волос. Это профессиональный уход, направленный на улучшение пяти
						    показателей здоровья волос: средства делают волосы более густыми, придают им объем, 
						    силу, сокращают выпадение волосков, вызванное их ломкостью, а также стимулируют 
						    работу волосяных луковиц.  
						</p>
						<p>
						    Каждый день волосы будут выглядеть такими же густыми и роскошными, как после посещения салона.
						</p>
					    </div>
					</div>
				    </div>
				</div>
			    </div>
			    <div class="modal-slide-item">
				<div class="row">
				    <div class="col-sm-5">
					<div class="product-preview h-100" style="background-image: url(images/products/P-UP-C.png);"></div>
				    </div>
				    <div class="col-sm-7">
					<div class="product-title" style="background-image: url('images/logo4.png');"></div>
					<div class="js-scroll scrollbar-inner">
					    <div class="product-desc">
						<p>
						    SYOSS представляет коллекцию профессиональных стайлинговых средств FIBER FLEX
						    с инновационной технологией тончайшего распыления, которая позволяет добиться 
						    эффекта «укладки без укладки».  
						</p>
						<p>
						    SYOSS FIBER FLEX не склеивает волосы и при этом гарантирует 
						    экстрасильную фиксацию и естественную подвижность волос на 48 часов без
						    утяжеления. Средства SYOSS FIBER FLEX не оставляют следов и легко удаляются при
						    расчёсывании, а также позволяют защитить волосы от воздействия влажности, УФ-лучей,
						    а мусс SYOSS FIBER FLEX помогает защитить волосы при укладке феном.
						</p>
					    </div>
					</div>
				    </div>
				</div>
			    </div>
			    <div class="modal-slide-item">
				<div class="row">
				    <div class="col-sm-5">
					<div class="product-preview h-100" style="background-image: url(images/products/P-UP-D.png);"></div>
				    </div>
				    <div class="col-sm-7">
					<div class="product-title" style="background-image: url('images/logo1.png');"></div>
					<div class="js-scroll scrollbar-inner">
					    <div class="product-desc">
						<p>
						    Schauma Энергия Природы наполнен экстрактами 100% 
						    природного происхождения для естественно красивых волос  от
						    корней  до кончиков. Формула со свежим ароматом тщательно очищает
						    и освежает, делает волосы мягкими и гладкими. 
						</p>
						<p>
						    Без силиконов. Без искусственных красителей
						</p>
					    </div>
					</div>
				    </div>
				</div>
			    </div>
			    <div class="modal-slide-item">
				<div class="row">
				    <div class="col-sm-5">
					<div class="product-preview h-100" style="background-image: url(images/products/P-UP-E.png);"></div>
				    </div>
				    <div class="col-sm-7">
					<div class="product-title" style="background-image: url('images/logo1.png');"></div>
					<div class="js-scroll scrollbar-inner">
					    <div class="product-desc">
						<p>
						    Моментальный результат и мегафиксация от TAFT <br>
						    Новый Taft Power Экспресс-укладка обеспечивает моментальный
						    результат и мегафиксацию без компромиссов – при сушке волос феном и для придания финального штриха укладке.
						</p>
						<p>
						    Лак для волос Формула Taft моментально обеспечивает сухую фиксацию укладки благодаря
						    технологии сверхтонкого распыления и экспресс-высыхания -  для совершенного образа при любых условиях! <br>
						    Спрей для волос: Активатор быстрой укладки сокращает
						    время укладки феном и помогает воплотить желаемый образ за считанные минуты.

						</p>
					    </div>
					</div>
				    </div>
				</div>
			    </div>
			    <div class="modal-slide-item">
				<div class="row">
				    <div class="col-sm-5">
					<div class="product-preview h-100" style="background-image: url(images/products/P-UP-F.png);"></div>
				    </div>
				    <div class="col-sm-7">
					<div class="product-title" style="background-image: url('images/logo1.png');"></div>
					<div class="js-scroll scrollbar-inner">
					    <div class="product-desc">
						<p>
						    Революционная формула GLISS KUR с технологией OMEGAPLEX <br>
						    Из-за частого окрашивания и укладки волос феном и щипцами для завивки или выпрямления структурные связи
						    между волокнами волоса разрушаются, и они теряют здоровый вид. 
						</p>
						<p>
						    Революционная формула GLISS KUR с технологией OMEGAPLEX проникает
						    внутрь волоска и восстанавливает разорванные структурные связи. <br>
						    Результат – видимое улучшение качества после каждого применения и защита от повреждений.
						</p>
					    </div>
					</div>
				    </div>
				</div>
			    </div>
			    <div class="modal-slide-item">
				<div class="row">
				    <div class="col-sm-5">
					<div class="product-preview h-100" style="background-image: url(images/products/P-UP-G.png);"></div>
				    </div>
				    <div class="col-sm-7">
					<div class="product-title" style="background-image: url('images/logo1.png');"></div>
					<div class="js-scroll scrollbar-inner">
					    <div class="product-desc">
						<p>
						    GLISS KUR ЖИДКИЙ ШЕЛК, серия средств по уходу за ломкими,
						    лишенными блеска волосами, содержит специальный набор активных
						    ингредиентов с компонентами нутри-шелка в жидком виде, 
						    придающих им дополнительный блеск. Ваши волосы приобретают 
						    ослепительный блеск и гладкость, как шелк.
						</p>
					    </div>
					</div>
				    </div>
				</div>
			    </div>
			    <div class="modal-slide-item">
				<div class="row">
				    <div class="col-sm-5">
					<div class="product-preview h-100" style="background-image: url(images/products/P-UP-H.png);"></div>
				    </div>
				    <div class="col-sm-7">
					<div class="product-title" style="background-image: url('images/logo3.png');"></div>
					<div class="js-scroll scrollbar-inner">
					    <div class="product-desc">
						<p>
						    Длительная защита от линейки Fа Защита и Комфорт<br>
						    Fа антиперспирант-спрей Защита&Комфорт <br>
						    -	Длительная защита против пота и запаха 48 часов<br>
						    -	 Бережная защита: новая формула с технологией сухих масел регулирует потоотделение, защищает от пересушивания - для ощущения мягкости кожи<br>
						    -	 Свежий и тонкий аромат <br>
						    -	 0% спирта<br>
						    -	 Против пятен<br>
						</p>
						<p>
						    Fa антиперспирант-ролик  <br>
						    -	Эмульсионная формула обеспечивает экстра эффективное сокращение потоотделения<br>
						    -	Быстросохнущая технология обеспечивает экстра комфортное нанесение <br>
						    -	Увеличенная концентрация аромата обеспечивает экстра свежую защиту от запаха<br>
						</p>
					    </div>
					</div>
				    </div>
				</div>
			    </div>
			    <div class="modal-slide-item">
				<div class="row">
				    <div class="col-sm-5">
					<div class="product-preview h-100" style="background-image: url(images/products/P-UP-I.png);"></div>
				    </div>
				    <div class="col-sm-7">
					<div class="product-title" style="background-image: url('images/logo3.png');"></div>
					<div class="js-scroll scrollbar-inner">
					    <div class="product-desc">
						<p>
						    Просто побалуйте себя – гели для душа Fa щедро 
						    одарят Вас заботой в сочетании со свежими ароматами. 
						    Гели подойдут как для расслабления, так и для освежения. 
						    Ваша кожа станет шёлковой и нежной – идеальное сочетание 
						    заботы и свежести. FA - настоящий подарок для Вашего тела и чувств.
						</p>
					    </div>
					</div>
				    </div>
				</div>
			    </div>
			    <div class="modal-slide-item">
				<div class="row">
				    <div class="col-sm-5">
					<div class="product-preview h-100" style="background-image: url(images/products/P-UP-J.png);"></div>
				    </div>
				    <div class="col-sm-7">
					<div class="product-title" style="background-image: url('images/logo2.png');"></div>
					<div class="js-scroll scrollbar-inner">
					    <div class="product-desc">
						<p>
						    Продукты линии DIADEMINE Основная программа разработаны для очищения и ухода за кожей любого типа.
						    Кожа – самый большой и самый чувствительный орган человека. Для ее защиты и укрепления необходим ежедневный уход. 
						</p>
						<p>
						    Благодаря очищению поверхность становится свежей, мягкой и гладкой. 
						    Инновационные продукты обеспечивают ее длительное увлажнение, мягкость и естественное сияние. 
						</p>
					    </div>
					</div>
				    </div>
				</div>
			    </div>
			    <div class="modal-slide-item">
				<div class="row">
				    <div class="col-sm-5">
					<div class="product-preview h-100" style="background-image: url(images/products/P-UP-K.png);"></div>
				    </div>
				    <div class="col-sm-7">
					<div class="product-title" style="background-image: url('images/logo2.png');"></div>
					<div class="js-scroll scrollbar-inner">
					    <div class="product-desc">
						<p>
						    Diademine №110 - 110 капель мощного Эликсира молодости для Вашей красоты. 
						    <br>
						    Основываясь на своем 110-летнем опыте, бренд Diademine 
						    представляет антивозрастную технологию мульти-действия, 
						    которая заключена в 110 каплях концентрированного коктейля из самых эффективных антивозрастных компонентов.
						</p>
						<p>
						    Формула линейки продуктов №110 активизирует изнутри 11 признаков молодой кожи.<br>
						    Для более молодой и сияющей кожи
						</p>
					    </div>
					</div>
				    </div>
				</div>
			    </div>
			    <div class="modal-slide-item">
				<div class="row">
				    <div class="col-sm-5">
					<div class="product-preview h-100" style="background-image: url(images/products/P-UP-L.png);"></div>
				    </div>
				    <div class="col-sm-7">
					<div class="product-title" style="background-image: url('images/logo5.png');"></div>
					<div class="js-scroll scrollbar-inner">
					    <div class="product-desc">
						<p>
						    Увеличить объем, придать волосам естественный блеск 
						    поможет серия Shamtu, которая давно стала узнаваемой и 
						    любимой во всем мире. Натуральные экстракты, специальные 
						    составы создают эффект пышных и густых волос. Даже ломкие, 
						    тонкие и секущиеся волосы могут выглядеть шикарно, если вы будете ухаживать за ними при помощи продукции от Shamtu.  
						</p>
					    </div>
					</div>
				    </div>
				</div>
			    </div>
			</div>								
		    </div>
		</div>					
	    </div>
	</div>
    </div>
</div>
