<div class="modal fade" id="winner" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
	<div class="modal-content">
	    <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
	    </div>
	    <div class="modal-body">
		<div class="modal-wrapper pb-0">
		    <div class="row justify-content-center static-title-wrap">
			<h3 class="static-title static-title-nw col-md-auto">Уведомление</h3>
		    </div>		    
		    <div class="modal-form modal-content-block modal-info-block">
			<p class='text-center'>
			    Поздравляем! <br>
			    Вы стали обладателем еженедельного / главного приза.
			</p>
			<p class="text-center">
			    Пожалуйста, заполните данные для <br> получения приза.
			</p>
			<div class="text-center">
			    <a href="" class="btn-custom btn-custom-sm" data-dismiss="modal" aria-label="Close" data-toggle="modal" data-target="#info">Заполнить форму</a>
			</div>
		    </div>
		</div>					
	    </div>
	</div>
    </div>
</div>
