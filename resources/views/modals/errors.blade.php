<div class="modal fade" id="errors" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
	<div class="modal-content">
	    <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
	    </div>
	    <div class="modal-body">
		<div class="modal-wrapper pb-0">
		    <div class="row justify-content-center static-title-wrap">
			<h3 class="static-title static-title-nw col-auto">Ошибка</h3>
		    </div>		    
		    <div class="modal-form modal-content-block modal-info-block">
			<div class="row justify-content-center">
			    <div class="col-8">
				<p class='text-center js-modal-body'>@if(isset($message)){{$message}} @endif</p>
			    </div>									
			</div>																
		    </div>
		</div>					
	    </div>
	</div>
    </div>
</div>
