<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div>
                <div class="modal-header">
                    <h2 class="modal-title">Вход</h2>
                    <div class="close" data-dismiss="modal" aria-label="Close">X</div>
                </div>
                <div class="modal-body">
                    <div class="social-row text-center">
                                <div class="">Войти через:</div>
                                <a href="/login/vkontakte" class="">
                                    <i class="fa fa-vk"></i>
                                </a>
                                <a href="/login/odnoklassniki" class="">
                                    <i class="fa fa-odnoklassniki"></i>
                                </a>
                                <a href="/login/facebook" class="">
                                    <i class="fa fa-facebook-f"></i>
                                </a>
                            </div>
                    <form id="loginform" action="/login" method="POST" class="js-form m-auto form-sm" data-location="/cabinet" data-action="location">
                        {{csrf_field()}}
                        <div class="form-group custom-row">
                            
                            <input type="text" class="custom-input form-control " name="Email" required>
                            <label for="">E-mail</label>
                        </div>
                        <div class="form-group custom-row">
                            
                            <input type="password" class="custom-input form-control " name="Password" required>
                            <label for="">Пароль</label>
                        </div>
                        <div class="form-group custom-row">
                            <div class="captcha-wrap">
                                <div id="recaptcha3"></div>
                            </div>
                        </div>
                        <div class="checkbox-block">
                            <div class="checkbox-wrap">
                                <input type="checkbox" class="form__checkbox" name="Remember" value="1">
                                <span></span>
                            </div>
                            <div class="checkbox-body">Запомнить пароль</div>
                        </div>
                        <div class="error mb-sm-3 mb-5"></div>
                        <div class="text-center mb-sm-3 mb-5">
                            <button class="btn-custom w-100 btn-custom-green">Войти</button>
                        </div>
                        
                        

                        <div class="modal-footer mb-sm-3 mb-5">
                            <div class="row">
                                <div class="col-12 text-center">
                                    <a href="#" data-toggle="modal" data-target="#reset" data-dismiss="modal">Забыли пароль?</a>
                                </div>                            
                            </div>
                        </div>
                        <div class="text-center mb-3">
                            <a class="btn-custom w-100 btn-custom-invert" data-dismiss="modal" data-toggle="modal" data-target="#registrationModal">Зарегистрироваться</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
