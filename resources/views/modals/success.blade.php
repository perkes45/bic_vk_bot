<div class="modal fade" id="success" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
	<div class="modal-content">
	    <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
	    </div>
	    <div class="modal-body">
		<div class="modal-wrapper pb-0">
		    <div class="row justify-content-center static-title-wrap">
			<h3 class="static-title col-auto">Уведомление <br> об отправке письма</h3>
		    </div>
		    <div class="post-title">На указанную почту отправлено письмо для входа в личный кабинет с логином и паролем</div>
		    <div class="modal-form modal-content-block">
			<div class="row justify-content-center">
			    <div class="col-12 col-sm-8">
				<div class="input-placeholder">
				    E-mail : <span class="js-email">xxxxxx@xxxxxx.xx</span>
				</div>
			    </div>									
			</div>								
			<div class="modal-footer-block">
			    <div class="modal-link text-center">
				<a href="" data-dismiss="modal" aria-label="Close" data-toggle="modal" data-target="#reset">Отправить письмо повторно</a>
			    </div>
			</div>								
		    </div>
		</div>					
	    </div>
	</div>
    </div>
</div>
