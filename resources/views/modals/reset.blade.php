<div class="modal fade" id="reset" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div>
                <div class="modal-header">
                    <h2 class="modal-title">Восстановление пароля</h2>
                    <div class="close" data-dismiss="modal" aria-label="Close">X</div>
                </div>
                <div class="modal-body">
                    <p>
                        Введите email, указанный вами  <br>
                        при регистрации
                    </p>
                    <form action="/restore" method="POST" class="js-form m-auto" id="reset-form" data-action="modal" data-target="#static">
                        {{csrf_field()}}
                        <div class="form-group custom-row">
                            <input type="text" class="custom-input form-control" name="Email" required >
                            <label for="">
                                e-mail
                            </label>
                        </div>                                               
                        
                        
                        <div class="form-group ">
                            <button class="btn-custom btn-custom-green w-100">Восстановить</button>
                        </div>
                        <div class="error"></div>
                    </form>                    
                </div>
            </div>
        </div>
    </div>
</div>