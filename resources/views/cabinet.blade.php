@extends('layout')

@section('content')

@push('styles')
<link rel="stylesheet" href="/css/stat1.css">
<style>
header {
    position: relative;
    z-index: 2;
    background: rgba(255, 255, 255, 0.6);
    backdrop-filter: blur(100px);
}

footer {
    position: relative;
    z-index: 2;
}

footer ul li a {
    color: #fff;
}
.copyright {
    color: #162342;
}
@media (max-width: 992px) {
    header {
        background-color: #fff !important;
    }
    footer ul li a {
    color: #162342;
}
}
@media (max-width: 768px){
    .header-l .header-timer-wrap {
        color: #162346;
        background-image: url(/images/timerb.png);
    }

}


</style>
@endpush
<div class="fly-form fly-form-l">
    <h1 class="page-title">Личный кабинет</h1> 
    <div class="personal-info-wrap">
        <div class="p-info-row">
            <div>Имя</div>
            <span>{{$user->FirstName}}</span>
        </div>
        <div class="p-info-row">
            <div>Email</div>
            <span>{{$user->Email}}</span>
        </div>
        <div class="p-info-row">
            <div>Телефон</div>
            <span>{{$user->Phone}}</span>
        </div>
        <div class="p-info-row mb-0">
            <a href="/logout">Выход</a>
        </div>
    </div>
    <div class="p-images-wrap">
        @if (!$user->winners()->where('WinType', 'daily')->count())
        <div class="p-image-item" style="background-image: url(/images/prize11.png);"></div>
        @else
        <div class="p-image-item" style="background-image: url(/images/prize3.png);"></div>
        @endif
        <div class="p-image-item" style="background-image: url(/images/prize21.png);"></div>
    </div>
</div>  
<div class="static-section">  

    <div class="">
        <div class="row">
            <div class="col-lg-4 d-flex">

            </div>
            <div class="col-lg-8">
                <div class="personal-wrap">
                    <h1 class="page-title">Мои чеки</h1> 
                    <a href="" class="btn-custom js-apm-show" onclick="ym(72406741,'reachGoal','reg_chek_2')">Зарегистрировать чек</a>
                    <div class="p-table-wrap">
                        @if (count($user->cheques))
                        <div class="table-responsive">
                            <table class="table-custom table-cabinet ">
                                <thead>
                                    <tr>
                                        <th>
                                            ID
                                        </th>
                                        <th>
                                            Чек
                                        </th>
                                        <th>
                                            <div class="d-none d-sm-block">
                                                Дата загрузки
                                            </div>
                                            <div class="d-block d-sm-none">
                                                Дата 
                                            </div>
                                        </th>
                                        <th>
                                            Статус
                                        </th>
                                        <th class="d-none d-sm-table-cell">
                                            Причина отклонения
                                        </th>   
                                        <th class="d-table-cell d-sm-none">
                                            
                                        </th>                            
                                    </tr>
                                </thead>
                                <tbody class="js-pagination">
                                    @if ($user->cheques->count())
                                    @foreach($user->cheques as $i => $cheque)
                                    <tr class="@if ($cheque->RejectReason) error-l @endif">
                                        <td>
                                            {{$cheque->ReceiptId}}    
                                                                                  
                                        </td>
                                        <td>
                                            @foreach ($cheque->GetImages() ?? [] ?? [] as $img)
                                            <a href="{{$img}}" target="_blank" class="cheque-link"></a>                                            
                                            @endforeach
                                        </td>
                                        <td>
                                            {{$cheque->CreatedAt->format('d.m.Y')}}                                           
                                        </td>
                                        <td>
                                            @if ($cheque->Status == 'REVIEWED')
                                                        @if ($cheque->RejectReason)
                                                        <img src="/images/rej.png" class="state-img" alt="">
                                                        @else
                                                        <img src="/images/app.png" class="state-img" alt="">
                                                        @endif
                                                        @else В обработке @endif    

                                            <div class="d-block d-sm-none mt-2">
                                                @if ($cheque->Status == 'REVIEWED')
                                                        @if ($cheque->RejectReason)
                                                        {{$cheque->RejectReason}}
                                                        @else
                                                        принят
                                                        @endif
                                                @endif  
                                            </div>  

                                            @if ($cheque->winners()->where('WinType', 'garant')->count())
                                            <div class="mt-2" data-toggle="modal" data-target="#fft">Приз 50 руб</div>
                                            @endif                                
                                        </td>
                                        <td class="d-none d-sm-table-cell">
                                            @if ($cheque->RejectReason)
                                            {{$cheque->RejectReason}}
                                            @endif
                                        </td>
                                        <td class="d-table-cell d-sm-none">
                                            
                                        </td> 
                                        
                                    </tr>
                                    @endforeach        
                                    @else
                                    <tr>
                                        <td colspan="">
                                            Вы пока не загрузили ни одного чека.
                                        </td>
                                    </tr>
                                    @endif               
                                </tbody>
                            </table>
                        </div>
                        @else
                        <div class="emp-c">У вас еще нет загруженных чеков</div>
                        @endif
                         <div class="js-winners-pag"></div>
                    </div>
                </div>
                
                
            </div>
            
        </div>
    </div>
</div>   

@endsection




@push('modals')
@include('modals.fft')
@endpush




@if ($showDaily)

@push('modals')
@include('modals.info')
@endpush

@push('scripts')
<script>
    $(function(){
        $('#info').modal('show')
    })
</script>
@endpush

@endif


@push('modals')
@include('modals.help')
@include('modals.help1')
@include('modals.code')
@endpush

