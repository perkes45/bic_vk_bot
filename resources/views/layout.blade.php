<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>EPICA «Больше хочется - больше сбудется!»</title>
    <meta name="description" content="Покупайте не менее 3-х йогуртов EPICA, регистрируйте чеки и увеличивайте призовой фонд на 500р с каждой покупкой, выигрывайте до 1 000 000 каждую неделю!">
    <meta name="token" content="{{csrf_token()}}">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/fonts.css">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link href="https://qr.apmcheck.ru/css/widget.min.css" rel="stylesheet"/>

    <link rel="stylesheet" href="/css/index2.css">
    <link rel="stylesheet" href="/css/apm.css">

    @stack('styles')
        <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
         (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
         m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
         (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

         ym(72406741, "init", {
              clickmap:true,
              trackLinks:true,
              accurateTrackBounce:true,
              webvisor:true
         });
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/72406741" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->

     <script async src="https://www.googletagmanager.com/gtag/js?id=UA-189742869-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-189742869-1');
    </script>

    <!-- Google Tag Manager -->

    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':

    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],

    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=

    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);

    })(window,document,'script','dataLayer','GTM-KB6FZGS');</script>

    <!-- End Google Tag Manager -->

    <script type="text/javascript">!function(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,t.src="https://vk.com/js/api/openapi.js?168",t.onload=function(){VK.Retargeting.Init("VK-RTRG-673320-cerhZ"),VK.Retargeting.Hit()},document.head.appendChild(t)}();</script><noscript><img src="https://vk.com/rtrg?p=VK-RTRG-673320-cerhZ" style="position:fixed; left:-999px;" alt=""/></noscript>

    <!-- Facebook Pixel Code -->
    <script>
      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '210625297383023');
      fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=210625297383023&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->
    
</head> 
<body class="">
  <!-- Google Tag Manager (noscript) -->

<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KB6FZGS"

height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<!-- End Google Tag Manager (noscript) -->
    @if (Auth::check() && (Auth::user()->CanLoadCheques()))
    <div id="revizor"></div>
    @endif
    <main>
        <header class="trs">
            <div class="header-l">
                <a href="/" class="logo-link"></a>
                <div class="header-timer-wrap">
                    <p>
                        до следующего розыгрыша:
                    </p>
                    <div class="header-timer">
                        <div>
                            <div id="days"></div>
                            <span>Дней</span>
                        </div>
                        <div>
                            <div id="hours"></div>
                            <span>Часов</span>
                        </div>
                        <div>
                            <div id="minutes"></div>
                            <span>Минут</span>
                        </div>
                        <div>
                            <div id="seconds"></div>
                            <span>Секунд</span>
                        </div>
                    </div>
                </div>
                <div class="header-score">
                    <p>Еженедельный приз</p>
                    @if ($activeDrawing ?? false)
                    <div>{{number_format($activeDrawing->getScore(), 0, ' ', ' ')}} ₽</div>
                    @else
                    <div></div>
                    @endif
                    <a href="" data-toggle="modal" data-target="#results" class="link-u">
                        Призы предыдущих периодов
                    </a>
                </div>
            </div>
            <div class="header-r">

                @if (\Auth::user())
                <a href="" class="btn-custom d-none d-md-inline-block js-apm-show" onclick="ym(72406741,'reachGoal','reg_chek')">Зарегистрировать чек</a>
                <a href="/cabinet" class="btn-custom btn-custom-icon" style="background-image: url(/images/user.png);"></a>
                @else
                <a href="" class="btn-custom d-none d-md-inline-block" data-toggle="modal" data-target="#registrationModal" onclick="ym(72406741,'reachGoal','reg_chek')">Зарегистрировать чек</a>
                <a href="" class="btn-custom btn-custom-icon" data-toggle="modal" data-target="#loginModal" style="background-image: url(/images/user.png);"></a>
                @endif
                <span class="btn-custom js-menu-toggler">
                    Меню
                    
                </span>
            </div>
            <span class="main-menu">
                <span class="menu-close js-menu-toggler"></span>
                <ul>
                    <li>
                        <a href="/">На главную</a>
                    </li>
                   
                    <li>
                        <a href="/winners">Победители</a>
                    </li>
                    <li>
                        <a href="/feedback">Вопрос-ответ</a>
                    </li>
                    <li>
                        <a href="/rules.pdf" target="_blank">Правила акции</a>
                    </li>
                    @if (\Auth::check())
                    <li>
                        <a href="/cabinet" target="" class="menu-info">Личный кабинет</a>
                    </li>
                    
                    @endif
                </ul>
                @if (!\Auth::check())
                <span class="menu-btns">
                    <a href="" class="btn-custom btn-log" data-toggle="modal" data-target="#loginModal">Вход</a>
                    <a href="" class="btn-custom btn-reg" data-toggle="modal" data-target="#registrationModal">Регистрация</a>
                </span>
                @endif
                <div class="header-score d-block d-md-none">
                    <p>Еженедельный приз</p>
                    @if ($activeDrawing ?? false)
                    <div>{{number_format($activeDrawing->getScore(), 0, ' ', ' ')}} ₽</div>
                    @else
                    <div></div>
                    @endif
                    <a href="" data-toggle="modal" data-target="#results" class="link-u">
                        Призы предыдущих периодов
                    </a>
                </div>
                <div class="header-timer-wrap">
                    <p>
                        до следующего розыгрыша:
                    </p>
                    <div class="header-timer">
                        <div>
                            <div id="days1"></div>
                            <span>Дней</span>
                        </div>
                        <div>
                            <div id="hours1"></div>
                            <span>Часов</span>
                        </div>
                        <div>
                            <div id="minutes1"></div>
                            <span>Минут</span>
                        </div>
                        <div>
                            <div id="seconds1"></div>
                            <span>Секунд</span>
                        </div>
                    </div>
                </div>
            </span>
        </header>
        <section class="page-render">
            @yield('content')
        </section>
        <footer>
            <div class="copyright">
               EPICA © Все права защищены
           </div>
           <ul>
            <li>
                <a href="/PS.pdf" target="_blank">Пользовательское соглашение</a>
            </li>
            <li>
                <a href="/rules.pdf" target="_blank">Правила акции</a>
            </li>
        </ul>
    </footer>
</main>
</body>
</html>


@include('modals.static')
@include('modals.errors')
@include('modals.result')

@stack('modals')

@if (Auth::check())
@include('modals.upload')
@else
@include('modals.registration')
@include('modals.reset')
@include('modals.login')
@endif





<script src="/js/jquery.js"></script>
<script src="/js/owl.carousel.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<!-- <script src="/js/scroll.js"></script>  -->
<script src="/js/paginathing.js"></script>
<script src="/js/jquery.maskedinput.js"></script>


<script src="/js/jquery.paginate.js"></script>
<script src="/js/index1.js"></script> 

<script src='https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit'></script>
<script>
    var recaptcha1;
    var onloadCallback = function () {
        if (document.getElementById('recaptcha1')) {
            recaptcha1 = grecaptcha.render('recaptcha1', {
                'sitekey': "{{env('RECAPTCHA_PUBLIC')}}",
            });
        }

        if (document.getElementById('recaptcha2')) {
            recaptcha2 = grecaptcha.render('recaptcha2', {
                'sitekey': "{{env('RECAPTCHA_PUBLIC')}}",
            });
        }

        if (document.getElementById('recaptcha3')) {
            recaptcha3 = grecaptcha.render('recaptcha3', {
                'sitekey': "{{env('RECAPTCHA_PUBLIC')}}",
            });
        }

    };
</script>

<script type="text/javascript">
    var set = true;
    $(function ($) {
        $("[name=Phone]").mask("+7 999 9999999");
        $("[name=PhoneConfirm]").mask("+7 999 9999999");
    });
    $(function(){
        $('.js-render-map').click(function(){
            if (set) {
                ymaps.ready(init)
                set = false;
            } 
        });
    })
</script>

@if (\Auth::check())

<script src="https://qr.apmcheck.ru/js/widget.min.js"></script>
<script type="text/javascript">
    const widgetParams = {
        api: '{{env("APM_URL")}}',
        apiKey: '{{env("APM_KEY")}}',
        userUuid: '{{\Auth::user()->Uid}}',
        i18n: {
         labels: {
             mainButton: 'сканировать qr-код чека',
             manualQrButton: 'ввести данные вручную',
             uploadPhotosButton: 'загрузить фото чека',
             submitManualQrButton: 'отправить',
             addPhotoButton: 'добавить фото',
             submitPhotosButton: 'отправить'
         },
         screens: {
             scanQr: {
                 header: 'Сканирование',
                 subheader: 'Наведите камеру на QR-код'
             },
             manualInput: {
                 header: 'Ручной ввод',
                 subheader: 'Введите данные с чека'
             },
             cameraNotAvailable: {
                 subheader: 'Мы не можем получить доступ к камере устройства.<br>Разрешите браузеру обращаться к камере или введите данные с чека вручную'
             },
             qrParsed: {
                header: 'Спасибо за регистрацию чека!',
                 subheader: 'Мы увеличим призовой фонд на 500 Р, как только чек пройдёт проверку. Модерация занимает от нескольких минут до 3-х дней.'
             },
             uploadFiles: {
                 header: 'Загрузка фото',
                 subheader: 'Добавьте фото чека (до 5 частей)',
                 fileTooLargeError: 'Файл больше 10 МБ. Загрузите файл меньшего размера.'
             },
             sentReceiptFailed: {
                 header: 'Не удалось отправить чек!'
             }
         }
     },
     callbacks: {
       onReceiptSentSuccess: function (res) {
                   // $('#upload').modal('hide');
                   // $('.js-modal-body').html('Чек успешно загружен');
                   // $('#static').modal('show');
		try {
			ym(72406741,'reachGoal','reg_success');
		} catch (e) {
			console.log(e);
		}
                   $('.apm-p').text('Спасибо за регистрацию чека! Мы увеличим призовой фонд на 500 Р, как только чек пройдёт проверку. Модерация занимает от нескольких минут до 3-х дней.')
                   setTimeout(function () {
                    document.location.reload(); 
                }, 5000);
               },
               onReceiptSentError: function (res) {
                   $('#upload').modal('hide');
                   $('.js-modal-body').html('Произошла ошибка при отправке чека - '+res);
                   $('#static').modal('show');

               }
           }
       };
       qrWidget.init('revizor', widgetParams);
   </script>
   @endif

   

   <script>

    @if ($activeDrawing ?? false)
    const second = 1000,
    minute = second * 60,
    hour = minute * 60,
    day = hour * 24;

    let start = "{{\Carbon\Carbon::parse($activeDrawing->FireAt)->format('D M d Y H:i:s')}}";
    countDown = new Date(start).getTime(),
    console.log(countDown);
    console.log(start);
    x = setInterval(function() {    

        let now = new Date().getTime(),
        distance = countDown - now;

        var daysL = Math.floor(distance / (day));
        var hoursL = Math.floor((distance % (day)) / (hour));
        var minutesL = Math.floor((distance % (hour)) / (minute));
        var secondsL = Math.floor((distance % (minute)) / second);

        if (daysL < 10) {
            daysL = '0' + daysL;
        }

        if (hoursL < 10) {
            hoursL = '0' + hoursL;
        }

        if (minutesL < 10) {
            minutesL = '0' + minutesL;
        }

        if (secondsL < 10) {
            secondsL = '0' + secondsL;
        }

        document.getElementById("days").innerText = daysL,
        document.getElementById("days1").innerText = daysL,
        document.getElementById("hours").innerText = hoursL,
        document.getElementById("hours1").innerText = hoursL,
        document.getElementById("minutes").innerText = minutesL,
        document.getElementById("minutes1").innerText = minutesL,
        document.getElementById("seconds").innerText = secondsL;
        document.getElementById("seconds1").innerText = secondsL;

        //do something later when date is reached
        if (distance < 0) {


          clearInterval(x);
      }
        //seconds
    }, 0)

    @else
    document.getElementById("days").innerText = '00',
        document.getElementById("days1").innerText = '00',
        document.getElementById("hours").innerText = '00',
        document.getElementById("hours1").innerText = '00',
        document.getElementById("minutes").innerText = '00',
        document.getElementById("minutes1").innerText = '00',
        document.getElementById("seconds").innerText = '00';
        document.getElementById("seconds1").innerText = '00';
    @endif
</script>

@if (isset($autoOpen) && $autoOpen)
    <script>
        $(function () {
            $('#registrationModal').modal('show');
        })
    </script>
    @endif

@stack('scripts')
</html>
