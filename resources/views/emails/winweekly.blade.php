<!-- Новое Email сообщение -->
        <html>
            <head>
                
            </head>
            <body>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td align="center">
            <table style="width: 640px; margin: 0 auto; font-size: 0; line-height: 1; font-family: Verdana, Arial;">
                    <tbody>
                        <tr>
                            <td>
                                 <a href="{{env('APP_URL')}}">
                                    <img src="{{env('APP_URL').'/images/et.png'}}" width="640">
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p style="font-family:Arial;text-align:center; font-size: 26px; font-weight: bold; color: #000 ; margin: 0 0 0;">
                                    <br>
                                    Здравствуйте, {{$name}}!
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 20px;"></td>
                        </tr>
                        <tr>
                            <td>
                                
                            
                            <table style="width: 640px; font-size: 0; line-height: 1; font-family:  Arial;">
                                <tbody>
                                    <tr>
                                        <td width="30"></td>
                                        <td style="font-family:Arial;font-size: 17px; line-height: 25px; text-align:center;color: #000 ">
                                             У нас потрясающие новости — вы можете получить главный еженедельный приз от EPICA! Поздравляем с исполнением желаний — мы знали, что все сбудется. <br>  

Сейчас мы считаем каждый зарегистрированный чек, чтобы точно узнать, сколько вы выиграли. Как только мы закончим, сразу свяжемся с вами, чтобы уточнить данные для перечисления приза согласно Правилам акции. Данные необходимо будет предоставить в течение 5 дней.<br>  <br>  

Еще раз поздравляем!<br><br>
                                            
                                        </td>
                                        <td width="30"></td>
                                    </tr>
                                </tbody>
                            </table>
                            </td>
                        </tr>
                       
                        <tr>
                            <td style="font-family:Arial;font-size: 17px; line-height: 25px; text-align:center;color: #000 ">
                                Спасибо, ваша команда EPICA. 
                                <br><br>
                                <a href="{{env('APP_URL')}}">
                                    <img src="{{env('APP_URL').'/images/eb.png'}}" width="640">
                                </a>
                            </td>
                        </tr>
                        
                    </tbody>
                </table>
        </td>
    </tr>
</table>
                
            
            </body>
        </html>