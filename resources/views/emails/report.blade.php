<html>
	<head></head>
	<body>
		Данные по акции <b> «Больше хочется - больше сбудется»</b> <br>

		Отчетная дата - {{\Carbon\Carbon::now()->addDays(-1)->format('d.m.Y')}}   <br><br>

		Количество зарегистрированных участников за {{\Carbon\Carbon::now()->addDays(-1)->format('d.m.Y')}} - <b> {{\App\Models\User::whereDate('CreatedAt', \Carbon\Carbon::now()->addDays(-1)->format('Y-m-d'))->count()}} </b> <br>

		Количество зарегистрированных чеков за {{\Carbon\Carbon::now()->addDays(-1)->format('d.m.Y')}} - <b> {{\App\Models\Cheque::whereDate('CreatedAt', \Carbon\Carbon::now()->addDays(-1)->format('Y-m-d'))->count()}} </b> <br>

		Количество валидных чеков за {{\Carbon\Carbon::now()->addDays(-1)->format('d.m.Y')}} - <b> {{\App\Models\Cheque::whereDate('CreatedAt', \Carbon\Carbon::now()->addDays(-1)->format('Y-m-d'))->where('Approved', 1)->count()}} </b> <br>

		Количество зарегистрированных участников всего - <b> {{\App\Models\User::whereDate('CreatedAt', '<=', \Carbon\Carbon::now()->format('Y-m-d'))->count()}} </b> <br>

		Количество зарегистрированных чеков всего - <b> {{\App\Models\Cheque::whereDate('CreatedAt', '<=', \Carbon\Carbon::now()->format('Y-m-d'))->count()}} </b> <br>

		Количество валидных чеков всего - <b> {{\App\Models\Cheque::whereDate('CreatedAt', '<=', \Carbon\Carbon::now()->format('Y-m-d'))->where('Approved', 1)->count()}} </b> <br><br><br>


		Выдано призов  50 руб. на телефон - <b> {{\App\Models\Winner::whereDate('CreatedAt', '<=', \Carbon\Carbon::now()->format('Y-m-d'))->where('WinType', 'garant')->count()}} </b> <br>

		@if ($dr = \App\Models\Drawing::getActive())
		Стоимость еженедельного приза текущей недели - <b> {{$dr->getScore()}} </b> <br>
		@endif

	</body>
</html>
