@extends('layout')

@section('content')
@push('styles')
<link rel="stylesheet" href="/css/stat1.css">
<style>
footer {
	background-color: #fff !important;
}
footer * {
	color: #162346 !important;
}
header {
	position: relative;
	z-index: 10;
}
body.scrolled header {
	background: rgb(255 ,255, 255 , 0.7);
}
</style>
@endpush
<div class="main-screen">
	<div class="top-render">
		<h1 class="main-title">
			<div>Больше хочется — </div>
			<div>больше сбудется</div>
		</h1>
		@if ($activeDrawing)
		<div class="tl-info">
			<p>
				Еженедельный приз
			</p>
			<div>
				{{number_format($activeDrawing->getScore(), 0, ' ', ' ')}} ₽
			</div>
			<p>
				из максимального  <b>1 000 000 ₽</b>
			</p>
		</div>
		@endif
		<div class="tl-btns">
			<a href="" @if (!\Auth::user()) data-toggle="modal" data-target="#registrationModal" @endif class="btn-custom @if (\Auth::user()) js-apm-show @endif" onclick="ym(72406741,'reachGoal','uchastie')">Принять участие</a>
			<a href="/winners" class="btn-custom btn-custom-invert">Результаты розыгрышей</a>			
		</div>
	</div>
	<div class="hover-render">
		<div class="hover-block trs">
			<div class="">
				<div class="hover-title">Покупайте</div>
				<div class="hover-desc"> 
					не менее 3-х йогуртов <br> EPICA в одном чеке
				</div>
			</div>
			<div class="hover-sm hover-desc">
				Участвуют все продукты EPICA
			</div>
		</div>
		<div class="hover-block trs">
			<div>
				<div class="hover-title">Регистрируйте</div>
				<div class="hover-desc"> 
					чеки на этом сайте или в чат-боте <br> 
					 и увеличивайте призовой фонд  <br> 
					 на 500₽ с каждой покупкой  <br> 
					 до 1 000 000 ₽ 
				</div>
			</div>
			<div class="hover-sm hover-desc">
				За первый зарегистрированный чек<br>
				 вы получаете гарантированный приз 50₽ <br>
				 на счет мобильного телефона
			</div>
		</div>
		<div class="hover-block trs">
			<div>
				<div class="hover-title">ВЫИГРЫВАЙТЕ</div>
				<div class="hover-desc"> 
					до 1 000 000₽ — каждую неделю  <br>
					3 000₽ — три приза каждый день <br>
					<span>
					50₽ — гарантированный приз <br>
					на счет мобильного телефона <br>
					за первый зарегистрированный чек
					 </span>
				</div>
			</div>
			<div class="hover-sm ">
				<div class="hover-title">Получайте</div>
				<div class="hover-desc"> 
					50₽ — гарантированный приз <br>
					на счет мобильного телефона <br>
					за первый зарегистрированный чек
				</div>
			</div>
		</div>
	</div>
</div>
<div class="pt" id="prizes"></div>
<div class="pt-wrap">
	<img src="/images/mp111.png" alt="">
	<img src="/images/mp211.png" alt="">
	<img src="/images/mp311.png" alt="">
</div>

@endsection

