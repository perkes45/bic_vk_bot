@extends('layout')

@section('content')

<p class='lg-text text-center mt-5' style="font-size: 16px; color: #fff">{!!$message!!}</p>

@push('styles')
<link rel="stylesheet" href="/css/stat1.css">
<style>
	main {
		min-height: 100vh  !important;
	}
</style>
@endpush

@endsection

