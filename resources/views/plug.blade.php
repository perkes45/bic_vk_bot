<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Epica «Больше хочется - больше сбудется!»</title>
    <meta name="description" content="Купи продукт «Лаборатории Касперского» и гаджет в Мегафоне, получи возможность вернуть до 100% стоимости устройства.">
    <meta name="token" content="{{csrf_token()}}">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/fonts.css">
    <!-- <link rel="stylesheet" href="/css/scroll.css"> -->
    <link href="https://qr.apmcheck.ru/css/widget.min.css" rel="stylesheet"/>

    <link rel="stylesheet" href="/css/index.css">
    <link rel="stylesheet" href="/css/apm.css">
    
    <style>
        .copyright {
            color: #fff !important;
        }
        footer {
            background: transparent !important;
        }
    </style>


</head> 
<body class="">

    <main style="background-image: url(/images/hero{{rand(1,5)}}.png);">
        <header>
            <a href="/?plug" class="logo-link"></a>
        </header> 
        <section class="plug-section">
            <h1>Начните исполнять свои желания через</h1>
            <div class="timer-wrap">
                <div>
                    <div id="days"></div>
                    <span>Дней</span>
                </div>
                <div>
                    <div id="hours"></div>
                    <span>Часов</span>
                </div>
                <div>
                    <div id="minutes"></div>
                    <span>Минут</span>
                </div>
                <div>
                    <div id="seconds"></div>
                    <span>Секунд</span>
                </div>

            </div>
        </section>
        <footer>
            <div class="copyright">
               EPICA © Все права защищены
           </div>
           <ul>
            <li>
                <a href="">Пользовательское соглашение</a>
            </li>
            <li>
                <a href="">Правила акции</a>
            </li>
        </ul>
    </footer>
</main>
</body>
</html>




@stack('modals')






<script src="/js/jquery.js"></script>
<script src="/js/owl.carousel.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<!-- <script src="/js/scroll.js"></script>  -->

<script src="/js/jquery.maskedinput.js"></script>

<script src="/js/jquery.paginate.js"></script>
<script src="/js/index.js"></script> 

<script>
    $(function(){
        if (window.innerWidth < 768) {
           $('main').css({'height' : document.documentElement.clientHeight+'px'})
        }

        $(window).on('resize', function(){
            if (window.innerWidth < 768) {
               $('main').css({'height' : document.documentElement.clientHeight+'px'})
            } else {
                $('main').css({'height' : '100vh'})
            }
        }) 
   })

    const second = 1000,
    minute = second * 60,
    hour = minute * 60,
    day = hour * 24;

    let start = "Mar 01, 2021 12:00:00",
    countDown = new Date(start).getTime(),
    x = setInterval(function() {    

        let now = new Date().getTime(),
        distance = countDown - now;

        var daysL = Math.floor(distance / (day));
        var hoursL = Math.floor((distance % (day)) / (hour));
        var minutesL = Math.floor((distance % (hour)) / (minute));
        var secondsL = Math.floor((distance % (minute)) / second);

        if (daysL < 10) {
            daysL = '0' + daysL;
        }

        if (hoursL < 10) {
            hoursL = '0' + hoursL;
        }

        if (minutesL < 10) {
            minutesL = '0' + minutesL;
        }

        if (secondsL < 10) {
            secondsL = '0' + secondsL;
        }

        document.getElementById("days").innerText = daysL,
        document.getElementById("hours").innerText = hoursL,
        document.getElementById("minutes").innerText = minutesL,
        document.getElementById("seconds").innerText = secondsL;

        //do something later when date is reached
        if (distance < 0) {


          clearInterval(x);
      }
        //seconds
    }, 0)
</script>


@stack('scripts')
</html>
