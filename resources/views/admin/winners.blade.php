@extends('admin.layout')
@section('content')
<table class="table table-bordered table-hover table-striped" id="js-table">
    
</table>

<form action="" method="POST">
    {{csrf_field()}}
    <input type="hidden" name="export" value="1">
    <div class="text-center" style="margin: 30px 0">
	<button type="submit" class="btn btn-danger">Выгрузить в Excel</button>
    </div>
</form>

<div class="mt-5">
<form action="" method="POST">
    {{csrf_field()}}
    <input type="hidden" name="exportCSV" value="1">
    <div class="text-center" style="margin: 30px 0">
    <button type="submit" class="btn btn-primary">Выгрузить призы</button>
    </div>
</form>
</div>

<script>
    $(function () {
	
	
    var table = $('#js-table').DataTable({
    "processing": true,
            "serverSide": true,
	    "ajax": {
		"url": "/{{Request::path()}}?{!!Request::getQueryString()!!}"
	    },
            "columns" : [
                    @foreach ($attrs as $k => $v)
            {data : '{{$k}}', title : '{{$v}}' }
            ,
                    @endforeach             
           ]
    });
    table.columns().every(function () {
    var that = this;
    index = that[0][0];
    $(that.header()).append('<input type="text" placeholder="Поиск" />');
    $('input', this.header()).on('click', function (e) {
    e.preventDefault();
    e.stopPropagation();
    return false;
    })
            $('input', this.header()).on('keyup change', function () {
    if (that.search() !== this.value) {
    console.log(this.value);
    that
            .search(this.value)
            .draw();
    }
    });
    })
    }
    )
</script>

@endsection
