@extends('admin.layout')
@section('content')
<div class="container-fluid">
    <div class="table-responsive">
	<table class="table table-bordered table-hover table-striped" id="js-table">
	    <thead></thead>
	    <tbody></tbody>
	</table>
    </div>
</div>
<form action="" method="POST">
    {{csrf_field()}}
    <input type="hidden" name="export" value="1">
    <div class="text-center" style="margin: 30px 0">
<!--	<button type="submit" class="btn btn-danger">Выгрузить все чеки</button> -->
    </div>
</form>

<script>
    $(function () {
	
    $.ajaxSetup({
	headers: {
	    'X-CSRF-TOKEN': $('meta[name="token"]').attr('content')
	}
    });
	
    var table = $('#js-table').DataTable({
    "processing": true,
            "serverSide": true,
	    "ajax": {
		"url": "/{{Request::path()}}?{!!Request::getQueryString()!!}",
		"type": "POST"
	    },
            "columns" : [
                    @foreach ($attrs as $k => $v)
            {data : '{{$k}}', title : '{{$v}}' }
            ,
                    @endforeach
            
            ]
    });
    
    table.on( 'draw', function () {
	console.log('s');
		if ($.fn.datetimepicker) {
		    $.datetimepicker.setLocale('ru');
		    $('body').find('.js-date').datetimepicker({
			timepicker: false,
			format: 'Y-m-d'
		    });
		}
    });
    
     table.columns().every(function () {
     var that = this;
     index = that[0][0];
     if (index != 10 && index != 11) $(that.header()).append('<input type="text" placeholder="Поиск" />');
     $('input', this.header()).on('click', function (e) {
     e.preventDefault();
     e.stopPropagation();
     return false;
     })
             $('input', this.header()).on('keyup change', function () {
    if (that.search() !== this.value) {
     console.log(this.value);
     that
             .search(this.value)
             .draw();
     }
     });
     })
    }
    )
</script>

@endsection
