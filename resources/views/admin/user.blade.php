@extends('admin.layout')
@section('content')

<div class="container">
	<div class="breadcrumb">
		<a href="/admin/users" class="breadcrumb-item">Пользователи</a> > 
		<b>{{$user->FirstName}} {{$user->LastName}}</b>
	</div>
	<div class="form-group">
		<form action="" method="POST" class="js-form">
			{{csrf_field()}}
			<div class="form-group">
				<label>Имя</label>
				<input type="text" required="" class="form-control" name="FirstName" value="{{$user->FirstName}}">
			</div>
			<div class="form-group">
				<label>Фамилия</label>
				<input type="text" required="" class="form-control" name="LastName" value="{{$user->LastName}}">
			</div>
			<div class="form-group">
				<label>Дата рождения</label>
				<input type="date" required="" class="form-control" name="BirthDate" value="{{$user->BirthDate}}">
			</div>
			<div class="form-group">
				<label>Город</label>
				<input type="text" required="" class="form-control" id="City" name="City" value="{{$user->City}}">
			</div>
			<div class="form-group">
				<label>Телефон</label>
				<input type="tel" id="Phone" required="" class="form-control" name="Phone" value="{{$user->Phone}}">
			</div>
			<div class="error"></div>
			<button type="submit" class="btn btn-success">Сохранить</button>
		</form>
	</div>
</div>

@endsection
