<!doctype html>
<html lang="{{ app()->getLocale() }}">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="token" content="{{csrf_token()}}">

		<title>Admin Panel</title>

		<!-- Fonts -->
		<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

		<script src="/js/jquery-3.2.1.min.js"></script>
		<script src="/js/jquery.dataTables.min.js"></script>
		<script src="/js/bootstrap.min.js"></script>

		<link rel="stylesheet" href="/css/bootstrap.min.css">    
		<link rel="stylesheet" href="/css/jquery.dataTables.min.css">    
		<link rel="stylesheet" href="/css/datetimepicker.min.css">    
		<link rel="stylesheet" href="/css/font-awesome.min.css">          
		<link rel="stylesheet" href="/css/suggestions.min.css">          
		<link rel="stylesheet" href="/css/admin.css">          

		<script src="/js/datetimepicker.min.js"></script>
		<script src="/js/jquery.maskedinput.js"></script>
		<script src="/js/suggestions.min.js"></script>
		<script src="/js/index1.js"></script>
		<script src="/js/admin.js"></script>


	</head>
	<body>
		<div class="page-content">
			<ul class="nav nav-tabs container">
				<li  class="nav-item" >
					<a class="nav-link @if (isset($active) && $active == 'admin')active @endif " href="/admin" >Данные по чекам</a>
				</li>   
				@if (Auth::user()->isAdmin())
				<li class="nav-item">
					<a class="nav-link @if (isset($active) && $active == 'users')active @endif " href="/admin/users" >Данные участники</a>
				</li>
				<li class="nav-item">
					<a class="nav-link @if (isset($active) && $active == 'stat')active @endif " href="/admin/stat" >Сводка</a>
				</li>   
	 
				<li class="nav-item">
					<a class="nav-link @if (isset($active) && $active == 'winners')active @endif " href="/admin/winners" >Победители</a>
				</li>
<li class="nav-item">
                                        <a class="nav-link @if (isset($active) && $active == 'feedback')active @endif " href="/admin/feedback" >Обратная связь </a>
                                </li>
				@endif
			</ul>

			<div class="page-content-row">
				@yield('content')
			</div>
		</div>
	</body>
</html>
