<form action="" method="POST" class="js-form" data-action="alert">
    {{csrf_field()}}
    <select name="Status" >
	<option value="{{ \App\Models\Winner::TYPE_PROCESSING }}" @if ($status == \App\Models\Winner::TYPE_PROCESSING) selected @endif>На рассмотрении</option>
	<option value="{{ \App\Models\Winner::TYPE_CONFIRMED }}" @if ($status == \App\Models\Winner::TYPE_CONFIRMED) selected @endif>Подтвержден </option>
    </select>
    <input type="hidden" value="{{$id}}" name="WinnerId">
    <button class="btn btn-xs btn-primary">ОК</button>
    <div class="error"></div>
</form>