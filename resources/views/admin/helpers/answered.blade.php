<form action="/admin/updateFeedback" method="POST" class="js-form">
	<input type="checkbox" name="Answered" value="1" class="checkbox" @if ($answered) checked @endif>
	<input type="hidden" name="FeedbackId" value="{{$id}}">
	<div class="error"></div>
	<button class="btn btn-sm btn-primary">Ок</button>
    </form>