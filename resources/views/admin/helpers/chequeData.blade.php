@if (count($data))
@foreach($data as $key => $val)
<div @if (isset($count) && $count > 1) class="alerted" @endif>
    <form action="/admin/updateCheque" method="POST" class="js-form" data-action="alert-refresh">
	<input type="text" name="{{$key}}" value="{{$val}}" class="form-control-sm form-control @if ($key == 'BuyDate') js-date @endif">
	<input type="hidden" name="ChequeId" value="{{$id}}">
	<div class="error"></div>
	<button class="btn btn-sm btn-primary">сохранить</button>
    </form>
</div>
@endforeach
@endif
