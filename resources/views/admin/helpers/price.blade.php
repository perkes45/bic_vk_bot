
<form action="/admin/winners/price" method="POST" class="js-form" data-action="alert">
    {{csrf_field()}}
    <input type="text" value="{{$price}}" name="Price">
    <input type="hidden" value="{{$id}}" name="WinnerId">
    <button class="btn btn-xs btn-primary">ОК</button>
    <div class="error"></div>
</form>
