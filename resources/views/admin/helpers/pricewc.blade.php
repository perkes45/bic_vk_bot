<form action="/admin/winners/pricewc" method="POST" class="js-form" data-action="alert">
    {{csrf_field()}}
    <input type="text" value="{{$price}}" name="PriceWC">
    <input type="hidden" value="{{$id}}" name="WinnerId">
    <button class="btn btn-xs btn-primary">ОК</button>
    <div class="error"></div>
</form>
