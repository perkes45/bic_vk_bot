<form action="" method="POST" class="js-form" data-action="alert">
    {{csrf_field()}}
    @foreach ($prizes as $prize)
    <div class="checkbox">
	<label>			    
	    <nobr>
		<input type="checkbox" name="PrizeId[]" @if (in_array($prize->Id , $cheque->winners->pluck('PrizeId')->toArray()))disabled checked @endif value="{{$prize->Id}}"> {{$prize->PrizeName}}
	    </nobr>
	</label>
    </div>
    @endforeach		    
    <input type="hidden" value="{{$cheque->Id}}" name="ChequeId">
    <button class="btn btn-sm btn-primary">ОК</button>
    <div class="error"></div>
</form>