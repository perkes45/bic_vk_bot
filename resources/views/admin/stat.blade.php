@extends('admin.layout')
@section('content')
<div class="container">
<div class="mt-5 mb-5">
	<form action="">
		<div class="row">
			<div class="col-6">
				<div class="form-group">
					<label for="">Дата с</label>
					<input type="date" name="DateFrom" class="form-control" value="{{$dateFrom->format('Y-m-d')}}">
				</div>
			</div>
			<div class="col-6">
				<div class="form-group">
					<label for="">Дата по</label>
					<input type="date" name="DateTo" class="form-control" value="{{$dateTo->format('Y-m-d')}}">
				</div>
			</div>
		</div>
		<div class="text-center">
			<button class="btn btn-success">Отправить</button>
		</div>
	</form>
</div>
<table class="table table-bordered table-hover table-striped">
    <thead>
	<tr>
	    <th>Поле</th>
	    <th>Значение</th>
	</tr>
    </thead>
    <tbody>
		<tr>
			<td colspan="2">
				УЧАСТНИКИ И ЧЕКИ
			</td>			
		</tr>
		<tr>
			<td>Зарегистрированные Участники </td>
			<td>{{$result['users']}}</td>
		</tr>
		<tr>
			<td>Участники с чеками</td>
			<td>{{$result['users_with_cheques']}}</td>
		</tr>
		<tr>
			<td>Участники с валидными чеками</td>
			<td>{{$result['users_with_valid_cheques']}}</td>
		</tr>
		<tr>
			<td>Принятые чеки</td>
			<td>{{$result['cheques']}}</td>
		</tr>
		<tr>
                        <td>Всего чеков</td>
                        <td>{{$result['all_cheques']}}</td>
                </tr>
		<tr>
                        <td>Отклоненных чеков</td>
                        <td>{{$result['rejected_cheques']}}</td>
                </tr>
		<tr>
			<td>Количество чеков на 1 участника</td>
			<td>{{$result['cheques_per_user']}}</td>
		</tr>
		<tr>
			<td colspan="2">
				ПОБЕДИТЕЛИ
			</td>			
		</tr>
		
		<tr>
			<td>Победители ежедневного приза</td>
			<td>{{$result['users_3000']}}</td>
		</tr>
		<tr>
			<td>Победители еженедельного приза</td>
			<td>{{$result['users_1000000']}}</td>
		</tr>
    </tbody>
</table>

</div>
@endsection


      
      
      
      
      
      
      
