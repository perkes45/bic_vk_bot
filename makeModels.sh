#!/bin/bash

TMPDIR=models.tmp
if [ -d $TMPDIR ]; then
	rm -rf $TMPDIR
fi

mkdir $TMPDIR

sudo php artisan models:generate -vvv -p $TMPDIR --namespace=App\\Models\\Base
sed -e s/Base\\\\//g -e 's/extends Model/extends AbstractTable/g' -i $TMPDIR/*.php
for model in `ls $TMPDIR`; do
	nonBaseModelFile=app/Models/$model
	if [ ! -f $nonBaseModelFile ]; then
		modelClass=`echo $model | sed s/\\.php$//g`
		echo "<?php\nnamespace App\\Models;\n\nclass $modelClass extends Base\\\\$modelClass {\n\t//\n}\n" > $nonBaseModelFile
	fi
	tr '\n' '\000' < $TMPDIR/$model > $TMPDIR/$model.1
	mv $TMPDIR/$model.1 $TMPDIR/$model
done

for model in `ls $TMPDIR`; do
	tr '\000' '\n' < $TMPDIR/$model > $TMPDIR/$model.1

	mv $TMPDIR/$model.1 $TMPDIR/$model
done


cp $TMPDIR/* app/Models/Base
rm -rf $TMPDIR

sudo php artisan ide-helper:models -vvv -W


