var elixir = require("laravel-elixir");

elixir(function (mix) {
    mix.styles([
        'bootstrap.min.css',
        'fonts.css',
        'owl.carousel.min.css',
        'owl.theme.default.css',
        'jquery.scrollbar.css',
        'datetimepicker.min.css',
        'suggestions.min.css',
        'index.css',
    ], 'public/css/all.css', 'public/css');


    mix.scripts([
        'jquery-3.2.1.min.js',
        'bootstrap.min.js',
        'owl.carousel.min.js',
        'jquery.scrollbar.min.js',
        'jquery.maskedinput.js',
        'datetimepicker.min.js',
        'suggestions.min.js',
        'index.js',
    ], 'public/js/all.js', 'public/js');
    mix.version(["css/all.css", "js/all.js"], 'public');

});

